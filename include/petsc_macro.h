#ifndef __PETSC_MACRO_H__
#define __PETSC_MACRO_H__


/*
// Won't work, unless it can be made single-line?
#define macro_PATTERN_NO_OBJ(obj,val)           \
pattern No ## obj :: obj \
pattern No ## obj <- ((== obj val) -> True) \
  where 
*/


#define macro_PETSC_OBJECT_INST(pfx,dat)                                     \
instance IsPetscObject dat where {                                           \
  {-# INLINE create #-}                                                      \
; {-# INLINE setFromOptions #-}                                              \
; {-# INLINE setUp #-}                                                       \
; {-# INLINE destroy #-}                                                     \
; create         = pfx/**/Create                                             \
; setFromOptions = pfx/**/SetFromOptions                                     \
; setUp          = pfx/**/SetUp                                              \
; destroy        = pfx/**/Destroy }

#define macro_PETSC_CREATE(pfx,lab,dat)                                      \
pfx/**/Create comm = liftIO $ with No/**/dat $ \ptr -> do {                  \
lab/**/Create comm ptr >>= chkErr >> peek ptr }

#endif /* __PETSC_MACRO_H__ */

