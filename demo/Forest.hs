{-# LANGUAGE CPP, DeriveGeneric #-}

------------------------------------------------------------------------------
-- |
-- Module      : Forest
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Create & view a parallel-forest mesh.
-- 
-- Changelog:
--  + 08/08/2016  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Main where

import Foreign.Ptr

import GHC.Generics (Generic)
import qualified System.Environment as System
import Control.Monad (when)
import Control.Monad.IO.Class
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
import Text.Printf

import Bindings.Numeric.PETSc
import Bindings.Data.Mesh.P4est hiding (Comm)
import qualified Bindings.Data.Mesh.P4est as Fest

import Unsafe.Coerce (unsafeCoerce)


------------------------------------------------------------------------------
data Opts =
  Opts { dmtype  :: DMType
       , refine  :: Int
       , rdepth  :: Int
       , adjdim  :: Int
       , overlap :: Int
       , tpause  :: Double
       } deriving (Eq, Show, Generic)

------------------------------------------------------------------------------
-- | Hard-code this, so that `p4est` works as expected.
dim :: Int
dim  = 2

------------------------------------------------------------------------------
parseOptions :: MonadIO m => m Opts
parseOptions  = liftIO $ do
  let fp = __FILE__
  petscOptions "" "Parallel-forest options." "DMFOREST" $ Opts
    <$> petscOptionsType "-dm_type" "The type of the DM" DMFOREST
    <*> option "-refine"  "Initial refinement"       fp 2
    <*> option "-depth"   "Maximum refinement depth" fp 5
    <*> option "-adjdim"  "Adjacency dimension"      fp 0
    <*> option "-overlap" "Partition overlap amount" fp 0
    <*> option "-pause"   "Pause duration"           fp 0

------------------------------------------------------------------------------
createMesh :: MonadIO m => Comm -> Opts -> m DM
createMesh comm opts = do
  mesh <- create comm
  rank <- mpiCommRank comm
  -- ^ construct a forest, and check to make sure that its type is correct:
  dmSetType mesh $ dmtype opts
  dmIsForest mesh >>= \fore -> when (not fore && rank == 0) $ liftIO $ do
    printf "Mesh-type (%s) is not a forest-type\n\n" $ show (dmtype opts)
  -- ^ set additional forest parameters:
  dmSetDimension mesh dim
  dmForestSetInitialRefinement  mesh $ refine  opts
  dmForestSetAdjacencyDimension mesh $ adjdim  opts
  dmForestSetPartitionOverlap   mesh $ overlap opts
  setFromOptions mesh
  -- ^ TODO: needs to be "distributed" to work with multiple cores, and there
  --     is no built-in way to do this? Conversion from `p4est` may work, if
  --     already distributed?
  setUp mesh
  return mesh

------------------------------------------------------------------------------
forestMesh :: MonadIO m => Comm -> Opts -> m DM
forestMesh comm opts = liftIO $ do
  -- ^ construct initial parallel-forest:
  conn <- connCreate UnitSquare
  fest <- p4estNew (unsafeCoerce comm) conn 0 NoInitFn nullPtr
  -- ^ refine & convert to `DMPlex`:
  refn <- mkRefineFn $ refineFn (rdepth opts)
  p4estRefine fest False refn NoInitFn
  plex <- p4estToPlex comm fest
  -- ^ clean-up:
  p4estDestroy fest
  connDestroy conn
  return plex

p4estToPlex :: MonadIO m => Comm -> P4est -> m DM
p4estToPlex comm fest = liftIO $ printf "p4estToPlex: STUB\n" >> create comm

-- | Refine the given quadrant if the result from this callback is `True`.
refineFn :: Int -> P4est -> Int -> Quadrant -> IO Bool
refineFn maxLevel p4est i quad
  | getLevel quad > maxLevel = return False
  | otherwise                = do
      return False

------------------------------------------------------------------------------
-- | Just one field, so easier to figure out what the hell is going on.
createField :: MonadIO m => DM -> Opts -> m Section
createField dm opts = liftIO $ do
  let ndofs = Vec.replicate n 0 Vec.// vass
      vass  = [(0, 1)]          -- vertex assignment for field
      eass  = [(1, 1)]          -- edge assignment for field
      fass  = [(d, 1)]          -- face assignment for field
      (n,d) = (d*succ d, dim)
      comps = Vec.singleton 1
      bcs   = Vec.singleton 0
  bcpt <- Vec.singleton <$> dmGetStratumIS dm "marker" 1
  secn <- dmPlexCreateSection dm d comps ndofs bcs Vec.empty bcpt NoIS
  destroy $ Vec.head bcpt
  petscSectionSetFieldName secn 0 "mu"
  return secn

------------------------------------------------------------------------------
-- | Distribute the mesh amongst all nodes.
--   TODO: Move this to the library?
distribute :: MonadIO m => DM -> Opts -> m DM
distribute base opts = do
  let nochng = "Mesh already partitioned as desired."
      redist = "Overlap of " ++ show olap ++ " setup for new mesh."
      olap   = overlap opts
  dmPlexDistribute base olap NoSF >>= \mesh -> case mesh of
    NoDM -> liftIO (putStrLn nochng) >> return base
    _    -> liftIO (putStrLn redist) >> destroy base >> return mesh

--   TODO: Move this to the library?
createBCLabel :: MonadIO m => DM -> String -> m ()
createBCLabel dm str = do
  dmCreateLabel dm str
  lab <- dmGetLabel dm str
  dmPlexMarkBoundaryFaces dm lab
  dmPlexLabelComplete     dm lab

------------------------------------------------------------------------------
forestHelp = __FILE__ ++ "Create & view a parallel-forest mesh." :: String

main :: IO ()
main  = do
  argv <- ("main":) <$> System.getArgs
  withPETSc argv forestHelp $ do
    opts <- parseOptions
    rank <- mpiCommRank CommWorld
    mesh <- createMesh CommWorld opts
    -- ^ add a mesh-viewer:
    view <- fst <$> petscOptionsGetViewer CommWorld "" "-dm_view"
    dmView mesh view
    -- ^ convert to `DMPLEX` and then define a field on the mesh:
    plex <- dmConvert mesh DMPLEX >>= flip distribute opts
    labd <- dmHasLabel plex "marker"
    if labd
      then printf " + boundary `marker` exists\n"
      else printf " + labeling boundary as `marker`\n" >> createBCLabel plex "marker"
    setFromOptions plex
    setUp plex
    secn <- createField plex opts
    dmSetDefaultSection plex secn
    -- ^ allocate a system matrix:
    matJ <- dmCreateMatrix plex
    matView matJ view
    draw <- petscViewerDrawOpen CommWorld NoDisplay "" Nothing $ Just (800,800)
    matView matJ draw
    -- ^ wait for time/key-press:
    when (tpause opts < 0 && rank == 0) $ do
      putStrLn "\npress ENTER to continue..."
    petscSleep (tpause opts)
    -- ^ clean-up:
    destroy draw
    destroy matJ
    destroy plex
    destroy view
    destroy mesh
