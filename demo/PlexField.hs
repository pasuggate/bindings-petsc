{-# LANGUAGE CPP, TemplateHaskell, DeriveGeneric, FlexibleInstances #-}

------------------------------------------------------------------------------
-- |
-- Module      : PlexField
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Creates a parallel mesh, adds a field to it, and then views it.
-- 
-- Changelog:
--  + ??/08/2016  --  initial file;
-- 
-- TODO:
--  + add a field, and then see what happens (after write) to the local data
--    when `overlap > 0`;
-- 
------------------------------------------------------------------------------

module Main where

import Foreign.Ptr
import Foreign.Storable

import GHC.Generics (Generic)
import qualified System.Environment as System
import Control.Monad (when)
import Control.Monad.IO.Class
import qualified Data.IntSet as Set
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
import Text.Printf

import Bindings.Numeric.PETSc
import Bindings.Numeric.PETSc.Internal (vdbl)


------------------------------------------------------------------------------
-- | User options.
data Opts = Opts { topodim, elements, overlap :: Int
                 , simplex, interp :: Bool
                 , fname :: String
                 , tpause :: Double
                 , dmview, secview, matview :: Bool
                 } deriving (Eq, Show, Generic)


------------------------------------------------------------------------------
-- | Register additional command-line options, and then parse the command-line
--   options.
parseOptions :: MonadIO m => Comm -> m Opts
parseOptions comm = liftIO $ petscOptions "" "PlexField options" "PF" $ do
  let fp = __FILE__ :: String
  Opts
    <$> option "-dim"         "Topological mesh dimension" fp 2
    <*> option "-elements"    "Number of elements"         fp 1
    <*> option "-overlap"     "Element overlap amount"     fp 0
    <*> option "-simplex"     "Use simplicial mesh?"       fp False
    <*> option "-interpolate" "Use interpolation?"         fp False
    <*> option "-filename"    "Filename for export"        fp "sol.vtk"
    <*> option "-pause"       "Pause duration"             fp 1
    <*> option "-dmview"      "Display the DM?"            fp False
    <*> option "-secview"     "Display field section?"     fp False
    <*> option "-matview"     "Display matrix structure?"  fp False

------------------------------------------------------------------------------
-- TODO: Cuurently incomplete or incorrect?
nonconformalMesh :: MonadIO m => Comm -> Opts -> m DM
nonconformalMesh comm opts = do
  let numpts = [14, 7]
      csize  = [4,4,4,4,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
      cones  = [0,1,3,4, 1,2,4,6, 3,4,10,11, 4,5,7,8, 5,6,8,9, 7,8,11,12, 8,9,12,13]
      orient = replicate 28 0
      coords = [-2,2, 0,2, 2,2, -2,0, 0,0, 1,0, 2,0, 0,-1, 1,-1, 2,-1, -2,-2, 0,-2,
                1,-2, 2,-2]
      dim    = topodim opts
  base <- create comm
  dmSetType base DMPLEX
  dmSetDimension base dim
  dmPlexCreateFromDAG base 1 numpts csize cones orient coords
  plex <- dmPlexInterpolate base
  -- ^ construct the reference refinement-tree:
  tree <- dmPlexCreateDefaultReferenceTree comm dim $ simplex opts
  dmPlexSetReferenceTree plex tree
  destroy tree
  -- ^ set parents & children:
  return plex

------------------------------------------------------------------------------
-- | Nonconformal mesh, as might be constructed from a quadtree:
--    
--    (3)----11----(4)--12--(5)
--     |           /|        |
--     |          | 14  [1]  16
--     |         18 |        |
--     13   [0]   |(6)--15--(7)
--     |          | |        |
--     |          | 19  [2]  21
--     |           \|        |
--    (8)----17----(9)--20--(10)
--   
--   NOTE: See notes, diagrams (2) & (5), @ 19/08/2017 .
--   NOTE: No free dof's, when Dirichlet boundary-conditions are imposed?
quadtreeMesh :: MonadIO m => Comm -> Opts -> m DM
quadtreeMesh comm opts = do
  let numpts = [8, 11, 3]
      csize  = [4,4,4, 0,0,0,0,0,0,0,0, 2,2,2,2,2,2,2,2,2,2,2]
      cones  = [11,18,17,13, 12,16,15,14, 15,21,20,19,
                3,4, 4,5, 8,3, 6,4, 7,6, 5,7, 9,8, 4,9, 9,6, 10,9, 7,10]
      orient = length cones `replicate` 0
      coords = [0,2, 2,2, 3,2, 2,1, 3,1, 0,0, 2,0, 3,0]
      dim    = 2
  base  <- create comm
  dmSetType base DMPLEX
  dmSetDimension base dim
  dmSetCoordinateDim base dim
  dmPlexCreateFromDAG base dim numpts csize cones orient coords
  -- ^ construct the reference refinement-tree:
  tree  <- dmPlexCreateDefaultReferenceTree comm dim $ simplex opts
  dmPlexSetReferenceTree base tree
  destroy tree
  -- ^ TODO: setup parent & children sections:
  (s,e) <- dmPlexGetChart base
  secn  <- create comm
  petscSectionSetChart secn s e
  let parents  = [18, 18, 18]
      children = [ 6, 14, 19]
  sequence_ [petscSectionSetDof secn i 1 | i <- children]
  setUp secn
  dmPlexSetTree base secn parents [] -- children
  destroy secn
  -- ^ distribute the mesh amongst all nodes:
  let nochng = "Mesh already partitioned as desired."
      redist = "Overlap of " ++ show olap ++ " setup for new mesh."
      olap   = overlap opts
  dmPlexDistribute base olap NoSF >>= \mesh -> case mesh of
    NoDM -> liftIO (putStrLn nochng) >> return base
    _    -> liftIO (putStrLn redist) >> destroy base >> return mesh

------------------------------------------------------------------------------
createMesh :: MonadIO m => Opts -> m DM
createMesh opts = liftIO $ do
  let cells = Vec.replicate dim elN :: Vector Int
      bt    = BdyGhost
      dim   = topodim opts
      elN   = elements opts
      olap  = overlap opts
  -- ^ build the initial mesh:
  plex <- case simplex opts of
    True -> dmPlexCreateBoxMesh    CommWorld dim $ interp opts
    _    -> dmPlexCreateHexBoxMesh CommWorld cells bt bt bt
  -- ^ construct the reference refinement-tree:
  tree <- dmPlexCreateDefaultReferenceTree CommWorld dim (simplex opts)
  dmPlexSetReferenceTree plex tree
  destroy tree
  -- ^ distribute the mesh amongst all nodes:
  let nochng = "Mesh already partitioned as desired."
      redist = "Overlap of " ++ show olap ++ " setup for new mesh."
  dmPlexDistribute plex olap NoSF >>= \mesh -> case mesh of
    NoDM -> putStrLn nochng >> return plex
    _    -> putStrLn redist >> dmDestroy plex >> return mesh

------------------------------------------------------------------------------
meshRefine :: MonadIO m => DM -> Opts -> m DM
meshRefine dm opts = liftIO $ do
  rank <- getComm dm >>= mpiCommRank
  rlim <- dmPlexGetRefinementLimit dm
  dmPlexSetRefinementLimit dm 1.0
  printf "** MPI%d: \tRefinement limit  \t= %g\n" rank rlim
  runi <- dmPlexGetRefinementUniform dm
  -- ^ TODO: needs to be "distributed" to work with multiple cores, and there
  --     is no built-in way to do this? Conversion from `p4est` may work, if
  --     already distributed?
--   dmPlexSetRefinementUniform dm False
  printf "** MPI%d: \tRefinement uniform\t= %s\n" rank $ show runi
  dmPlexSetRefinementFunction dm $ refineVolume (topodim opts)
  getComm dm >>= dmRefine dm
--   return dm

-- | Callback that computes the volume of an element, given the coordinates
--   of the centroid of the element.
refineVolume :: Int -> Ptr PReal -> Ptr PReal -> IO PErr
refineVolume dim pos ptr = do
  loc <- vdbl <$> fromArrayM dim pos
  printf " loc: %s\n" $ show loc
  poke ptr 0
  petscSuccess


------------------------------------------------------------------------------
createFields :: MonadIO m => DM -> Opts -> m Section
createFields dm opts = liftIO $ do
  let ndofs = Vec.replicate 12 0 Vec.// [(0, 1), (2*dim+1, dim), (3*dim+1, dim-1)]
--       locns = [0*(dim+1)+0, 1*(dim+1)+dim, 2*(dim+1)+dim-1]
      comps = Vec.fromList [1, dim, dim-1]
      bcs   = Vec.singleton 0
      dim   = topodim opts
  bcpt <- Vec.singleton <$> dmGetStratumIS dm "marker" 1
  secn <- dmPlexCreateSection dm dim comps ndofs bcs Vec.empty bcpt NoIS
  isDestroy (Vec.head bcpt)
  petscSectionSetFieldName secn 0 "u"
  petscSectionSetFieldName secn 1 "v"
  petscSectionSetFieldName secn 2 "w"
  return secn

-- | Just one field, so easier to figure out what the hell is going on.
createField :: MonadIO m => DM -> Opts -> m Section
createField dm opts = liftIO $ do
  let ndofs = Vec.replicate n 0 Vec.// vass
      vass  = [(0, 1)]          -- vertex assignment for field
      eass  = [(1, 1)]          -- edge assignment for field
      fass  = [(d, 1)]          -- face assignment for field
      (n,d) = (d*succ d, topodim opts)
      comps = Vec.singleton 1
      bcs   = Vec.singleton 0
  bcpt <- Vec.singleton <$> dmGetStratumIS dm "marker" 1
  secn <- dmPlexCreateSection dm d comps ndofs bcs Vec.empty bcpt NoIS
  destroy $ Vec.head bcpt
  petscSectionSetFieldName secn 0 "mu"
  return secn

------------------------------------------------------------------------------
neighbourhood :: MonadIO m => DM -> Int -> m (Vector Int)
neighbourhood dm cell = liftIO $ do
--   (s,e) <- dmPlexGetHeightStratum dm 0
  cone <- dmPlexCone dm cell
  quad <- nubs <$> dmPlexCone dm `mapM` Vec.toList cone
  skel <- nubs <$> dmPlexSupport dm `mapM` Vec.toList quad
  supp <- nubs <$> dmPlexSupport dm `mapM` Vec.toList skel
--   supp <- dmPlexSupport dm `mapM` Vec.toList (Vec.concat face)
  return supp

-- | Build a sorted vector of indices, from the given list of index-vectors.
nubs :: [Vector Int] -> Vector Int
nubs  = Vec.fromList . Set.toList . foldr ins Set.empty
  where
    ins xs js = foldr Set.insert js $ Vec.toList xs

------------------------------------------------------------------------------
localStuff :: MonadIO m => DM -> Vec -> Opts -> m ()
localStuff dm vecU opts = liftIO $ do
  vecL <- dmGetLocalVector dm
  dmGlobalToLocalBegin dm vecU InsertValues vecL
  dmGlobalToLocalEnd   dm vecU InsertValues vecL

  rank <- getComm dm >>= mpiCommRank
{-- }
  lsiz <- vecGetLocalSize vecL
  cone <- show <$> dmPlexGetAdjacencyCone dm
  clos <- show <$> dmPlexGetAdjacencyClosure dm
  printf "** RANK %2d:\tLocal vector size\t= %d\n" rank lsiz
  printf "** RANK %2d:\tAdjacency cone   \t= %s\n" rank cone
  printf "** RANK %2d:\tAdjacency closure\t= %s\n" rank clos
--}

{-- }
  -- ^ closure data:
  (s, e ) <- dmPlexGetHeightStratum dm 0
  (n, ar) <- dmPlexVecGetClosure dm NoSection vecL s
  dmPlexVecRestoreClosure dm NoSection vecL s n ar
  printf "** RANK %2d:\tClosure size     \t= %d\n" rank n

  -- ^ closure indices:
--   (s, e ) <- dmPlexGetHeightStratum dm $ topodim opts
  setFromOptions dm >> setUp dm >> dmPlexCreateClosureIndex dm NoSection
  glob <- dmGetDefaultGlobalSection dm
  locl <- dmGetDefaultSection dm
--   locl <- dmGetCoordinateSection dm
  let clos i = dmPlexClosureIndices dm locl glob i >>= printf "> clos(%d): \t%s\n" i . show
      cone i = dmPlexCone dm i >>= disp rank "cone" i
      nbrs i = neighbourhood dm i >>= disp rank "nbrs" i
      disp 0 l i = printf "> %s(%d): \t%s\n" l i . show
      disp _ _ _ = const $ pure ()
--   sequence_ [clos i | i <- [s..pred e]]
--   sequence_ [cone i | i <- [s..pred e]]
  sequence_ [nbrs i | i <- [s..pred e]]
--}

{-- }
  -- ^ show the chart:
  (s, e ) <- dmPlexGetChart dm
  printf "** RANK %2d:\tMesh chart       \t= [%d, %d)\n" rank s e

  -- ^ display the supports of the nodes (which are edges):
  (s, e ) <- dmPlexGetHeightStratum dm $ topodim opts
  let supp i = dmPlexSupport dm i >>= printf "> supp(%d): \t%s\n" i . show
      adjy i = dmPlexAdjacency dm i >>= printf "> adjy(%d): \t%s\n" i . show
      pdat i = dmPlexGetPointLocal dm i >>= printf "> pdat(%d): \t%s\n" i . show
--   setFromOptions dm >> setUp dm
  sequence_ [supp i | i <- [s..pred e]]
--   sequence_ [adjy i | i <- [s]] -- ..pred e]]
--   sequence_ [pdat i | i <- [s..pred e]]
--}

{-- }
  let ij = Vec.enumFromN s 3
--   let ij = Vec.enumFromN s (e-s)
  dmPlexMeet dm ij >>= printf "** RANK %2d:\tMeet \t= %s\n" rank . show
  dmPlexJoin dm ij >>= printf "** RANK %2d:\tJoin \t= %s\n" rank . show

  -- ^ show the anchor-points:
  (sc,is) <- dmPlexGetAnchors dm
--   view <- petscViewerStdOutWorld
--   petscSectionView sc view
--   isView is view
--}

  dmRestoreLocalVector dm vecL

------------------------------------------------------------------------------
writeVTK :: MonadIO m => Vec -> Opts -> m ()
writeVTK vec opts = liftIO $ do
  -- ^ create a `Vec` with this layout, and then view it:
  file <- getComm vec >>= petscViewerCreate
  petscViewerSetType file ViewerVTK
  petscViewerPushFormat file PETSC_VIEWER_ASCII_VTK
  petscViewerFileSetName file $ fname opts
  vecView vec file
  destroy file
  
------------------------------------------------------------------------------
plexHelp = "Define a simple field over the mesh\n\n" :: String

showStrata :: MonadIO m => DM -> m ()
showStrata dm = liftIO $ do
  rank <- getComm dm >>= mpiCommRank
  dim  <- dmGetDimension dm
  let go i = do se <- dmPlexGetHeightStratum dm i
                printf "> Strata: %d\t%s\n" i $ show se
  when (rank == 0) $ sequence_ [go i | i <- [0..dim]]

main :: IO ()
main  = do
  argv <- ("main":) <$> System.getArgs
  withPETSc argv plexHelp $ do
    -- ^ determine the initial settings & environment:
    opts <- parseOptions CommWorld
    rank <- mpiCommRank CommWorld
    view <- petscViewerStdOutWorld
    let dim = topodim opts
    when (rank == 0) $ print opts

    -- ^ construct & distribute a (DMPlex) mesh:
    dm   <- createMesh opts
--     fine <- meshRefine dm opts
    when (dmview opts) $ do
      putStrLn ""
--       dmView dm view
--       reft <- dmPlexGetReferenceTree dm
--       dmView reft view
--       dmView fine view
--       plex <- nonconformalMesh CommWorld opts
--       dmView plex view
      quad <- quadtreeMesh CommWorld opts
      showStrata quad
      dmView quad view

--       supp <- dmPlexGetSupportSection dm
--       sectionView supp view
--       cone <- dmPlexGetConeSection dm
--       sectionView cone view

    -- ^ define the fields on the mesh:
--     secn <- createFields dm opts
    secn <- createField dm opts
    dmSetDefaultSection dm secn
    when (secview opts) $ petscSectionView secn view

    -- ^ store the field-coefficients within a (distributed) vector:
    vecU <- dmCreateGlobalVector dm
    localStuff dm vecU opts

    withGlobalVector dm $ writeVTK `flip` opts

    -- ^ allocate (and view) the non-zero pattern of a Jacobian matrix:
    matx <- dmCreateMatrix dm
    draw <- petscViewerDrawOpen CommWorld NoDisplay "" Nothing $ Just (800,800)
    matView matx draw
    when (matview opts) $ matView matx view

    -- ^ wait for time/key-press:
    when (tpause opts < 0 && rank == 0) $ do
      putStrLn "\npress ENTER to continue..."
    petscSleep (tpause opts)

    -- ^ clean-up:
    destroy secn
    destroy dm
