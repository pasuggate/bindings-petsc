{-# LANGUAGE CPP, DeriveGeneric #-}

------------------------------------------------------------------------------
-- |
-- Module      : PoissonAMR
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Stokes problem in 2D or 3D with simplicial finite elements, and using
-- unstructured meshes, via DMPlex.
-- 
-- Changelog:
--  + 09/08/2016  --  initial file;
-- 
-- NOTE:
--  + based on `ex62.c` from the SNES tutorials;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Main where

import GHC.Generics (Generic)
import qualified System.Environment as System
import Control.Monad.IO.Class
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec

import Bindings.Numeric.PETSc


------------------------------------------------------------------------------
-- | User options.
data Opts = Opts { debug   :: Int
                 , runtype :: RunType
                 , logev   :: LogEvent
                 , showIni :: Bool
                 , showSol :: Bool
                 , showErr :: Bool
                 , topodim :: Z
                 , interp  :: Bool
                 , simplex :: Bool
                 , rlimit  :: Double
                 , tpart   :: Bool
                 , bctype  :: BCType
--                  , exactfns    :: Vector R -> Vector R -> Opts -> IO ()
                 } deriving (Eq, Show, Generic)

data BCType = Neumann | Dirichlet
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic)

data RunType = RunFull | RunTest
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic)

type Z = Int


------------------------------------------------------------------------------
parseOptions :: MonadIO m => m Opts
parseOptions  = liftIO $ do
  let fp = __FILE__
      bc = ["neumman", "dirichlet"]
      bd = bc !! fromEnum Dirichlet
      rt = ["full", "test"]
      rd = rt !! fromEnum RunFull
  petscOptions "" "Stokes' problem options." "DMPLEX" $ Opts
    <$> petscOptionsInt   "-debug"    "Debugging level"             fp 0
    <*> petscOptionsEList "-run_type" "<full|test>"                 fp rt rd
    <*> liftIO (petscLogEventRegister "CreateMesh" DMClassId)
    <*> petscOptionsBool  "-show_initial"  "Show initial guess"     fp False
    <*> petscOptionsBool  "-show_solution" "Show solution"          fp True
    <*> petscOptionsBool  "-show_error"    "Output the error"       fp False
    <*> petscOptionsInt   "-dim" "Topological mesh dimension"       fp 2
    <*> petscOptionsBool  "-interpolate" "Use intermediate cells"   fp False
    <*> petscOptionsBool  "-simplex" "Use simplicial elements"      fp True
    <*> petscOptionsReal  "-refinement_limit" "Maximal cell volume" fp 0
    <*> petscOptionsBool  "-test_partition" "Use a fixed partition" fp False
    <*> petscOptionsEList "-bc_type" "<neumann|dirichlet>"          fp bc bd

createMesh :: MonadIO m => Opts -> m DM
createMesh opts = do
  petscLogEventBegin (logev opts) NoObj NoObj NoObj NoObj
  let dim   = topodim opts
      intp  = interp opts
      cells = Vec.replicate 3 3
  dm0 <- if simplex opts
         then dmPlexCreateBoxMesh CommWorld dim intp
         else dmPlexCreateHexBoxMesh CommWorld dim cells BdyNone BdyNone BdyNone
  dmPlexSetRefinementLimit dm0 $ rlimit opts
  dm1 <- dmRefine dm0 CommWorld >>= \dm' -> case dm' of
    NoDM -> return dm0
    _    -> destroy dm0 >> return dm'
  dm2 <- dmPlexDistribute dm1 0 NoSF >>= \dm' -> case dm' of
    NoDM -> return dm1
    _    -> destroy dm1 >> return dm'
  petscLogEventEnd   (logev opts) NoObj NoObj NoObj NoObj
  return dm2

setupProblem :: MonadIO m => DM -> Opts -> m Opts
setupProblem dm opts = liftIO $ do
  prob <- dmGetDS dm
  petscDSSetResidual prob 0 f0_u f1_u
  petscDSSetResidual prob 1 f0_p f1_p
  petscDSSetJacobian prob 0 0 NoDSJac NoDSJac NoDSJac g3_uu
  petscDSSetJacobian prob 0 1 NoDSJac NoDSJac g3_up NoDSJac
  petscDSSetJacobian prob 1 0 NoDSJac g3_pu NoDSJac NoDSJac
  return $ opts { exactfns = case topodim opts of
                   2 -> [quadratic_u_2d, linear_p_2d]
                   3 -> [quadratic_u_3d, linear_p_3d] }

setupDiscretisation :: MonadIO m => DM -> Opts -> m ()
setupDiscretisation dm opts = liftIO $ do
  let dim = topodim opts
      bct = bctype opts == Dirichlet
      lab = if bct then "marker" else "boundary"
      pid = Vec.singleton 1
  fe0 <- petscFECreateDefault dm dim dim (simplex opts) "vel_" (-1)
  setName fe0 "velocity"
  q   <- petscFEGetQuadrature fe0
  ord <- petscQuadratureGetOrder q
  fe1 <- petscFECreateDefault dm dim 1 (simplex opts) "pres_" ord
  setName fe1 "pressure"
  let go NoDM = return ()
      go cdm  = do
        prob <- dmGetDS cdm
        petscDSSetDiscretization prob 0 fe0
        petscDSSetDiscretization prob 1 fe1
        opt' <- setupProblem cdm opts
        dmAddBoundary cdm bct "wall" lab 0 0 nullPtr (exactfns opt'!!0) 1 pid opt'
        dmGetCoarseDM cdm >>= go
  go dm
  destroy fe1
  destroy fe0

------------------------------------------------------------------------------
createPressureNullSpace :: MonadIO m => DM -> Opts -> m (Vec, MatNullSpace)
createPressureNullSpace dm opts = liftIO $ do
  return (NoVec, NoMatNullSpace)


------------------------------------------------------------------------------
stokesHelp :: String
stokesHelp  = __FILE__ ++
  "Stokes' problem in 2D & 3D, and using a parallel unstructured mesh."

main :: IO ()
main  = do
  argv <- ("main":) <$> System.getArgs
  withPETSc argv stokesHelp $ do
    opts <- parseOptions
    mesh <- createMesh opts
    -- ^ add a mesh-viewer:
    view <- fst <$> petscOptionsGetViewer CommWorld "" "-dm_view"
    dmView mesh view
    -- ^ clean-up:
    destroy view
    destroy mesh
