{-# LANGUAGE CPP, DeriveGeneric #-}

------------------------------------------------------------------------------
-- |
-- Module      : Fields
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Creates a parallel mesh, and then views it.
-- 
-- Changelog:
--  + 14/08/2016  --  initial file;
-- 
-- TODO:
--  + add a field, and then see what happens (after write) to the local data
--    when `overlap > 0`;
-- 
------------------------------------------------------------------------------

module Main where

import GHC.Generics (Generic)
import qualified System.Environment as System
import Control.Monad.IO.Class
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec

import Bindings.Numeric.PETSc


------------------------------------------------------------------------------
data Opts =
  Opts { simplex, interp :: Bool, topodim, elems, overlap, debug :: Int
       , logev :: LogEvent }
  deriving (Eq, Show, Generic)


------------------------------------------------------------------------------
parseOptions :: MonadIO m => m Opts
parseOptions  = liftIO $ do
  let fp = __FILE__
  petscOptions "" "Fields-example options." "DMPLEX" $ Opts
    <$> option "-simplex"     "Use simplicial elements"    fp False
    <*> option "-interpolate" "Use intermediate cells"     fp False
    <*> option "-dim"         "Topological mesh dimension" fp 2
    <*> option "-elements"    "Elements per dimension"     fp 3
    <*> option "-overlap"     "Element overlap amount"     fp 0
    <*> option "-debug"       "Debugging level"            fp 0
    <*> petscLogEventRegister "CreateMesh" DMClassId

------------------------------------------------------------------------------
createMesh :: MonadIO m => Opts -> m DM
createMesh opts = liftIO $ do
  petscLogEventBegin (logev opts) NoObj NoObj NoObj NoObj
  let els = Vec.replicate dim $ elems opts
      dim = topodim opts
  dm0 <- case simplex opts of
    True -> dmPlexCreateBoxMesh CommWorld dim (interp opts)
    _    -> dmPlexCreateHexBoxMesh CommWorld els BdyNone BdyNone BdyNone
  dm1 <- dmPlexDistribute dm0 (overlap opts) NoSF >>= \dm' -> case dm' of
    NoDM -> return dm0
    _    -> putStrLn "desroying DM..." >> destroy dm0 >> return dm'
  petscLogEventEnd (logev opts) NoObj NoObj NoObj NoObj
  return dm1


------------------------------------------------------------------------------
fieldsHelp :: String
fieldsHelp  = __FILE__ ++
  "Fields example in 2D & 3D, and using a parallel unstructured mesh."

main :: IO ()
main  = do
  argv <- ("main":) <$> System.getArgs
  withPETSc argv fieldsHelp $ do
    opts <- parseOptions
    print opts
    mesh <- createMesh opts
    -- ^ add a mesh-viewer:
    view <- fst <$> petscOptionsGetViewer CommWorld "" "-dm_view"
    dmView mesh view
    -- ^ view the Jacobian matrix structure, for the mesh:
    matx <- dmCreateMatrix mesh
    setUp matx
    dmView mesh view
    -- ^ clean-up:
    destroy view
    destroy mesh
