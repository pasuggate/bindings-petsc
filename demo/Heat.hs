{-# LANGUAGE CPP, ForeignFunctionInterface, RankNTypes, ScopedTypeVariables,
             TypeFamilies, GADTs, KindSignatures, DataKinds, PolyKinds,
             PatternSynonyms, ViewPatterns
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Heat
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Time-dependent PDE in 2D , based on examples 7 & 13 from PETSc.
-- 
-- Changelog:
--  + 08/07/2017  --  initial file;
--
-- FIXME:
--  + not threadsafe?
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Main where

import qualified System.Environment as System
import Control.Monad (when)
import Text.Printf

import Data.Vector.Helpers
import Bindings.Numeric.PETSc
import Bindings.Numeric.PETSc.Internal (mkBoxOfToys, mkVecOfPtrs)

import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal
import Foreign.C.Types
import Bindings.Numeric.PETSc.Internal.Types
import qualified Data.Vector as Box
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec


type Box = Box.Vector

foreign export ccall rhsFun :: TS -> PReal -> Vec -> Vec -> PCtx -> IO PErr
foreign export ccall rhsJac :: TS -> PReal -> Vec -> Mat -> Mat -> PCtx -> IO PErr


-- * Callbacks for the solver.
------------------------------------------------------------------------------
rhsFun :: TS -> PReal -> Vec -> Vec -> PCtx -> IO PErr
rhsFun ts t vecU vecF ctx = do
  da   <- tsGetDM ts
  vecL <- dmGetLocalVector da
  (m, n, _) <- dmDAGetInfoNums da
  let (hx, sx) = (recip $ fromIntegral (m-1), recip $ hx*hx)
      (hy, sy) = (recip $ fromIntegral (n-1), recip $ hy*hy)

  -- ^ scatter+gather points to the local vectors:
  --   NOTE: Schedule tasks between these functions, to hide the costs of the
  --     gather/scatter.
  dmGlobalToLocalBegin da vecU InsertValues vecL
  dmGlobalToLocalEnd   da vecU InsertValues vecL

  -- ^ get local grid boundaries, and extract the arrays from the vectors:
  ((xs, ys, _), (xm, ym, _)) <- dmDAGetCorners da
  let s = if ys == 0 then 0 else ys-1
      w = if ys > 0 && ys+ym < n then ym+2 else (if ym == n then ym else ym+1)
  uarr <- dmDAVecGetArrayRead da vecL
  farr <- dmDAVecGetArray     da vecF
  ubox <- mkBoxOfToys s w m uarr
  fvec <- mkVecOfPtrs n farr

  -- ^ compute function over the locally-owned part of the grid:
  let ix = efn xs xm
      jx = efn ys ym
  Vec.mapM_ `flip` jx $ \j -> do
    Vec.mapM_ `flip` ix $ \i -> do
      let u = ubox!j!i
      if i == 0 || j == 0 || i == m-1 || j == n-1 then
        do pokeElemOff (fvec!j) i u else
        do let uxx = (ubox!j!(i-1) + ubox!j!(i+1) - 2*u)*sx
               uyy = (ubox!(j-1)!i + ubox!(j+1)!i - 2*u)*sy
           pokeElemOff (fvec!j) i $ uxx+uyy

  dmDAVecRestoreArray     da vecF farr
  dmDAVecRestoreArrayRead da vecL uarr
  dmRestoreLocalVector    da vecL
  return 0

rhsJac :: TS -> PReal -> Vec -> Mat -> Mat -> PCtx -> IO PErr
rhsJac ts t vecU matJ matP ctx = do
  da   <- tsGetDM ts
  info <- dmDAGetLocalInfo da
  let (hx, sx) = (recip $ fromIntegral (_mx info-1), recip $ hx*hx)
      (hy, sy) = (recip $ fromIntegral (_my info-1), recip $ hy*hy)
      (xs, xm) = (fromIntegral $ _xs info, fromIntegral $ _xm info)
      (ys, ym) = (fromIntegral $ _ys info, fromIntegral $ _ym info)
      (mx, my) = (fromIntegral $ _mx info, fromIntegral $ _my info)
      (ix, jx) = (efn xs xm, efn ys ym)
  Vec.mapM_ `flip` jx $ \j -> do
    Vec.mapM_ `flip` ix $ \i -> do
      let row = Vec.singleton $ MatStencil 0 j i 0
      if i == 0 || j == 0 || i == mx-1 || j == my-1 then
        do let col = Vec.singleton $ MatStencil 0 j i 0
               val = Vec.singleton 1
           matSetValuesStencil matP row col val InsertValues else
        do let cols = Vec.fromList
                      [MatStencil 0 j (i-1) 0, MatStencil 0 j (i+1) 0,
                       MatStencil 0 (j-1) i 0, MatStencil 0 (j+1) i 0,
                       MatStencil 0 j i 0]
               vals = Vec.fromList [sx, sx, sy, sy, -2*(sx+sy)]
           matSetValuesStencil matP row cols vals InsertValues

  matAssemblyBegin matP MAT_FINAL_ASSEMBLY
  matAssemblyEnd   matP MAT_FINAL_ASSEMBLY
  whn (matJ /= matP) $ do
    matAssemblyBegin matJ MAT_FINAL_ASSEMBLY
    matAssemblyEnd   matJ MAT_FINAL_ASSEMBLY
  return 0

initJac :: DM -> Mat -> Mat -> IO ()
initJac da matJ matP = do
  info <- dmDAGetLocalInfo da
  let (hx, sx) = (recip $ fromIntegral (_mx info-1), recip $ hx*hx)
      (hy, sy) = (recip $ fromIntegral (_my info-1), recip $ hy*hy)
      (xs, xm) = (fromIntegral $ _xs info, fromIntegral $ _xm info)
      (ys, ym) = (fromIntegral $ _ys info, fromIntegral $ _ym info)
      (mx, my) = (fromIntegral $ _mx info, fromIntegral $ _my info)
      (ix, jx) = (efn xs xm, efn ys ym)
  Vec.mapM_ `flip` jx $ \j -> do
    Vec.mapM_ `flip` ix $ \i -> do
      let row = Vec.singleton $ MatStencil 0 j i 0
      if i == 0 || j == 0 || i == mx-1 || j == my-1 then
        do let col = Vec.singleton $ MatStencil 0 j i 0
               val = Vec.singleton 1
           matSetValuesStencil matP row col val InsertValues else
        do let cols = Vec.fromList
                      [MatStencil 0 j (i-1) 0, MatStencil 0 j (i+1) 0,
                       MatStencil 0 (j-1) i 0, MatStencil 0 (j+1) i 0,
                       MatStencil 0 j i 0]
               vals = Vec.fromList [sx, sx, sy, sy, -2*(sx+sy)]
           matSetValuesStencil matP row cols vals InsertValues

  matAssemblyBegin matP MAT_FINAL_ASSEMBLY
  matAssemblyEnd   matP MAT_FINAL_ASSEMBLY
  whn (matJ /= matP) $ do
    matAssemblyBegin matJ MAT_FINAL_ASSEMBLY
    matAssemblyEnd   matJ MAT_FINAL_ASSEMBLY
  matStoreValues matP
  matStoreValues matJ

rhsJac' :: TS -> PReal -> Vec -> Mat -> Mat -> PCtx -> IO PErr
rhsJac' ts t vecU matJ matP ctx = do
  matRetrieveValues matP
  matRetrieveValues matJ
  matAssemblyBegin matP MAT_FINAL_ASSEMBLY
  matAssemblyEnd   matP MAT_FINAL_ASSEMBLY
  whn (matJ /= matP) $ do
    matAssemblyBegin matJ MAT_FINAL_ASSEMBLY
    matAssemblyEnd   matJ MAT_FINAL_ASSEMBLY
  return 0

------------------------------------------------------------------------------
-- | Set the initial values.
formInitialSolution :: DM -> Vec -> PCtx -> IO PErr
formInitialSolution da vecU ctx = do
  (m, n, _) <- dmDAGetInfoNums da
  let hx = recip $ fromIntegral (m-1)
      hy = recip $ fromIntegral (n-1)
  uarr <- dmDAVecGetArray da vecU
  ((xs, ys, _), (xm, ym, _)) <- dmDAGetCorners da
  c <- peek $ castPtr ctx
  let ix = efn xs xm
      jx = efn ys ym
  Vec.mapM_ `flip` jx $ \j -> do
    let y = fromIntegral j * hy
    p <- peekElemOff uarr j
    Vec.mapM_ `flip` ix $ \i -> do
      let x = fromIntegral i * hx
          r = sqrt $ (x-0.5)^2 + (y-0.5)^2
          a = exp $ c*r^3
      if r < 0.125
        then pokeElemOff p i a
        else pokeElemOff p i 0
  dmDAVecRestoreArray da vecU uarr
  return 0


-- * Heat-equation simulator.
------------------------------------------------------------------------------
heatHelp = __FILE__ ++ "\t Heat equation simulation." :: String

main :: IO ()
main  = do
  argv <- ("main":) <$> System.getArgs
  rank <- withPETSc argv heatHelp $ do
    -- ^ register additional options:
    (draw, save) <- petscOptions "" "Heat Options" "IGA" $ do
      let dhlp = "If dim <= 2, then draw the solution"
          shlp = "Save the solution to file"
      draw <- petscOptionsBool "-draw" dhlp __FILE__ False
      save <- petscOptionsBool "-save" shlp __FILE__ False
      return (draw, save)

    comm <- petscCommWorld
    rank <- mpiCommRank comm
    when (rank == 0) $ do
      printf "Options: \tdraw = %s \tsave = %s\n" (show draw) (show save)

--     let nel = 8
--     let nel = 64
--     let nel = 256
    let nel = 1024
    da   <- dmDACreate2d DM_BOUNDARY_NONE DM_BOUNDARY_NONE DMDA_STENCIL_STAR nel nel 1 1
    dmSetFromOptions da
    dmSetUp da

    -- ^ extract global vectors from the DMDA:
    vecU <- dmCreateGlobalVector da
    vecR <- vecDuplicate vecU

    -- ^ create the time-stepping solver context:
    ts   <- tsCreate comm
    tsSetDM ts da
    tsSetType ts TSBEULER
    tsSetProblemType ts TSLinear -- TSNonlinear
    tsSetRHSFunction ts vecR rhsFun nullPtr -- user

    -- ^ setup the Jacobian:
    dmSetMatType da MATAIJ
    matJ <- dmCreateMatrix da
    initJac da matJ matJ
    tsSetRHSJacobian ts matJ matJ rhsJac' nullPtr

    let maxsteps = 1000
        ftime = 1.0
        dt = 0.01
    tsSetDuration ts maxsteps ftime
    tsSetExactFinalTime ts TSFinalTimeStepOver

    -- ^ set initial conditions:
    with (-30.0 :: CDouble) $ \p -> do
      formInitialSolution da vecU $ castPtr p
    tsSetInitialTimeStep ts 0.0 dt
    tsSetFromOptions ts

    -- ^ solve by time-stepping:
    tsSolve ts vecU
    st <- tsGetSolveTime ts
    sn <- tsGetTimeStepNumber ts

    -- ^ write solution as a VTK file?
    when (rank == 0) $ printf "Final time = %g (steps = %d)\n" st sn
    when save $ do
      view <- petscViewerVTKOpen comm "HeatSolution.vts" FileWrite
      vecView vecU view
      petscViewerDestroy view

    matDestroy matJ
    vecDestroy vecR
    vecDestroy vecU
    tsDestroy ts
    dmDestroy da
    return rank
  when (rank == 0) $ printf "done\n"
