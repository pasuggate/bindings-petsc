
------------------------------------------------------------------------------
-- |
-- Module      : KSP-Ex19
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Driven cavity problem, ported for KSP tutorial `ex19.c`.
-- 
-- Changelog:
--  + 29/06/2016  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Main where

import Text.Printf


main :: IO ()
main  = do
  printf "\n> Haskell port of KSP tutorial `ex19.c` <\n\n"
