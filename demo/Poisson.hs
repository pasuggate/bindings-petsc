{-# LANGUAGE CPP #-}
module Main where

import qualified System.Environment as System
-- import Data.String
import Text.Printf

import Bindings.Numeric.PETSc


main :: IO ()
main  = do
  let help = "Hi\n\n"
  argv <- System.getArgs
  withPETSc argv help $ do
    -- ^ register additional options:
    let prfx = ""
        desc = "Poisson Options"
        mmod = "IGA"

    (draw, save) <- petscOptions prfx desc mmod $ do
      let dhlp = "If dim <= 2, then draw the solution"
          shlp = "Save the solution to file"
          file = "Poisson.hs"
      draw <- petscOptionsBool "-draw" dhlp __FILE__ False
      save <- petscOptionsBool "-save" shlp __FILE__ False
      return (draw, save)

    printf "draw: %s\n" $ show draw
    printf "save: %s\n" $ show save

    comm <- petscCommWorld
    matA <- matCreate comm

    matDestroy matA

    return ()
  printf help
