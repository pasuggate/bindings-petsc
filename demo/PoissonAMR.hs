{-# LANGUAGE CPP, DeriveGeneric #-}

------------------------------------------------------------------------------
-- |
-- Module      : PoissonAMR
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Poisson problem in 2D with tensor-product finite elements, and using AMR
-- (Adaptive Mesh Refinement), via the parallel forests of `p4est`, and
-- PETSc's `DMForest'.
-- 
-- Changelog:
--  + 06/08/2016  --  initial file;
-- 
-- TODO:
--  + move to `bindings-p4est`?
-- 
------------------------------------------------------------------------------

module Main where

import GHC.Generics (Generic)
import qualified System.Environment as System
import Control.Monad.IO.Class
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec

import Bindings.Numeric.PETSc


------------------------------------------------------------------------------
-- | User options.
data Opts = Opts { interp  :: Bool
                 , fname   :: String
                 , topodim :: Z
                 , spdim   :: Z
                 , bctype  :: BCType
--                  , exactfns    :: Vector R -> Vector R -> Opts -> IO ()
                 } deriving (Eq, Show, Generic)

data BCType = Neumann | Dirichlet
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic)


-- * Potential functions?
------------------------------------------------------------------------------
zero_scalar :: V3 R -> Vector R -> Opts -> Vector R
zero_scalar _ xs _ = xs Vec.// [(0, 0.0)]

zero_vector :: V3 R -> Vector R -> Opts -> Vector R
zero_vector _ xs opts = let us = [ (i, 0.0) | i <- [0..spdim opts-1] ]
                        in  xs Vec.// us

quadratic_u_2d :: V3 R -> Vector R -> Opts -> Vector R
quadratic_u_2d (V3 x y _) xs = xs Vec.// [(0, x*x + y*y)]


------------------------------------------------------------------------------
-- | Register additional command-line options, and then parse the command-line
--   options.
parseOptions :: MonadIO m => Comm -> m Opts
parseOptions comm = liftIO $ do
  petscOptions "" "Poisson AMR options" "DMFOREST" $ do
    let ihlp = "Use interpolation?"
        fhlp = "Filename for export"
        dhlp = "Topological mesh dimension"
        bhlp = "Type of boundary condition"
        blst = ["neumann", "dirichlet"]
        bdft = blst !! fromEnum Dirichlet
    Opts <$> petscOptionsBool   "-interpolate" ihlp __FILE__ False
         <*> petscOptionsString "-filename"    fhlp __FILE__ "mesh.exo"
         <*> petscOptionsInt    "-dim"         dhlp __FILE__ 2
         <*> pure 2
         <*> petscOptionsEList  "-bc_type"     bhlp __FILE__ blst bdft

createMesh :: MonadIO m => Comm -> Opts -> m DM
createMesh comm opts = do
  let cells = Vec.replicate 3 3
      bt    = DM_BOUNDARY_NONE
  plex <- dmPlexCreateHexBoxMesh comm cells bt bt bt
  -- ^ (re)distribute the mesh amongst all nodes:
  dm   <- dmPlexDistribute plex 0 NoSF >>= \tdm -> case tdm of
    NoDM -> return plex
    _    -> destroy plex >> return tdm
  setFromOptions dm
  viewFromOptions dm NoObject "-dm_view"
  return dm

setupProblem :: MonadIO m => DM -> Opts -> m ()
setupProblem dm opts = do
  prob <- dmGetDS dm
  petscDSSetResidual prob 0 (Just f0_u) (Just f1_u)
  petscDSSetJacobian prob 0 0 NoDSJac NoDSJac NoDSJac (Just g3_uu)
  return ()

setupDiscretisation :: DM -> Opts -> m ()
setupDiscretisation dm opts = do
  setupProblem dm opts
  return ()

------------------------------------------------------------------------------
-- | 
refineFn :: MonadIO m => Ptr P4est -> P4Idx -> Ptr P4Quad -> m CInt
refineFn p4est top quads = liftIO $ do
  return 0


------------------------------------------------------------------------------
pamrHelp = "Solver a Poisson problem using AMR\n" :: String

main :: IO ()
main  = do
  argv <- ("main":) <$> System.getArgs
  withPETSc argv meshHelp $ do
    comm <- petscCommWorld
    opts <- parseOptions comm
    mesh <- createMesh comm opts
    setupDiscretisation mesh opts
    destroy mesh
