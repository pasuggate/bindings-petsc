{-# LANGUAGE CPP, ForeignFunctionInterface, RankNTypes, ScopedTypeVariables,
             TypeFamilies, GADTs, KindSignatures, DataKinds, PolyKinds,
             PatternSynonyms, ViewPatterns
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : MathFun
-- Copyright   : (C) Patrick Suggate 2019
-- License     : BSD3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Mathematical functions, and based on the example:
--
--   https://www.mcs.anl.gov/petsc/petsc-current/src/dm/examples/tutorials/ex4.c.html
-- 
-- Changelog:
--  + 03/08/2019  --  initial file;
--
-- TODO:
-- 
------------------------------------------------------------------------------

module Main where

import qualified System.Environment as System
import Control.Monad (when)
import Text.Printf

import Data.Vector.Helpers
import Bindings.Numeric.PETSc
import Bindings.Numeric.PETSc.Internal (mkBoxOfToys, mkVecOfPtrs)

import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal
import Foreign.C.Types
import Bindings.Numeric.PETSc.Internal.Types
import qualified Data.Vector as Box
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec


-- * Heat-equation simulator.
------------------------------------------------------------------------------
mathHelp = __FILE__ ++ "\t Mathematical functions." :: String

main :: IO ()
main  = do
  argv <- ("main":) <$> System.getArgs
  rank <- withPETSc argv mathHelp $ do
    comm <- petscCommWorld
    rank <- mpiCommRank comm
    nInt <- petscOptions "" "Math Options" "FUN" $ do
      let nhlp = "Choose 'n'"
      petscOptionsInt "-n" nhlp __FILE__ 20
    when (rank == 0) $ do
      printf "> Number of elements:\t%d\n" nInt

    vecX <- vecCreate comm
    vecSetSizes vecX PDecide (fromIntegral nInt)
    vecSetFromOptions vecX

    vecY <- vecDuplicate vecX
    vecW <- vecDuplicate vecX

    vecSet vecX 1.0
    vecSet vecY 2.0
    
    vecDestroy vecW
    vecDestroy vecY
    vecDestroy vecX

    pure rank
  printf "Goodbye from #%d\n" rank
