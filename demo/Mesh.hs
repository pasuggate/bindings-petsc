{-# LANGUAGE CPP, DeriveGeneric #-}
module Main where

import GHC.Generics (Generic)
import qualified System.Environment as System
import Control.Monad.IO.Class
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
import Bindings.Numeric.PETSc


------------------------------------------------------------------------------
-- | User options.
data Opts = Opts { interp :: Bool
                 , fname  :: String
                 } deriving (Eq, Show, Generic)


------------------------------------------------------------------------------
-- | Register additional command-line options, and then parse the command-line
--   options.
parseOptions :: MonadIO m => Comm -> m Opts
parseOptions comm = liftIO $ do
  petscOptions "" "PlexField options" "PF" $ do
    let ihlp = "Use interpolation?"
        fhlp = "Filename for export"
    Opts <$> petscOptionsBool   "-interpolate" ihlp __FILE__ False
         <*> petscOptionsString "-filename"    fhlp __FILE__ "mesh.exo"

createMesh :: MonadIO m => Comm -> Opts -> m DM
createMesh comm opts = do
  dm <- dmPlexCreateFromFile comm (fname opts) (interp opts)
  setName dm "Mesh"
  viewFromOptions dm NoObject "-dm_view"
  return dm

checkTopology :: MonadIO m => DM -> m ()
checkTopology dm = liftIO $ do
  dim   <- dmGetDimension dm
  start <- fst <$> dmPlexGetHeightStratum dm 0
  csize <- dmPlexGetConeSize dm start
  let simplex = csize == dim+1
  dmPlexCheckSymmetry dm
  dmPlexCheckSkeleton dm simplex 0
  dmPlexCheckFaces    dm simplex 0

-- TODO:
checkGeometry :: MonadIO m => DM -> m ()
checkGeometry dm = liftIO $ do
  dim   <- dmGetDimension dm
  (s,e) <- dmPlexGetHeightStratum dm 0
  csize <- dmPlexGetConeSize dm s
  -- TODO: see `ex2.c` of the plex tutorials.
  return ()

meshHelp = "Read in a mesh and test whether it is valid\n" :: String

main :: IO ()
main  = do
  argv <- ("main":) <$> System.getArgs
  withPETSc argv meshHelp $ do
    comm <- petscCommWorld
    opts <- parseOptions comm
    mesh <- createMesh comm opts
    checkTopology mesh
    checkGeometry mesh
    destroy mesh
