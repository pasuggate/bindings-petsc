# Installation #

```bash
  $ sudo aptitude install petsc-dev
  $ cabal new-build -j petsc-demo --extra-lib-dirs=/usr/lib/x86_64-linux-gnu/hdf5/openmpi/
```

## Download ##

If manually installing (rather than from your package repositories) then it is probably best to use the version from Bitbucket::

```bash
  $ git clone https://bitbucket.org/petsc/petsc.git
```

## Configure ##

PETSc was configured with additional meshing options:

```bash
  $ sudo aptitude install cmake
  $ ./configure --download-triangle --download-ctetgen --download-hdf5 \
      --download-exodusii --download-netcdf --with-zlib --download-p4est \
      --download-pnetcdf
```

And in `.bashrc`, set the location of PETSc, target architecture, and then set the `LD_LIBRARY_PATH`:

```bash
  export PETSC_DIR=/opt/petsc
  export PETSC_ARCH=arch-linux2-c-debug
  export LD_LIBRARY_PATH=$PETIGA_DIR/$PETSC_ARCH/lib:$PETSC_DIR/$PETSC_ARCH/lib:$LD_LIBRARY_PATH
```

GCC then seems to correctly find the PETSc libraries.
