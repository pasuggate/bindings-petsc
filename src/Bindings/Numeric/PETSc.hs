{-# LANGUAGE Arrows, TypeOperators, PatternSynonyms, ViewPatterns, CPP,
             FlexibleContexts, TupleSections,
             MultiParamTypeClasses, FunctionalDependencies,
             FlexibleInstances, TypeSynonymInstances,
             GeneralizedNewtypeDeriving,
             DeriveFunctor, DeriveFoldable, DeriveTraversable, DeriveGeneric,
             GADTs, KindSignatures, DataKinds, PolyKinds,
             TypeFamilies, RankNTypes, ScopedTypeVariables,
             UndecidableInstances,
             TemplateHaskell, ForeignFunctionInterface,
             NoMonomorphismRestriction
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc
-- Copyright   : (C) Patrick Suggate 2016
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Mid-level bindings to PETSc.
-- 
-- Changelog:
--  + 04/10/2016  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc
       ( -- ^ core PETSc types bindings:
         PetscT(..), pattern PetscBool, pattern PetscInt,
         pattern PetscDouble, pattern PetscString,
         PInt, PReal, PScalar, PCtx, PErr,
         Comm, pattern NoComm, pattern CommWorld, pattern PetscCommWorld,
         ClassId, pattern NoClassId, pattern DMClassId,
         LogEvent, pattern NoLogEvent,
         PetscErrorCode(..), IsPetscObject(..),
         CopyMode(..),
         pattern CopyValues, pattern OwnPointer, pattern UsePointer,
         pattern NoObject, pattern NoObj,
         pattern NoDM, pattern NoVec, pattern NoMat,
         pattern NoMatNullSpace,
--          Jaco, Func,
--          pattern PDecide, pattern PDetermine,
         PFileMode(..),
         pattern FileRead, pattern FileWrite, pattern FileAppend,
         pattern FileUpdate, pattern FileAppendUpdate,
         -- ^ MPI types & functions:
         MInt, mpiCommRank,
         StarForest(..), PetscSF, PPSF, SF,
         pattern NoSF, noSF,
         -- ^ core PETSc functions:
         withPETSc, petscCommWorld,
         petscInitialise, petscFinalise,
         makeArgv,
         petscMemzero, petscFree,
         fromArrayM, toArrayM,
         petscFunctionBeginUser,
--          petscFunctionReturn,
         petscSuccess,
         petscSleep,
         -- ^ some viewers stuff:
         Viewer(..), ViewerT(..),
         petscCitationsRegister, petscLogEventRegister,
         petscLogEventBegin, petscLogEventEnd,
         -- ^ index-sets and sections:
         Section, IS,
         pattern NoIS, pattern NoSection,
         -- ^ and a bunch of submodules:
         module Bindings.Numeric.PETSc.Options,
         module Bindings.Numeric.PETSc.Vec,
         module Bindings.Numeric.PETSc.Mat,
         module Bindings.Numeric.PETSc.SNES,
         module Bindings.Numeric.PETSc.KSP,
         module Bindings.Numeric.PETSc.IS,
         module Bindings.Numeric.PETSc.DM,
         module Bindings.Numeric.PETSc.TS,
         module Bindings.Numeric.PETSc.View,
         module Bindings.Numeric.PETSc.DMPlex,
#ifdef __WITH_P4EST
         module Bindings.Numeric.PETSc.DMForest,
#endif /* __WITH_P4EST */
         module Bindings.Numeric.PETSc.Partition,
{-- }
         -- ^ PETSc options functionality:
         petscOptionsBegin, petscOptionsEnd,
         petscOptions,
         petscOptionsBool,
         -- ^ Vector types & bindings:
         Vec, VecType,
         pattern VecSEQ, pattern VecMPI, pattern VecStandard,
         fromVector, fillVec, toVector,
         vecCreate, vecDestroy, vecSetType, vecSetSizes,
         withVecArray,
         vecSet, vecSetValues, vecAddValues,
         vecSetFromOptions,
         vecEmpty, vecNULL, vecZeros, vecDuplicate,
         vecView,
         -- ^ Matrix types & bindings:
         Mat, MatType,
         matSetSizes, matDestroy,
         matSetUp, matEmpty, matNULL,
         -- ^ Matrix-free bindings:
         matCreateSNESMF, matCreateMFFD,
         -- ^ SNES types & bindings:
         SNES, SNESType,
         pattern SNESNewtonLS, pattern SNESNCG, pattern SNESNGMRES,
         withSNES, snesCreate, snesDestroy,
         snesSetFunction, snesSetJacobian, snesSolve,
         -- ^^ SNES setttings:
         snesSetFromOptions, snesGetSolution,
--          snesSetIterationNumber,
         snesGetTolerances, snesSetTolerances,
         -- ^^ KSP data-types & bindings:
         KSP, KSPType,
         kspCreate, kspDestroy,
         -- ^^ SNES solver-function helpers:
         liftFunc, liftFuncIO, liftJacoIO, mffdJaco,
         snesComputeJacobianDefault, snesComputeJacobianDefaultColor,
         matMFFDComputeJacobian,
         -- ^^ DM data-types & bindings:
         DM, BdyT, StencilT,
         dmdaCreate2d, dmDestroy
--}
       ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal.Array
import Foreign.Marshal.Utils
import Foreign.C.Types
import Foreign.C.String
import System.IO.Unsafe
import Control.Monad
import Control.Monad.Primitive
import Data.Foldable
import Data.Bool (bool)
import Data.String
import qualified Data.Vector.Generic as G
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vector
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Storable.Mutable as Mut

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Core
-- import Bindings.Numeric.PETSc.Internal.View
-- import Bindings.Numeric.PETSc.Internal.SNES
-- import Bindings.Numeric.PETSc.Internal.Vec
-- import Bindings.Numeric.PETSc.Internal.Mat
-- import Bindings.Numeric.PETSc.Internal.KSP
-- import Bindings.Numeric.PETSc.Internal.DM

import Bindings.Numeric.PETSc.Internal
import Bindings.Numeric.PETSc.Options
import Bindings.Numeric.PETSc.View
import Bindings.Numeric.PETSc.Vec
import Bindings.Numeric.PETSc.Mat
import Bindings.Numeric.PETSc.SNES
import Bindings.Numeric.PETSc.KSP
import Bindings.Numeric.PETSc.IS
import Bindings.Numeric.PETSc.DM
import Bindings.Numeric.PETSc.TS
import Bindings.Numeric.PETSc.DMPlex
#ifdef __WITH_P4EST
import Bindings.Numeric.PETSc.DMForest
#endif /* __WITH_P4EST */
import Bindings.Numeric.PETSc.Partition


{-- }
-- * PETSc options functionality.
------------------------------------------------------------------------------
petscOptions :: String -> String -> String -> IO a -> IO a
petscOptions prfx desc mmod actn = do
  comm <- petscCommWorld
  petscOptionsBegin comm prfx desc mmod
  vals <- actn
  petscOptionsEnd
  return vals

petscOptionsBegin :: Comm -> String -> String -> String -> IO ()
petscOptionsBegin comm prfx desc mmod = do
  let prfx' = fromString prfx
      desc' = fromString desc
      mmod' = fromString mmod
  c'PetscOptionsBeginC comm prfx' desc' mmod' >>= chkErr

petscOptionsEnd :: IO ()
petscOptionsEnd  = c'PetscOptionsEnd >>= chkErr


-- ** Query and (un)register additional PETSc options.
------------------------------------------------------------------------------
petscOptionsBool :: String -> String -> String -> Bool -> IO Bool
petscOptionsBool opt help file def = do
  let opt' = fromString opt
      hlp' = fromString help
      fil' = fromString file
      def' = pbool def
  with def' $ \p -> do
    c'PetscOptionsBool opt' hlp' fil' def' p nullPtr >>= chkErr
    hbool <$> peek p
--}
