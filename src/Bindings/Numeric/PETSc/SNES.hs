{-# LANGUAGE TypeOperators, PatternSynonyms, ViewPatterns, TupleSections
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.SNES
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Mid-level bindings to the non-linear solver (SNES) module of PETSc.
-- 
-- Changelog:
--  + 11/06/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.SNES
       ( SNES(..), SNESType(..), Func, Jaco
       , pattern PDecide, pattern PDetermine
       , module Bindings.Numeric.PETSc.SNES
       ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal
import System.IO.Unsafe
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vector
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Storable.Mutable as Mut

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.SNES
import Bindings.Numeric.PETSc.Internal
import Bindings.Numeric.PETSc.Vec


-- ** SNES bindings.
------------------------------------------------------------------------------
-- | Create a new SNES context.
snesCreate :: Comm -> IO SNES
snesCreate c = with (SNES nullPtr) $ \s -> do
  c'SNESCreate c s >>= chkErr >> peek s

-- | Destroy a SNES context.
snesDestroy :: SNES -> IO ()
snesDestroy snes = with snes $ \p -> c'SNESDestroy p >>= chkErr

withSNES :: Comm -> (SNES -> IO a) -> IO a
withSNES c k = snesCreate c >>= \snes -> do
  k snes >>= \x -> snesDestroy snes >> return x

------------------------------------------------------------------------------
-- | Set the callback function, and the residual vector, for the nonlinear
--   solver.
snesSetFunction :: SNES -> Vec -> Func -> IO ()
snesSetFunction snes res f =
  c'SNESSetFunction snes res f nullPtr >>= chkErr

-- | Set the callback jacobian function, and the preconditioning matrices, for
--   the nonlinear solver.
snesSetJacobian :: SNES -> Mat -> Mat -> Jaco -> IO ()
snesSetJacobian snes ja jb jf =
  c'SNESSetJacobian snes ja jb jf nullPtr >>= chkErr

-- | Use the current configuration, and the given RHS & initial vectors, to
--   solve the nonlinear system.
snesSolve :: SNES -> Vec -> Vec -> IO ()
snesSolve snes b x = c'SNESSolve snes b x >>= chkErr


-- *** Additional SNES settings.
------------------------------------------------------------------------------
snesSetFromOptions :: SNES -> IO ()
snesSetFromOptions snes = c'SNESSetFromOptions snes >>= chkErr >> return ()

snesGetSolution :: SNES -> IO Vec
snesGetSolution snes = with vecNULL $ \p -> do
  c'SNESGetSolution snes p >>= chkErr >> peek p

snesSetIterationNumber :: SNES -> PInt -> IO ()
snesSetIterationNumber snes n =
  c'SNESSetIterationNumber snes n >>= chkErr >> return ()

snesGetTolerances :: SNES -> IO (PReal, PReal, PReal, PInt, PInt)
snesGetTolerances snes = do
  with 0 $ \a -> do
    with 0 $ \r -> do
      with 0 $ \s -> do
        with 0 $ \i -> do
          with 0 $ \f -> do
            c'SNESGetTolerances snes a r s i f >>= chkErr
            (,,,,) <$> peek a <*> peek r <*> peek s <*> peek i <*> peek f

snesSetTolerances :: SNES -> PReal -> PReal -> PReal -> PInt -> PInt -> IO ()
snesSetTolerances snes atol rtol stol maxit maxf =
  c'SNESSetTolerances snes atol rtol stol maxit maxf >>= chkErr >> return ()


-- *** Solve-function helpers.
------------------------------------------------------------------------------
-- | Lift the residual-evaluation function, so that it can be given to the
--   SNES solver, of PETSc.
liftFunc :: (Vector Double -> Vector Double) -> Func
liftFunc f =
  let go _ x r _ = toVector x >>= flip fillVec r . f >> return 0
  in  unsafePerformIO $ mk'F go

liftFuncIO :: (Vector Double -> IO (Vector Double)) -> IO Func
liftFuncIO f =
  let go _ x r _ = toVector x >>= f >>= flip fillVec r >> return 0
--   let go _ x r _ = do
--         vecGetSize x >>= print
--         vecGetSize r >>= print
--         toVector x >>= f >>= flip fillVec r >> return 0
  in  mk'F go
