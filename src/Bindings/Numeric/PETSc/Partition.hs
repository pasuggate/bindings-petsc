{-# LANGUAGE CPP, TypeOperators, PatternSynonyms, ViewPatterns, TupleSections
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Partition
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Mid-level bindings to the `Vec` objects of PETSc.
-- 
-- Changelog:
--  + 11/06/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.Partition
       ( Partitioner(..), PartitionerType(..)
       , module Bindings.Numeric.PETSc.Partition
       ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.C.String
import Foreign.Marshal
import Control.Monad.IO.Class

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Partition
import Bindings.Numeric.PETSc.Internal


-- * Convenience patterns for built-in partitioners and types.
------------------------------------------------------------------------------
-- | For empty/optional partitioners.
pattern NoPartitioner :: Partitioner
pattern NoPartitioner <- ((== Partitioner nullPtr) -> True)
  where NoPartitioner  = Partitioner nullPtr

-- | For empty/optional partitioner-types.
pattern NoPartitionerType :: PartitionerType
pattern NoPartitionerType <- ((== PartitionerType nullPtr) -> True)
  where NoPartitionerType  = PartitionerType nullPtr


-- * Standard partitioner instances.
------------------------------------------------------------------------------
-- | Common `PetscObject` functionality for `Partitioner` objects.
#include "petsc_macro.h"
macro_PETSC_OBJECT_INST(partitioner,Partitioner)


-- * Constructors & destructors.
------------------------------------------------------------------------------
partitionerCreate :: MonadIO m => Comm -> m Partitioner
partitionerCreate comm = liftIO $ with NoPartitioner $ \ptr -> do
  c'PetscPartitionerCreate comm ptr >>= chkErr >> peek ptr

partitionerDestroy :: MonadIO m => Partitioner -> m ()
partitionerDestroy part = liftIO $ with part $ \ptr -> do
  c'PetscPartitionerDestroy ptr >>= chkErr


-- * Additional partitioner setup.
------------------------------------------------------------------------------
partitionerSetFromOptions :: MonadIO m => Partitioner -> m ()
partitionerSetFromOptions part = liftIO $ do
  c'PetscPartitionerSetFromOptions part >>= chkErr

partitionerSetUp :: MonadIO m => Partitioner -> m ()
partitionerSetUp part = liftIO $ do
  c'PetscPartitionerSetUp part >>= chkErr

------------------------------------------------------------------------------
partitionerSetType :: MonadIO m => Partitioner -> PartitionerType -> m ()
partitionerSetType part typ = liftIO $ do
  c'PetscPartitionerSetType part typ >>= chkErr

partitionerGetType :: MonadIO m => Partitioner -> m PartitionerType
partitionerGetType part = liftIO $ with NoPartitionerType $ \ptr -> do
  c'PetscPartitionerGetType part ptr >>= chkErr >> peek ptr
