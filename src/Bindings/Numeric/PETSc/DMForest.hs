{-# LANGUAGE CPP, TypeOperators, PatternSynonyms, ViewPatterns, TupleSections,
             GeneralizedNewtypeDeriving, StandaloneDeriving, DeriveGeneric,
             DeriveDataTypeable, MultiParamTypeClasses, FlexibleInstances
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.DMForest
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Low-level bindings to the PETSc parallel-forests interface.
-- 
-- Changelog:
--  + 20/08/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.DMForest where

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal
import Foreign.C.String
import GHC.Generics (Generic)
import Control.Monad.IO.Class
import Control.Monad.Primitive
import Data.Typeable (Typeable)
import Data.StateVar
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vector
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Storable.Mutable as Mut

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.DM
import Bindings.Numeric.PETSc.Internal.DMForest
import Bindings.Numeric.PETSc.Internal


-- * Convenience patterns for adaptivity-purpose flags.
------------------------------------------------------------------------------
-- | Adaption purpose.
--   TODO: In later versions of PETSc, this has flag has been renamaed, and
--     there is now also a `xx_DETERMINE` purpose.
pattern AdaptKeep :: DMAdapt
pattern AdaptKeep <- ((== DMAdapt c'DM_FOREST_KEEP) -> True)
  where AdaptKeep  = DMAdapt c'DM_FOREST_KEEP

pattern AdaptRefine :: DMAdapt
pattern AdaptRefine <- ((== DMAdapt c'DM_FOREST_REFINE) -> True)
  where AdaptRefine  = DMAdapt c'DM_FOREST_REFINE

pattern AdaptCoarsen :: DMAdapt
pattern AdaptCoarsen <- ((== DMAdapt c'DM_FOREST_COARSEN) -> True)
  where AdaptCoarsen  = DMAdapt c'DM_FOREST_COARSEN

------------------------------------------------------------------------------
-- | Adaption strategy.
pattern AdaptAll :: DMAdaptStrategy
pattern AdaptAll <- ((== DMAdaptStrategy c'DMFORESTADAPTALL) -> True)
  where AdaptAll  = DMAdaptStrategy c'DMFORESTADAPTALL

pattern AdaptAny :: DMAdaptStrategy
pattern AdaptAny <- ((== DMAdaptStrategy c'DMFORESTADAPTANY) -> True)
  where AdaptAny  = DMAdaptStrategy c'DMFORESTADAPTANY


-- * Forest setup functions.
------------------------------------------------------------------------------
dmForestSetBaseDM :: MonadIO m => DM -> DM -> m ()
dmForestSetBaseDM dm base = liftIO $ c'DMForestSetBaseDM dm base >>= chkErr

dmForestGetBaseDM :: MonadIO m => DM -> m DM
dmForestGetBaseDM dm = liftIO $ with NoDM $ \ptr -> do
  c'DMForestGetBaseDM dm ptr >>= chkErr >> peek ptr

------------------------------------------------------------------------------
-- | Checks to see if the DM's type is one of the supported forest types.
dmIsForest :: MonadIO m => DM -> m Bool
dmIsForest dm = liftIO $ with pfalse $ \ptr -> do
  c'DMIsForest dm ptr >>= chkErr >> bpeek ptr

-- | Register the given DM type as subtype of `DMFOREST`.
dmForestRegisterType :: MonadIO m => DMType -> m ()
dmForestRegisterType typ = liftIO $ c'DMForestRegisterType typ >>= chkErr


-- * Distribution & partitioning.
------------------------------------------------------------------------------
dmForestSetPartitionOverlap :: MonadIO m => DM -> Int -> m ()
dmForestSetPartitionOverlap dm o = liftIO $ do
  c'DMForestSetPartitionOverlap dm (fromIntegral o) >>= chkErr

dmForestGetPartitionOverlap :: MonadIO m => DM -> m Int
dmForestGetPartitionOverlap dm = liftIO $ with 0 $ \ptr -> do
  c'DMForestGetPartitionOverlap dm ptr >>= chkErr >> ipeek ptr


-- * Forest topology & connectivity.
------------------------------------------------------------------------------
dmForestSetAdjacencyDimension :: MonadIO m => DM -> Int -> m ()
dmForestSetAdjacencyDimension dm adj = liftIO $ do
  c'DMForestSetAdjacencyDimension dm (fromIntegral adj) >>= chkErr

dmForestGetAdjacencyDimension :: MonadIO m => DM -> m Int
dmForestGetAdjacencyDimension dm = liftIO $ with 0 $ \ptr -> do
  c'DMForestGetAdjacencyDimension dm ptr >>= chkErr >> ipeek ptr

------------------------------------------------------------------------------
dmForestSetAdjacencyCodimension :: MonadIO m => DM -> Int -> m ()
dmForestSetAdjacencyCodimension dm adj = liftIO $ do
  c'DMForestSetAdjacencyCodimension dm (fromIntegral adj) >>= chkErr

dmForestGetAdjacencyCodimension :: MonadIO m => DM -> m Int
dmForestGetAdjacencyCodimension dm = liftIO $ with 0 $ \ptr -> do
  c'DMForestGetAdjacencyCodimension dm ptr >>= chkErr >> ipeek ptr


-- * Forest refinement & coarsening.
------------------------------------------------------------------------------
dmForestSetInitialRefinement :: MonadIO m => DM -> Int -> m ()
dmForestSetInitialRefinement dm i = liftIO $ do
  c'DMForestSetInitialRefinement dm (fromIntegral i) >>= chkErr

dmForestGetInitialRefinement :: MonadIO m => DM -> m Int
dmForestGetInitialRefinement dm = liftIO $ with 0 $ \ptr -> do
  c'DMForestGetInitialRefinement dm ptr >>= chkErr >> ipeek ptr

------------------------------------------------------------------------------
dmForestSetMaximumRefinement :: MonadIO m => DM -> Int -> m ()
dmForestSetMaximumRefinement dm i = liftIO $ do
  c'DMForestSetMaximumRefinement dm (fromIntegral i) >>= chkErr

dmForestGetMaximumRefinement :: MonadIO m => DM -> m Int
dmForestGetMaximumRefinement dm = liftIO $ with 0 $ \ptr -> do
  c'DMForestGetMaximumRefinement dm ptr >>= chkErr >> ipeek ptr

------------------------------------------------------------------------------
dmForestSetMinimumRefinement :: MonadIO m => DM -> Int -> m ()
dmForestSetMinimumRefinement dm i = liftIO $ do
  c'DMForestSetMinimumRefinement dm (fromIntegral i) >>= chkErr

dmForestGetMinimumRefinement :: MonadIO m => DM -> m Int
dmForestGetMinimumRefinement dm = liftIO $ with 0 $ \ptr -> do
  c'DMForestGetMinimumRefinement dm ptr >>= chkErr >> ipeek ptr
