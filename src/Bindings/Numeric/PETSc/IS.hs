{-# LANGUAGE OverloadedStrings, CPP, TemplateHaskell, DeriveGeneric,
             GeneralizedNewtypeDeriving, FlexibleInstances,
             PatternSynonyms, ViewPatterns #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.IS
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Bindings to the Index-Set (IS) module.
-- 
-- NOTE:
-- 
-- Changelog:
--  + 05/08/2017  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.IS where

import Foreign.Storable
import Foreign.Ptr
import Foreign.Marshal
import Foreign.C.String
import GHC.Generics (Generic)
import Control.Monad.IO.Class
import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Core
import Bindings.Numeric.PETSc.Internal.IS
import Bindings.Numeric.PETSc.Internal


-- * IS functions.
------------------------------------------------------------------------------
-- | Common `PetscObject` functionality for `IS` objects.
instance IsPetscObject IS where
  create         = isCreate
  setFromOptions = isSetFromOptions
  setUp          = isSetUp
  destroy        = isDestroy
  {-# INLINE create #-}
  {-# INLINE setFromOptions #-}
  {-# INLINE setUp #-}
  {-# INLINE destroy #-}


-- ** Constructors & destructors.
------------------------------------------------------------------------------
isCreate :: MonadIO m => Comm -> m IS
isCreate comm = liftIO $ with NoIS $ \pis -> do
  c'ISCreate comm pis >>= chkErr >> peek pis

isDestroy :: MonadIO m => IS -> m ()
isDestroy is = liftIO $ with is $ \p -> c'ISDestroy p >>= chkErr


-- ** Additional setup functions.
------------------------------------------------------------------------------
-- TODO: Just log and then ignore?
isSetFromOptions :: MonadIO m => IS -> m ()
-- isSetFromOptions sec = liftIO $ c'ISSetFromOptions sec >>= chkErr
isSetFromOptions  = error $ nosuch __FILE__ __LINE__ "isSetFromOptions"

-- TODO: Just log and then ignore?
isSetUp :: MonadIO m => IS -> m ()
-- isSetUp sec = liftIO $ c'ISSetUp sec >>= chkErr
isSetUp  = error $ nosuch __FILE__ __LINE__ "isSetUp"

------------------------------------------------------------------------------
isView :: MonadIO m => IS -> Viewer -> m ()
isView is view = liftIO $ c'ISView is view >>= chkErr


-- * Section functions.
------------------------------------------------------------------------------
-- | Common `PetscObject` functionality for `PetscSection` objects.
instance IsPetscObject Section where
  create         = petscSectionCreate
  setFromOptions = petscSectionSetFromOptions
  setUp          = petscSectionSetUp
  destroy        = petscSectionDestroy
  {-# INLINE create #-}
  {-# INLINE setFromOptions #-}
  {-# INLINE setUp #-}
  {-# INLINE destroy #-}


-- ** Constructors & destructors.
------------------------------------------------------------------------------
petscSectionCreate :: MonadIO m => Comm -> m Section
petscSectionCreate comm = liftIO $ with NoSection $ \pps -> do
  c'PetscSectionCreate comm pps >>= chkErr >> peek pps

petscSectionDestroy :: MonadIO m => Section -> m ()
petscSectionDestroy sec = liftIO $ with sec $ \p -> do
  c'PetscSectionDestroy p >>= chkErr


-- ** Additional setup functions.
------------------------------------------------------------------------------
petscSectionSetFromOptions :: MonadIO m => Section -> m ()
-- petscSectionSetFromOptions sec = liftIO $ do
--   c'PetscSectionSetFromOptions sec >>= chkErr
petscSectionSetFromOptions  =
  error $ nosuch __FILE__ __LINE__ "petscSectionSetFromOptions"

petscSectionSetUp :: MonadIO m => Section -> m ()
petscSectionSetUp sec = liftIO $ do
  c'PetscSectionSetUp sec >>= chkErr


-- ** Section field setter/getters.
------------------------------------------------------------------------------
petscSectionSetFieldName :: MonadIO m => Section -> Int -> String -> m ()
petscSectionSetFieldName sec i lab = liftIO $ withCString lab $ \str -> do
  c'PetscSectionSetFieldName sec (fromIntegral i) str >>= chkErr

petscSectionSetDof :: MonadIO m => Section -> Int -> Int -> m ()
petscSectionSetDof sec i n = liftIO $ do
  let (i', n') = (fromIntegral i, fromIntegral n)
  c'PetscSectionSetDof sec i' n' >>= chkErr

------------------------------------------------------------------------------
petscSectionSetChart :: MonadIO m => Section -> Int -> Int -> m ()
petscSectionSetChart sec s e = liftIO $ do
  let (s', e') = (fromIntegral s, fromIntegral e)
  c'PetscSectionSetChart sec s' e' >>= chkErr

petscSectionGetChart :: MonadIO m => Section -> m (Int, Int)
petscSectionGetChart sec = liftIO $ with 0 $ \ps -> with 0 $ \pe -> do
  c'PetscSectionGetChart sec ps pe >>= chkErr
  (,) <$> ipeek ps <*> ipeek pe


-- ** Section viewer API.
------------------------------------------------------------------------------
petscSectionView :: MonadIO m => Section -> Viewer -> m ()
petscSectionView sec view = liftIO $ c'PetscSectionView sec view >>= chkErr

sectionView :: MonadIO m => Section -> Viewer -> m ()
sectionView  = petscSectionView
{-# INLINE sectionView #-}
