{-# LANGUAGE TypeOperators, PatternSynonyms, ViewPatterns, TupleSections,
             MultiParamTypeClasses, FlexibleInstances
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Options
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Mid-level bindings to the options database funcitons.
-- 
-- Changelog:
--  + 13/06/2017  --  initial file;
-- 
-- TODO:
--  + extract the `__FILE__` label from the environment?
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.Options where

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.C.String
import Foreign.Marshal
import System.IO.Unsafe
import Unsafe.Coerce (unsafeCoerce)
import Control.Monad.Reader
import Control.Monad.IO.Class
import Data.IORef
import Data.String
import Text.Printf

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Core
import Bindings.Numeric.PETSc.Internal.Options
import Bindings.Numeric.PETSc.Internal


-- * A monadic environment for processing option-queries.
------------------------------------------------------------------------------
newtype OptM a = OptM { runOptM :: ReaderT POptItems IO a }


-- * Options-parsing function families.
------------------------------------------------------------------------------
-- TODO: Extract the `__FILE__` label from the environment?
class IsPetscOption t where
  option :: String -> String -> String -> t -> OptM t

------------------------------------------------------------------------------
instance IsPetscOption Bool where
  option opt hlp fil def = petscOptionsBool opt hlp fil def

instance IsPetscOption Int where
  option opt hlp fil def = petscOptionsInt opt hlp fil def

instance IsPetscOption Double where
  option opt hlp fil def = petscOptionsReal opt hlp fil def

instance IsPetscOption [Char] where
  option opt hlp fil def = petscOptionsString opt hlp fil def

instance IsPetscOption [Int] where
  option opt hlp fil xs = petscOptionsIntArray opt hlp fil xs


-- ** Standard instances for the environment.
------------------------------------------------------------------------------
instance Functor OptM where
  fmap f (OptM k) = OptM $ fmap f k

instance Applicative OptM where
  pure = OptM . pure
  OptM f <*> OptM x = OptM $ f <*> x

instance Monad OptM where
  return = OptM . return
  OptM m >>= k = OptM $ m >>= runOptM . k

instance MonadReader POptItems OptM where
  ask = OptM ask
  local f (OptM m) = OptM $ local f m

instance MonadIO OptM where
  liftIO = OptM . liftIO


------------------------------------------------------------------------------
-- | Use this when no (or default) options DB is to be used.
pattern NoOptions :: POptions
pattern NoOptions <- ((== POptions nullPtr) -> True)
  where NoOptions  = POptions nullPtr

-- OBSOLETE?
defaultDB :: POptions
defaultDB  = POptions nullPtr


-- * Standard database queries.
------------------------------------------------------------------------------
petscOptionsGetInt ::
  MonadIO m => POptions -> String -> String -> m (Maybe Int)
petscOptionsGetInt db pre lab = liftIO $ with pfalse $ \pb -> with 0 $ \pz -> do
  withCString pre $ \pp -> withCString lab $ \pl -> do
    c'PetscOptionsGetInt db pp pl pz pb >>= chkErr
    z <- fromIntegral <$> peek pz
    peek pb >>= \b -> return $ if (hbool b) then Just z else Nothing


-- * Begin an options-database session.
------------------------------------------------------------------------------
petscOptions :: String -> String -> String -> OptM a -> IO a
petscOptions prfx desc mmod kopt = do
  popt <- calloc
  let actn = do
        -- ^ querying and/or publishing?
        pub <- petscOptionsPublish
        petscOptionsSetCount $ if pub then -1 else 1
        -- ^ TODO: loop until count == 2:
        petscOptionsBegin prfx desc mmod
        kopt >>= \res -> petscOptionsEnd >> return res
  rest <- runReaderT (runOptM actn) $ POptItems popt
  free popt >> return rest

petscOptions' :: String -> String -> String -> OptM a -> IO a
petscOptions' prfx desc mmod kopt = do
  alloca $ \popt -> do
    petscMemzero popt
    let actn = do
          -- ^ querying and/or publishing?
          pub <- petscOptionsPublish
          petscOptionsSetCount $ if pub then -1 else 1
          -- ^ TODO: loop until count == 2:
          petscOptionsBegin prfx desc mmod
          kopt >>= \res -> petscOptionsEnd >> return res
    runReaderT (runOptM actn) $ POptItems popt


-- * Option queries.
------------------------------------------------------------------------------
petscOptionsBool :: String -> String -> String -> Bool -> OptM Bool
petscOptionsBool prfx desc mmod dflt = ask >>= \popt -> liftIO $ do
  let dft' = pbool dflt
  with dft' $ \p -> do
    pfx' <- newCString prfx
    dsc' <- newCString desc
    mod' <- newCString mmod
    c'PetscOptionsBool_Private popt pfx' dsc' mod' dft' p nullPtr >>= chkErr
    free pfx' >> free dsc' >> free mod'
    hbool <$> peek p

petscOptionsString :: String -> String -> String -> String -> OptM String
petscOptionsString p d m x = ask >>= \popt -> liftIO $ do
  let n = PMaxPathLen
  withPString p $ \pp -> withPString d $ \pd -> withPString m $ \pm ->
    withPString x $ \px -> allocaArray (fromIntegral n) $ \str ->
    with pfalse $ \pb -> do
      c'PetscOptionsString_Private popt pp pd pm px str n pb >>= chkErr
      bpeek pb >>= \b -> case b of
        True -> peekCString str
        _    -> return x

petscOptionsInt :: String -> String -> String -> Int -> OptM Int
petscOptionsInt p d m x = ask >>= \popt -> liftIO $ do
  withPString p $ \pp -> withPString d $ \pd -> withPString m $ \pm -> do
    let n = fromIntegral x
    with n $ \pn -> do
      c'PetscOptionsInt_Private popt pp pd pm n pn nullPtr >>= chkErr
      fromIntegral <$> peek pn

petscOptionsReal :: String -> String -> String -> Double -> OptM Double
petscOptionsReal p d m x = ask >>= \popt -> liftIO $ do
  withPString p $ \pp -> withPString d $ \pd -> withPString m $ \pm -> do
    let n = realToFrac x
    with n $ \pn -> do
      c'PetscOptionsReal_Private popt pp pd pm n pn nullPtr >>= chkErr
      realToFrac <$> peek pn

------------------------------------------------------------------------------
-- | Choose an option from an enumerated list.
petscOptionsEList ::
  Enum e => String -> String -> String -> [String] -> String -> OptM e
petscOptionsEList p d m xs x = ask >>= \popt -> liftIO $ do
  withPString p $ \pp -> withPString d $ \pd -> withPString m $ \pm -> do
    let n = fromIntegral (length xs)
    withCStringArray xs $ \ar -> withPString x $ \px -> with 0 $ \pn -> do
      c'PetscOptionsEList_Private popt pp pd pm ar n px pn nullPtr >>= chkErr
      toEnum . fromIntegral <$> peek pn

-- | Read an array of integer arguments.
petscOptionsIntArray ::
  String -> String -> String -> [Int] -> OptM [Int]
petscOptionsIntArray p d m xs = ask >>= \popt -> liftIO $ do
  withPString p $ \pp -> withPString d $ \pd -> withPString m $ \pm -> do
    let n = fromIntegral $ length xs
    withArray (fromIntegral <$> xs) $ \pa -> with n $ \pn -> do
      c'PetscOptionsIntArray_Private popt pp pd pm pa pn nullPtr >>= chkErr
      m <- ipeek pn
      fmap fromIntegral <$> peekArray m pa


-- ** Non-PETSc options parsers.
------------------------------------------------------------------------------
-- | For PETSc type-strings; e.g., `DMType`, coerce these to `CString's and
--   then parse the command-line options. If a type-string is found, coerce it
--   back to type `t`.
--   NOTE: The coercion works because the PETSc `CString` types are wrapped
--     with Haskell `newtype` constructors.
--   NOTE: Returns the default `x` type-value if the given type-string wasn't
--     found.
--   NOTE: Doesn't have a `man` prefix string.
petscOptionsType :: String -> String -> t -> OptM t
petscOptionsType p d x = ask >>= \po -> liftIO $ do
  let (n, m) = (PMaxPathLen, fromIntegral n)
  withPString p $ \pp -> withPString d $ \pd -> with pfalse $ \pb -> do
    x' <- unsafeCoerce x :: IO CString
    ps <- callocArray m
    c'PetscOptionsString_Private po pp pd nullPtr x' ps n pb >>= chkErr
    flag <- hbool <$> peek pb
    if flag then unsafeCoerce ps else free ps >> return x


-- * Internal functions.
------------------------------------------------------------------------------
petscOptionsBegin :: String -> String -> String -> OptM ()
petscOptionsBegin prfx desc mmod = ask >>= \popt -> liftIO $ do
  pfx' <- newCString prfx
  dsc' <- newCString desc
  mod' <- newCString mmod
  comm <- petscCommWorld
  c'PetscOptionsBegin_Private popt comm pfx' dsc' mod' >>= chkErr

petscOptionsEnd :: OptM ()
petscOptionsEnd  = ask >>= \popt -> liftIO $ do
  c'PetscOptionsEnd_Private popt >>= chkErr

------------------------------------------------------------------------------
-- | Flag that represents whether the options-block is to publish any new
--   options?
petscOptionsPublish :: MonadIO m => m Bool
petscOptionsPublish  = hbool <$> liftIO (peek p'PetscOptionsPublish)

petscOptionsGetCount :: OptM Int
petscOptionsGetCount  =
  ask >>= fmap fromIntegral . liftIO . peek . p'PetscOptionItems'count . unPOptItems

petscOptionsSetCount :: Int -> OptM ()
petscOptionsSetCount c =
  ask >>= liftIO . flip poke (fromIntegral c) . p'PetscOptionItems'count . unPOptItems


-- ** Debug functions.
------------------------------------------------------------------------------
petscOptionsDump :: OptM ()
petscOptionsDump  = ask >>= \popt -> liftIO $ do
  opt <- peek $ unPOptItems popt
  let fi = fromIntegral :: Integral i => i -> Int
  printf "PetscOptionsObject {\n"
  printf " count:\t\t %d\n" . fi $ c'PetscOptionItems'count opt
  printf " next:\t\t %s\n" . show $ c'PetscOptionItems'next opt
  let p0 = c'PetscOptionItems'prefix opt
      p1 = c'PetscOptionItems'pprefix opt
      p2 = c'PetscOptionItems'title opt
  when (castPtr p0 /= nullPtr) $ peekCString p0 >>= printf " prefix:\t %s\n"
  when (castPtr p1 /= nullPtr) $ peekCString p1 >>= printf " pprefix:\t %s\n"
  when (castPtr p2 /= nullPtr) $ peekCString p2 >>= printf " title:\t\t %s\n"
  printf " printhelp:\t %s\n" . show . hbool $ c'PetscOptionItems'printhelp opt
  printf " changedmethod:\t %s\n" . show . hbool $ c'PetscOptionItems'changedmethod opt
  printf " alreadyprinted: %s\n" . show . hbool $ c'PetscOptionItems'alreadyprinted opt
  printf "}\n"
