{-# LANGUAGE CPP, TypeOperators, PatternSynonyms, ViewPatterns, TupleSections,
             GeneralizedNewtypeDeriving, StandaloneDeriving, DeriveGeneric,
             DeriveDataTypeable, MultiParamTypeClasses, FlexibleInstances
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.View
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Low-level bindings to PETSc's object-viewer module.
-- 
-- Changelog:
--  + 08/08/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.View
       ( IsPetscObject(..)
       , Viewer, pattern NoViewer
       , ViewerT, pattern NoViewerT, ViewerF, pattern NoViewerF
       , Display(..), pattern NoDisplay
       , petscViewerStdOutWorld, pattern PetscViewerStdOutWorld
       , petscViewerDrawWorld
       , pattern ViewerASCII, pattern ViewerVTK
       , pattern PETSC_VIEWER_DEFAULT, pattern PETSC_VIEWER_ASCII_VTK
       , petscViewerCreate, petscViewerDestroy
       , petscViewerSetType
       , petscViewerPushFormat, petscViewerPopFormat
       , petscViewerVTKOpen, petscViewerHDF5Open
       , petscViewerDrawOpen
       , petscViewerFileSetName
         -- ^ viewer options:
       , petscOptionsGetViewer
       ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal
import Foreign.C.String
import GHC.Generics (Generic)
import Control.Monad.IO.Class
import Control.Monad.Primitive
import Data.Typeable (Typeable)
import Data.StateVar
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vector
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Storable.Mutable as Mut

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.View
import Bindings.Numeric.PETSc.Internal


-- * Some standard PETSc object instances.
------------------------------------------------------------------------------


-- * Some standard instances.
------------------------------------------------------------------------------
#include "petsc_macro.h"
macro_PETSC_OBJECT_INST(petscViewer,Viewer)


-- ** Configure viewers from command-line options.
------------------------------------------------------------------------------
petscOptionsGetViewer ::
  MonadIO m => Comm -> String -> String -> m (Viewer, ViewerF)
petscOptionsGetViewer comm pfx name = liftIO $ with NoViewer $ \pv -> do
  withPString pfx $ \pp -> withPString name $ \pn -> do
    with NoViewerF $ \pf -> with pfalse $ \pb -> do
      c'PetscOptionsGetViewer comm pp pn pv pf pb >>= chkErr
      (,) <$> peek pv <*> peek pf


-- ** PETSc viewers.
------------------------------------------------------------------------------
petscViewerDrawWorld :: MonadIO m => m Viewer
petscViewerDrawWorld  = liftIO $ petscCommWorld >>= c'PETSC_VIEWER_DRAW_
