{-# LANGUAGE TypeOperators, PatternSynonyms, ViewPatterns, TupleSections,
             RankNTypes, ScopedTypeVariables, ConstraintKinds, CPP
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Some internal helper-functions for the PETSc, mid-level bindings.
-- 
-- Changelog:
--  + 11/06/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.Internal
       ( module Bindings.Numeric.PETSc.Internal
       , chkErr, pbool, ptrue, pfalse, hbool
       ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal.Array
import Foreign.Marshal.Utils
import Foreign.C.Types
import Foreign.C.String
import System.IO.Unsafe (unsafePerformIO)
import System.Exit
import Unsafe.Coerce
import Control.Monad
import Control.Monad.IO.Class
import qualified Data.Vector.Generic as G
import Data.Vector.Storable (Vector, (!))
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector as Box
import Text.Printf

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Core

import Data.List (intercalate)
import Foreign.Marshal.Alloc


-- * Generic `PetscObject` functionality.
------------------------------------------------------------------------------
-- | The following methods are supported by any `PetscObject`.
class IsPetscObject a where
  -- ^ All Petsc objects have a `xxCreate` method:
  create :: MonadIO m => Comm -> m a
  -- ^ All Petsc objects have a `xxSetFromOptions` method:
  setFromOptions :: MonadIO m => a -> m ()
  -- ^ All Petsc objects have a `xxDestroy` method:
  destroy :: MonadIO m => a -> m ()
  -- ^ All Petsc objects have a `xxSetUp` method:
  setUp :: MonadIO m => a -> m ()
  -- ^ Needs to be cast to use `PetscObjectSetName`:
  --   NOTE: The default method should work for any `PetscObject`.
  setName :: MonadIO m => a -> String -> m ()
  setName a lab = liftIO $ withCString lab $ \str -> do
    a' <- unsafeCoerce a :: IO PetscObject
    c'PetscObjectSetName a' str >>= chkErr
  getName :: MonadIO m => a -> m String
  getName a = liftIO $ with nullPtr $ \ptr -> do
    a' <- unsafeCoerce a :: IO PetscObject
    c'PetscObjectGetName a' ptr >>= chkErr >> peek ptr >>= peekCString
  -- ^ Needs to be cast to use `PetscObjectViewFromOptions`:
  --   NOTE: The default method should work for any `PetscObject`.
  viewFromOptions :: MonadIO m => a -> PetscObject -> String -> m ()
  viewFromOptions a obj opt = liftIO $ withCString opt $ \str -> do
    a' <- unsafeCoerce a :: IO PetscObject
    c'PetscObjectViewFromOptions a' obj str >>= chkErr
  -- ^ Gets the MPI communicator for any `PetscObject`:
  --   TODO: PETSc docs say to use `PetscObjectGetComm` instead, as that has
  --     exception handling.
  getComm :: MonadIO m => a -> m Comm
  getComm a = liftIO $ with NoComm $ \pcomm -> do
    a' <- unsafeCoerce a :: IO PetscObject
    c'PetscObjectGetComm a' pcomm >>= chkErr >> peek pcomm

instance IsPetscObject PetscObject where
  create  = error $ __FILE__ ++ ": cannot create PetscObect with no type"
  destroy = petscObjectDestroy
  setFromOptions = const $ pure ()
  setUp          = const $ pure ()


------------------------------------------------------------------------------
-- | Adds a degree of type-safety to (PETSc) object-composition.
class Composable b where
  compose :: (IsPetscObject a, MonadIO m) => a -> String -> b -> m ()
  compose a lab b = liftIO $ withPString lab $ \str -> do
    a' <- unsafeCoerce a :: IO PetscObject
    b' <- unsafeCoerce b :: IO PetscObject
    c'PetscObjectCompose a' str b' >>= chkErr

instance Composable PetscObject
instance Composable MatNullSpace


-- * Convenience aliases & patterns.
------------------------------------------------------------------------------
-- | Just convenience aliass.
type Box = Box.Vector
type PFinalFun = IO PErr -- C'PFinalFun

-- | This is a "constant," so this should be safe?
pattern PetscCommWorld :: Comm
pattern PetscCommWorld <- ((== Comm unsafeCommWorld) -> True)
  where PetscCommWorld  = Comm unsafeCommWorld

pattern CommWorld :: Comm
pattern CommWorld  = PetscCommWorld

unsafeCommWorld :: C'MPI_Comm
unsafeCommWorld  = unComm $ unsafePerformIO petscCommWorld

------------------------------------------------------------------------------
pattern NoClassId :: ClassId
pattern NoClassId <- ((== ClassId 0) -> True)
  where NoClassId  = ClassId 0

pattern DMClassId :: ClassId
pattern DMClassId <- ((== ClassId c'DM_CLASSID) -> True)
  where DMClassId  = ClassId c'DM_CLASSID

------------------------------------------------------------------------------
pattern NoLogEvent :: LogEvent
pattern NoLogEvent <- ((== LogEvent 0) -> True)
  where NoLogEvent  = LogEvent 0


-- * Handle the PETSc context.
------------------------------------------------------------------------------
-- | Needs the programs `char* argv[]`, along with an appropriate help-string.
petscInitialise :: [String] -> String -> IO ()
petscInitialise argv help = withCString help $ \h -> do
  ar <- makeArgv argv
  with (fromIntegral (length argv)) $ \l -> do
    with ar $ \p -> c'PetscInitialize l p nullPtr h >>= chkErr

petscFinalise :: IO ()
petscFinalise  = c'PetscFinalize >>= chkErr

------------------------------------------------------------------------------
-- | Register additional finalisers.
petscRegisterFinalize :: MonadIO m => PFinalFun -> m ()
petscRegisterFinalize f = liftIO $ do
  f' <- mk'PFinalFun f
  c'PetscRegisterFinalize f' >>= chkErr

------------------------------------------------------------------------------
-- | Destroys any valid PETSc object.
petscObjectDestroy :: MonadIO m => PetscObject -> m ()
petscObjectDestroy obj = liftIO $ with obj $ \ptr -> do
  c'PetscObjectDestroy ptr >>= chkErr

------------------------------------------------------------------------------
petscCitationsRegister :: MonadIO m => String -> m Bool
petscCitationsRegister cit = liftIO $ with pfalse $ \pb -> do
  withPString cit $ \ps -> c'PetscCitationsRegister ps pb >>= chkErr
  hbool <$> peek pb

-- | Register a logging-event for the given name & `ClassId`.
petscLogEventRegister :: MonadIO m => String -> ClassId -> m LogEvent
petscLogEventRegister lab cid = liftIO $ do
  with NoLogEvent $ \ptr -> withPString lab $ \str -> do
    c'PetscLogEventRegister str cid ptr >>= chkErr >> peek ptr

------------------------------------------------------------------------------
-- | Use `ConstraintKinds` to help with stuff.
type IsPObj4 a b c d =
  (IsPetscObject a, IsPetscObject b, IsPetscObject c, IsPetscObject d)

petscLogEventBegin :: (MonadIO m, IsPObj4 a b c d) =>
  LogEvent -> a -> b -> c -> d -> m ()
petscLogEventBegin ev a b c d = liftIO $ do
  a' <- unsafeCoerce a
  b' <- unsafeCoerce b
  c' <- unsafeCoerce c
  d' <- unsafeCoerce d
  c'PetscLogEventBegin ev a' b' c' d' >>= chkErr

petscLogEventEnd   :: (MonadIO m, IsPObj4 a b c d) =>
  LogEvent -> a -> b -> c -> d -> m ()
petscLogEventEnd   ev a b c d = liftIO $ do
  a' <- unsafeCoerce a
  b' <- unsafeCoerce b
  c' <- unsafeCoerce c
  d' <- unsafeCoerce d
  c'PetscLogEventEnd   ev a' b' c' d' >>= chkErr


-- ** Command-line argument functions.
------------------------------------------------------------------------------
-- | Map the Haskell representation of `argv` back into a data-structure that
--   resembles the C version; i.e., a flat array of `char`, with `\0` between
--   each of the arguments.
makeArgv :: [String] -> IO (Ptr CString)
makeArgv xs = do
  let n  = length xs
      x0 = intercalate "\0" xs ++ "\0"
      js = scanl (+) 0 $ succ . length <$> xs
  cs <- newCString x0
  ps <- mallocArray0 n
  mapM_ (\(i, p) -> pokeElemOff ps i p) $ [0..] `zip` map (plusPtr cs) (init js)
  pokeElemOff cs (last js-1) 0
  return ps

-- | Release the memory used by the C representation of `argv`.
freeArgv :: Ptr (Ptr CString) -> IO ()
freeArgv ptr = do
  peek ptr >>= peek >>= free
  peek ptr >>= free
  free ptr

------------------------------------------------------------------------------
-- | PETSc's global MPI communicator.
petscCommWorld :: IO Comm
petscCommWorld  = Comm <$> peek p'PETSC_COMM_WORLD

withPETSc :: [String] -> String -> IO a -> IO a
withPETSc argv help aktn = do
  h <- newCString help
  l <- new $ fromIntegral (length argv)
  p <- new =<< makeArgv argv

  c'PetscInitialize l p nullPtr h >>= chkErr
  x <- aktn
  c'PetscFinalize >>= chkErr

  free h >> free l >> freeArgv p
  return x

withPETSc' :: [String] -> String -> IO a -> IO a
withPETSc' argv help k = do
  petscInitialise argv help
  k >>= \x -> petscFinalise >> return x


-- ** PETSc function begin/end.
------------------------------------------------------------------------------
petscFunctionBeginUser :: MonadIO m => m ()
petscFunctionBeginUser  = liftIO c'PetscFunctionBeginUser

-- petscFunctionReturn :: MonadIO m => PetscErrorCode -> m PetscErrorCode
-- petscFunctionReturn  = liftIO . c'PetscFunctionReturn

petscSuccess :: MonadIO m => m PetscErrorCode
-- petscSuccess  = petscFunctionReturn PetscSuccess
petscSuccess  = return PetscSuccess


-- * Helper functions.
------------------------------------------------------------------------------
-- | Peek and convert to Haskell's standard integer, `Int`.
ipeek :: Ptr PInt -> IO Int
ipeek  = fmap fromIntegral . peek
{-# INLINE ipeek #-}

bpeek :: Ptr PBool -> IO Bool
bpeek p = hbool <$> peek p
{-# INLINE bpeek #-}

dpeek :: Ptr PReal -> IO Double
dpeek  = fmap realToFrac . peek
{-# INLINE dpeek #-}


-- ** Vector-specific helper functions.
------------------------------------------------------------------------------
-- | The length is often needed as a `CInt`.
len :: (Integral i, G.Vector v a) => v a -> i
len  = fromIntegral . G.length

-- | Convenience function when using the FFI.
vdbl :: Vector CDouble -> Vector Double
vdbl  = Vec.map realToFrac

-- | Convenience function when using the FFI.
vint :: Vector CInt -> Vector Int
vint  = Vec.map fromIntegral

------------------------------------------------------------------------------
-- | Read `n` values from the memory at `ptr`, into a vector.
fromArrayM :: forall m a. (MonadIO m, Storable a) =>
              Int -> Ptr a -> m (Vector a)
{-# INLINE[1] fromArrayM #-}
fromArrayM n ptr = liftIO $ Vec.generateM n (\i -> peekElemOff ptr i)

-- | Write the contents of the given vector into the array pointed to by
--   `ptr`.
toArrayM :: forall m a. (MonadIO m, Storable a) =>
            Vector a -> Ptr a -> m ()
{-# INLINE[1] toArrayM #-}
toArrayM vec ptr = liftIO $ pokeArray ptr (Vec.toList vec)

------------------------------------------------------------------------------
-- | Extract the array from the `Vector`, and then evaluate with the given
--   action.
--   NOTE: When `vec` is empty, pass a `NULL` pointer instead.
withVec :: Storable a => Vector a -> (Ptr a -> IO b) -> IO b
withVec vec action
  | Vec.null vec = action nullPtr
  | otherwise    = Vec.unsafeWith vec $ \ptr -> action ptr


-- ** Additional vector helper functions.
------------------------------------------------------------------------------
-- TODO: Does list-fusion work correctly?
mkVecOfPtrs :: Storable a => Int -> Ptr (Ptr a) -> IO (Vector (Ptr a))
{-# INLINE[1] mkVecOfPtrs #-}
mkVecOfPtrs n pp = Vec.fromList <$> peekArray n pp

-- | More general version of the above.
mkVecOfShit :: Storable a => Int -> Ptr a -> IO (Vector a)
{-# INLINE[1] mkVecOfShit #-}
mkVecOfShit n = fmap Vec.fromList . peekArray n

-- | 2D array with `m` rows of `n` columns (and row-major).
mkBoxOfToys :: Storable a => Int -> Int -> Int -> Ptr (Ptr a) -> IO (Box (Vector a))
mkBoxOfToys s m n pp = do
  pv <- Box.convert <$> mkVecOfPtrs (s+m) pp
  let go j p | j  <  s   = return $ Vec.empty
             | otherwise = mkVecOfShit n p
  Box.imapM go pv

------------------------------------------------------------------------------
-- | Show the pointers (as hexadecimal) -- primarily for debugging.
showPtrs :: Storable a => Vector (Ptr a) -> IO ()
showPtrs pvec = Vec.mapM_ pshow js where
  pshow j = printf "p[%d]\t= %s\n" j (show $ pvec!j)
  js = 0 `Vec.enumFromN` len pvec


-- ** String-specific helper functions.
------------------------------------------------------------------------------
-- | From `petsc-hs`, Copyright Marco Zocca, 2015 , and licensed under the
--   GPL3 .
withCStrings :: [String] -> ([CString] -> IO a) -> IO a
withCStrings ss f = case ss of
  [] -> f []
  (s:ss') -> withCString s $ \cs -> 
    withCStrings ss' $ \css -> f (cs:css)

withCStringArray :: [String] -> (Ptr CString -> IO a) -> IO a
withCStringArray ss f = withCStrings ss $ \css -> withArray css f

withCStringArrayPtr :: [String] -> (Ptr (Ptr CString) -> IO a) -> IO a
withCStringArrayPtr ss f = withCStringArray ss $ \css -> with css f

withPString :: String -> (CString -> IO a) -> IO a
withPString "" k = k nullPtr
withPString ss k = withCString ss $ \ps -> k ps


-- ** Low-level, standard-library helpers.
------------------------------------------------------------------------------
petscMemzero :: forall a. Storable a => Ptr a -> IO ()
petscMemzero ptr =
  let n = fromIntegral $ sizeOf (undefined :: a)
      p = castPtr ptr
  in  c'PetscMemzero p n >>= chkErr

-- | Free any memory that was allocated by PETSc.
petscFree :: forall a. Storable a => Ptr a -> IO ()
petscFree ptr = c'PetscFree (castPtr ptr) >>= chkErr
