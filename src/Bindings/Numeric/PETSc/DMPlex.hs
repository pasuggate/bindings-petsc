{-# LANGUAGE OverloadedStrings, CPP, TemplateHaskell, DeriveGeneric,
             GeneralizedNewtypeDeriving, FlexibleInstances,
             PatternSynonyms, ViewPatterns, TupleSections #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.DMPlex
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Bindings for the grid/mesh (DM) module.
-- 
-- NOTE:
-- 
-- Changelog:
--  + 05/08/2017  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.DMPlex
       ( AdjFun(..), AdjFunT
       , mk'RefineFun
       , module Bindings.Numeric.PETSc.DMPlex
       ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.C.String
import Foreign.Marshal
import GHC.Generics (Generic)
import Control.Monad.IO.Class
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec

import Bindings.Numeric.PETSc.Internal
import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Core
import Bindings.Numeric.PETSc.Internal.DM
import Bindings.Numeric.PETSc.Internal.DMPlex
import Bindings.Numeric.PETSc.IS


-- * Patterns.
------------------------------------------------------------------------------
pattern NoAdjFun :: AdjFun
pattern NoAdjFun <- ((== AdjFun nullFunPtr) -> True)
  where NoAdjFun  = AdjFun nullFunPtr

pattern NoRefineFun :: RefineFun
pattern NoRefineFun <- ((== RefineFun nullFunPtr) -> True)
  where NoRefineFun  = RefineFun nullFunPtr


-- * DMPlex constructors.
------------------------------------------------------------------------------
dmPlexCreate :: MonadIO m => Comm -> m DM
dmPlexCreate comm = liftIO $ with (DM nullPtr) $ \p -> do
  c'DMPlexCreate comm p >>= chkErr >> peek p

------------------------------------------------------------------------------
-- | Construct from the DAG (adjacency list representation) and coordinates.
dmPlexCreateFromDAG ::
  MonadIO m => DM -> Int -> [Int] -> [Int] -> [Int] -> [Int] -> [Double] ->
  m ()
dmPlexCreateFromDAG dm depth npts csize cones corients coords = liftIO $ do
  let d = fromIntegral depth
      n = fromIntegral <$> npts
      s = fromIntegral <$> csize
      c = fromIntegral <$> cones
      o = fromIntegral <$> corients
      p = realToFrac <$> coords
  withArray n $ \pn -> withArray s $ \ps -> withArray c $ \pc ->
    withArray c $ \pc -> withArray o $ \po -> withArray p $ \pp -> do
      c'DMPlexCreateFromDAG dm d pn ps pc po pp >>= chkErr

------------------------------------------------------------------------------
dmPlexCreateBoxMesh :: MonadIO m => Comm -> Int -> Bool -> m DM
dmPlexCreateBoxMesh comm dim interp = liftIO $ with (DM nullPtr) $ \p -> do
  let (d, b) = (fromIntegral dim, pbool interp)
  c'DMPlexCreateBoxMesh comm d b p >>= chkErr >> peek p

{-- }
-- TODO:
dmPlexCreateHexBoxMesh :: MonadIO m => Comm -> Vector Int -> BdyT -> BdyT -> BdyT -> m DM
dmPlexCreateHexBoxMesh comm cells bx by bz = liftIO $ do
  let dim = fromIntegral $ Vec.length cells
      cs  = Vec.map fromIntegral cells
  with NoDM $ \pdm -> Vec.unsafeWith cs $ \pcs -> do
    c'DMPlexCreateHexBoxMesh comm dim pcs bx by bz pdm >>= chkErr
    peek pdm
--}

dmPlexCreateFromFile :: MonadIO m => Comm -> FilePath -> Bool -> m DM
dmPlexCreateFromFile comm fp interp = liftIO $ withCString fp $ \pf ->
  with NoDM $ \pdm -> do
    c'DMPlexCreateFromFile comm pf (pbool interp) pdm >>= chkErr
    peek pdm


-- * Additional DMPlex setup.
------------------------------------------------------------------------------
-- NOTE: Use `NoSection` to use the default `PetscSection` when building the
--   index.
dmPlexCreateClosureIndex :: MonadIO m => DM -> Section -> m ()
dmPlexCreateClosureIndex dm sec = liftIO $ do
  c'DMPlexCreateClosureIndex dm sec >>= chkErr

------------------------------------------------------------------------------
-- | Construct a mesh with all entities (nodes, edges, faces, and cells), from
--   the given cell-node mesh.
dmPlexInterpolate :: MonadIO m => DM -> m DM
dmPlexInterpolate dm = liftIO $ with NoDM $ \ptr -> do
  c'DMPlexInterpolate dm ptr >>= chkErr >> peek ptr


-- ** Subdivision/refinement functions.
------------------------------------------------------------------------------
dmPlexCreateDefaultReferenceTree :: MonadIO m => Comm -> Int -> Bool -> m DM
dmPlexCreateDefaultReferenceTree comm dim simpl = liftIO $ do
  with NoDM $ \ptr -> do
    let (d, s) = (fromIntegral dim, pbool simpl)
    c'DMPlexCreateDefaultReferenceTree comm d s ptr >>= chkErr
    peek ptr

dmPlexSetTree :: MonadIO m => DM -> Section -> [Int] -> [Int] -> m ()
dmPlexSetTree dm sec ps cs = liftIO $ do
  let ps' = fromIntegral <$> ps
      cs' = fromIntegral <$> cs
  withArray ps' $ \pp -> withArray cs' $ \pc -> do
    c'DMPlexSetTree dm sec pp pc >>= chkErr

------------------------------------------------------------------------------
dmPlexSetReferenceTree :: MonadIO m => DM -> DM -> m ()
dmPlexSetReferenceTree dm tree = liftIO $ do
  c'DMPlexSetReferenceTree dm tree >>= chkErr

dmPlexGetReferenceTree :: MonadIO m => DM -> m DM
dmPlexGetReferenceTree dm = liftIO $ with NoDM $ \ptr -> do
  c'DMPlexGetReferenceTree dm ptr >>= chkErr >> peek ptr

------------------------------------------------------------------------------
dmPlexGetTreeParent :: MonadIO m => DM -> Int -> m (Int, Int)
dmPlexGetTreeParent dm p = liftIO $ with 0 $ \pp -> with 0 $ \pc -> do
  c'DMPlexGetTreeParent dm (fromIntegral p) pp pc >>= chkErr
  (,) <$> ipeek pp <*> ipeek pc

dmPlexGetTreeChildren :: MonadIO m => DM -> Int -> m [Int]
dmPlexGetTreeChildren dm p = liftIO $ with 0 $ \pn -> with nullPtr $ \px -> do
  c'DMPlexGetTreeChildren dm (fromIntegral p) pn px >>= chkErr
  (n, ar) <- (,) <$> ipeek pn <*> peek px
  map fromIntegral <$> peekArray n ar


-- * DMPlex connectivity.
-- ** DMPlex support functions.
------------------------------------------------------------------------------
dmPlexGetSupportSize :: MonadIO m => DM -> Int -> m Int
dmPlexGetSupportSize dm p = liftIO $ with 0 $ \ptr -> do
  c'DMPlexGetSupportSize dm (fromIntegral p) ptr >>= chkErr >> ipeek ptr

dmPlexGetSupport :: MonadIO m => DM -> Int -> m (Ptr PInt)
dmPlexGetSupport dm p = liftIO $ with nullPtr $ \ptr -> do
  c'DMPlexGetSupport dm (fromIntegral p) ptr >>= chkErr >> peek ptr

dmPlexSupport :: MonadIO m => DM -> Int -> m (Vector Int)
dmPlexSupport dm p = liftIO $ do
  n  <- dmPlexGetSupportSize dm p
  ar <- dmPlexGetSupport dm p
  vint <$> fromArrayM n ar

------------------------------------------------------------------------------
dmPlexGetSupportSection :: MonadIO m => DM -> m Section
dmPlexGetSupportSection dm = liftIO $ with NoSection $ \ptr -> do
  c'DMPlexGetSupportSection dm ptr >>= chkErr >> peek ptr


-- ** Cone functions.
------------------------------------------------------------------------------
-- | The cone of a cell is its edges.
dmPlexGetConeSize :: MonadIO m => DM -> Int -> m Int
dmPlexGetConeSize dm p = liftIO $ with 0 $ \ps -> do
  c'DMPlexGetConeSize dm (fromIntegral p) ps >>= chkErr >> ipeek ps

-- | The cone of a cell is its edges.
dmPlexGetCone :: MonadIO m => DM -> Int -> m PPInt
dmPlexGetCone dm p = liftIO $ with nullPtr $ \ptr -> do
  c'DMPlexGetCone dm (fromIntegral p) ptr >>= chkErr >> peek ptr

dmPlexCone :: MonadIO m => DM -> Int -> m (Vector Int)
dmPlexCone dm p = liftIO $ do
  (n, ar) <- (,) <$> dmPlexGetConeSize dm p <*> dmPlexGetCone dm p
  vint <$> fromArrayM n ar

------------------------------------------------------------------------------
dmPlexGetConeSection :: MonadIO m => DM -> m Section
dmPlexGetConeSection dm = liftIO $ with NoSection $ \ptr -> do
  c'DMPlexGetConeSection dm ptr >>= chkErr >> peek ptr

------------------------------------------------------------------------------
-- NOTE: "developer" level.
dmPlexGetCones :: MonadIO m => DM -> m PPInt
dmPlexGetCones dm = liftIO $ with nullPtr $ \ptr -> do
  c'DMPlexGetCones dm ptr >>= chkErr >> peek ptr

-- NOTE: "developer" level.
dmPlexGetConeOrientations :: MonadIO m => DM -> m PPInt
dmPlexGetConeOrientations dm = liftIO $ with nullPtr $ \ptr -> do
  c'DMPlexGetConeOrientations dm ptr >>= chkErr >> peek ptr


-- ** Entity closure functions.
------------------------------------------------------------------------------
dmPlexGetClosureIndices ::
  MonadIO m => DM -> Section -> Section -> Int -> m (Int, PPInt, Int)
dmPlexGetClosureIndices dm glob locl p = liftIO $ do
  with 0 $ \pn -> with nullPtr $ \px -> with 0 $ \pm -> do
    let p' = fromIntegral p
    c'DMPlexGetClosureIndices dm glob locl p' pn px pm >>= chkErr
    (,,) <$> ipeek pn <*> peek px <*> ipeek pm

dmPlexRestoreClosureIndices ::
  MonadIO m => DM -> Section -> Section -> Int -> Int -> PPInt -> Int -> m ()
dmPlexRestoreClosureIndices dm glob locl p n ar m = liftIO $ do
  let (p', n', m') = (fromIntegral p, fromIntegral n, fromIntegral m)
  with n' $ \pn -> with ar $ \px -> with m' $ \pm -> do
    c'DMPlexRestoreClosureIndices dm glob locl p' pn px pm >>= chkErr

------------------------------------------------------------------------------
-- | Copies the PETSc array of closure-indices into a vector.
--   
--   TODO: Oriented indices, where negative indices are encoded as `-(i+1)`.
dmPlexClosureIndices ::
  MonadIO m => DM -> Section -> Section -> Int -> m (Vector Int)
dmPlexClosureIndices dm glob locl p = liftIO $ do
  (n, ar, m) <- dmPlexGetClosureIndices dm glob locl p
  xs <- vint <$> fromArrayM n ar
  dmPlexRestoreClosureIndices dm glob locl p n ar m
  return xs

{-- }
-- TODO: already come and gone from PETSc?

-- ** Adjacency functions.
------------------------------------------------------------------------------
dmPlexSetAdjacencyCone :: MonadIO m => DM -> Bool -> m ()
dmPlexSetAdjacencyCone dm x = liftIO $ do
  c'DMPlexSetAdjacencyUseCone dm (pbool x) >>= chkErr

dmPlexGetAdjacencyCone :: MonadIO m => DM -> m Bool
dmPlexGetAdjacencyCone dm = liftIO $ with pfalse $ \pb -> do
  c'DMPlexGetAdjacencyUseCone dm pb >>= chkErr >> bpeek pb

------------------------------------------------------------------------------
dmPlexSetAdjacencyClosure :: MonadIO m => DM -> Bool -> m ()
dmPlexSetAdjacencyClosure dm x = liftIO $ do
  c'DMPlexSetAdjacencyUseClosure dm (pbool x) >>= chkErr

dmPlexGetAdjacencyClosure :: MonadIO m => DM -> m Bool
dmPlexGetAdjacencyClosure dm = liftIO $ with pfalse $ \pb -> do
  c'DMPlexGetAdjacencyUseClosure dm pb >>= chkErr >> bpeek pb
--}

------------------------------------------------------------------------------
-- | Returns an array of the adjacent points of `p`.
--   NOTE: The resulting array must be freed using `petscFree`.
dmPlexGetAdjacency :: MonadIO m => DM -> Int -> m (Int, PPInt)
dmPlexGetAdjacency dm p = liftIO $ with 0 $ \pn -> with nullPtr $ \px -> do
  c'DMPlexGetAdjacency dm (fromIntegral p) pn px >>= chkErr
  (,) <$> ipeek pn <*> peek px

dmPlexAdjacency :: MonadIO m => DM -> Int -> m (Vector Int)
dmPlexAdjacency dm p = liftIO $ do
  (n, ar) <- dmPlexGetAdjacency dm p
  vec <- vint <$> fromArrayM n ar
  petscFree ar
  return vec

{-- }
dmPlexAdjacency' :: MonadIO m => DM -> Int -> m (Vector Int)
dmPlexAdjacency' dm p = liftIO $ with 0 $ \pn -> do
  let p' = fromIntegral p
  c'DMPlexGetAdjacency dm p' pn nullPtr >>= chkErr
  n <- ipeek pn
  allocaArray n $ \px -> do
    c'DMPlexGetAdjacency dm p' pn (castPtr px) >>= chkErr
    vint <$> fromArrayM n px
--}


{-- }
------------------------------------------------------------------------------
dmPlexSetAdjacencyUser :: MonadIO m => DM -> AdjFun -> PCtx -> m ()
dmPlexSetAdjacencyUser dm fun ctx = liftIO $ do
  c'DMPlexSetAdjacencyUser dm fun ctx >>= chkErr

dmPlexGetAdjacencyUser :: MonadIO m => DM -> m (AdjFun, PCtx)
dmPlexGetAdjacencyUser dm = liftIO $ with NoAdjFun $ \pf -> do
  with nullPtr $ \pc -> do
    c'DMPlexGetAdjacencyUser dm pf pc >>= chkErr
    (,) <$> peek pf <*> peek pc
--}


-- ** Meets & joins.
--    TODO: What do these functions do?
------------------------------------------------------------------------------
dmPlexGetMeet :: MonadIO m => DM -> Vector Int -> m (Int, PPInt)
dmPlexGetMeet dm ps = liftIO $ do
  let m   = fromIntegral $ Vec.length ps
      ps' = Vec.map fromIntegral ps
  Vec.unsafeWith ps' $ \ar -> with 0 $ \pn -> with nullPtr $ \px -> do
    c'DMPlexGetMeet dm m ar pn px >>= chkErr
    (,) <$> ipeek pn <*> peek px

-- TODO: Free the array (using xxRestorexx)?
dmPlexMeet :: MonadIO m => DM -> Vector Int -> m (Vector Int)
dmPlexMeet dm ps = liftIO $ do
  dmPlexGetMeet dm ps >>= fmap vint . uncurry fromArrayM

dmPlexGetJoin :: MonadIO m => DM -> Vector Int -> m (Int, PPInt)
dmPlexGetJoin dm ps = liftIO $ do
  let m   = fromIntegral $ Vec.length ps
      ps' = Vec.map fromIntegral ps
  Vec.unsafeWith ps' $ \ar -> with 0 $ \pn -> with nullPtr $ \px -> do
    c'DMPlexGetJoin dm m ar pn px >>= chkErr
    (,) <$> ipeek pn <*> peek px

-- TODO: Free the array (using xxRestorexx)?
dmPlexJoin :: MonadIO m => DM -> Vector Int -> m (Vector Int)
dmPlexJoin dm ps = liftIO $ do
  dmPlexGetJoin dm ps >>= fmap vint . uncurry fromArrayM


-- * DMPlex data.
------------------------------------------------------------------------------
dmPlexGetPointLocal :: MonadIO m => DM -> Int -> m (Int, Int)
dmPlexGetPointLocal dm p = liftIO $ with 0 $ \ps -> with 0 $ \pe -> do
  c'DMPlexGetPointLocal dm (fromIntegral p) ps pe >>= chkErr
  (,) <$> ipeek ps <*> ipeek pe


-- ** MPI distribution functions.
------------------------------------------------------------------------------
dmPlexDistribute :: MonadIO m => DM -> Int -> StarForest -> m DM
dmPlexDistribute dm overlap sf = liftIO $ with NoDM $ \pdm -> do
  let o = fromIntegral overlap
  with sf $ \psf -> c'DMPlexDistribute dm o psf pdm >>= chkErr
  peek pdm

dmPlexDistributeOverlap :: MonadIO m => DM -> Int -> StarForest -> m DM
dmPlexDistributeOverlap dm overlap sf = liftIO $ with NoDM $ \pdm -> do
  let o = fromIntegral overlap
  with sf $ \psf -> c'DMPlexDistributeOverlap dm o psf pdm >>= chkErr
  peek pdm

------------------------------------------------------------------------------
dmPlexDistributeField ::
  MonadIO m => DM -> SF -> Section -> Vec -> Section -> Vec -> m ()
dmPlexDistributeField dm sf isec ivec osec ovec = liftIO $ do
  c'DMPlexDistributeField dm sf isec ivec osec ovec >>= chkErr


-- *** Access to local/distributed arrays.
------------------------------------------------------------------------------
-- | Set the values for the closure of point `pt`.
dmPlexVecSetClosure :: MonadIO m =>
  DM -> Section -> Vec -> Int -> Vector Double -> InsertMode -> m ()
dmPlexVecSetClosure dm sec vec pt xs mod = liftIO $ do
  let xs' = Vec.map realToFrac xs :: Vector PScalar
      pt' = fromIntegral pt
  Vec.unsafeWith xs' $ \ptr -> do
    c'DMPlexVecSetClosure dm sec vec pt' ptr mod >>= chkErr

------------------------------------------------------------------------------
-- | Need to keep the pointer from this PETSc routine, so that the checked-out
--   array can be restored.
dmPlexVecGetClosure ::
  MonadIO m => DM -> Section -> Vec -> Int -> m (Int, Ptr PScalar)
dmPlexVecGetClosure dm sec vec pt = liftIO $ do
  with 0 $ \pn -> with nullPtr $ \px -> do
    c'DMPlexVecGetClosure dm sec vec (fromIntegral pt) pn px >>= chkErr
    (,) <$> ipeek pn <*> peek px

dmPlexVecRestoreClosure ::
  MonadIO m => DM -> Section -> Vec -> Int -> Int -> Ptr PScalar -> m ()
dmPlexVecRestoreClosure dm sec vec pt n p = liftIO $ do
  with (fromIntegral n) $ \pn -> with p $ \px -> do
    c'DMPlexVecRestoreClosure dm sec vec (fromIntegral pt) pn px >>= chkErr


-- * DMPlex queries.
------------------------------------------------------------------------------
dmPlexEqual :: MonadIO m => DM -> DM -> m Bool
dmPlexEqual dm0 dm1 = liftIO $ with pfalse $ \pb -> do
  c'DMPlexEqual dm0 dm1 pb >>= chkErr
  hbool <$> peek pb

-- | Get the bounds [start, end) for all points at a certain height. 
dmPlexGetHeightStratum :: MonadIO m => DM -> Int -> m (Int, Int)
dmPlexGetHeightStratum dm h = liftIO $ with 0 $ \ps -> with 0 $ \pe -> do
  c'DMPlexGetHeightStratum dm (fromIntegral h) ps pe >>= chkErr
  (,) <$> ipeek ps <*> ipeek pe


-- * Refinement functions.
------------------------------------------------------------------------------
dmPlexSetRefinementLimit :: (MonadIO m, Real a) => DM -> a -> m ()
dmPlexSetRefinementLimit dm lim = liftIO $ do
  c'DMPlexSetRefinementLimit dm (realToFrac lim) >>= chkErr

dmPlexGetRefinementLimit :: MonadIO m => DM -> m Double
dmPlexGetRefinementLimit dm = liftIO $ with 0 $ \ptr -> do
  c'DMPlexGetRefinementLimit dm ptr >>= chkErr >> dpeek ptr

------------------------------------------------------------------------------
dmPlexSetRefinementUniform :: MonadIO m => DM -> Bool -> m ()
dmPlexSetRefinementUniform dm uni = liftIO $ do
  c'DMPlexSetRefinementUniform dm (pbool uni) >>= chkErr

dmPlexGetRefinementUniform :: MonadIO m => DM -> m Bool
dmPlexGetRefinementUniform dm = liftIO $ with pfalse $ \ptr -> do
  c'DMPlexGetRefinementUniform dm ptr >>= chkErr >> bpeek ptr

------------------------------------------------------------------------------
dmPlexSetRefinementFunction :: MonadIO m => DM -> RefineFunT -> m ()
dmPlexSetRefinementFunction dm fun = liftIO $ do
  ptr <- RefineFun <$> mk'RefineFun fun
  c'DMPlexSetRefinementFunction dm ptr >>= chkErr

dmPlexGetRefinementFunction :: MonadIO m => DM -> m RefineFun
dmPlexGetRefinementFunction dm = liftIO $ with NoRefineFun $ \ptr -> do
  c'DMPlexGetRefinementFunction dm ptr >>= chkErr >> peek ptr


-- * Mesh-checking routines.
------------------------------------------------------------------------------
dmPlexCheckSymmetry :: MonadIO m => DM -> m ()
dmPlexCheckSymmetry dm = liftIO $ c'DMPlexCheckSymmetry dm >>= chkErr

dmPlexCheckSkeleton :: MonadIO m => DM -> Bool -> Int -> m ()
dmPlexCheckSkeleton dm simplex height = liftIO $ do
  c'DMPlexCheckSkeleton dm (pbool simplex) (fromIntegral height) >>= chkErr

dmPlexCheckFaces :: MonadIO m => DM -> Bool -> Int -> m ()
dmPlexCheckFaces dm simplex height = liftIO $ do
  c'DMPlexCheckFaces dm (pbool simplex) (fromIntegral height) >>= chkErr


-- * Index-Set (IS) functions.
------------------------------------------------------------------------------
-- #ccall DMPlexCreateSection , DM -> PInt -> PInt -> PPInt -> PPInt -> PInt -> PPInt -> Ptr IS -> Ptr IS -> IS -> Ptr PSection -> PRes

dmPlexCreateSection :: MonadIO m => DM -> Int -> Vector Int -> Vector Int -> Vector Int -> Vector IS -> Vector IS -> IS -> m Section
dmPlexCreateSection dm dim fields comps bcs bccs bcpt perm = liftIO $ do
  let [d, n, m] = fromIntegral <$> [dim, Vec.length fields, Vec.length bcs]
      fs' = Vec.map fromIntegral fields
      cs' = Vec.map fromIntegral comps
      bs' = Vec.map fromIntegral bcs
  withVec fs' $ \fs -> withVec cs' $ \cs -> withVec bs' $ \bs -> do
    withVec bccs $ \bc -> withVec bcpt $ \pt -> with NoSection $ \psec -> do
      c'DMPlexCreateSection dm d n fs cs m bs bc pt perm psec >>= chkErr
      peek psec


-- * DMPlex tree stuff.
------------------------------------------------------------------------------
dmPlexGetDepth :: MonadIO m => DM -> m Int
dmPlexGetDepth dm = liftIO $ with 0 $ \ptr -> do
  c'DMPlexGetDepth dm ptr >>= chkErr >> ipeek ptr

-- | Returns `[start, end)` forall mesh "points," which includes `d`-cells
--   forall `d \in [0, dim]`.
dmPlexGetChart :: MonadIO m => DM -> m (Int, Int)
dmPlexGetChart dm = liftIO $ with 0 $ \ps -> with 0 $ \pe -> do
  c'DMPlexGetChart dm ps pe >>= chkErr
  (,) <$> ipeek ps <*> ipeek pe


-- ** Mesh constraints & boundary-conditions.
------------------------------------------------------------------------------
-- | Gets/sets the points that are anchored by outside values.
--   NOTE: After setting the anchors, then use `DMGetConstraints`, followed
--     by filling in the entries of the constraint matrix.
dmPlexSetAnchors :: MonadIO m => DM -> Section -> IS -> m ()
dmPlexSetAnchors dm sec is = liftIO $ do
  c'DMPlexSetAnchors dm sec is >>= chkErr

dmPlexGetAnchors :: MonadIO m => DM -> m (Section, IS)
dmPlexGetAnchors dm = liftIO $ with NoSection $ \ps -> with NoIS $ \pt -> do
  c'DMPlexGetAnchors dm ps pt >>= chkErr
  (,) <$> peek ps <*> peek pt

------------------------------------------------------------------------------
-- | Boundary marking, with the given label.
dmPlexMarkBoundaryFaces :: MonadIO m => DM -> DMLabel -> m ()
dmPlexMarkBoundaryFaces dm lab = liftIO $ do
  c'DMPlexMarkBoundaryFaces dm lab >>= chkErr

dmPlexLabelComplete :: MonadIO m => DM -> DMLabel -> m ()
dmPlexLabelComplete dm lab = liftIO $ do
  c'DMPlexLabelComplete dm lab >>= chkErr
