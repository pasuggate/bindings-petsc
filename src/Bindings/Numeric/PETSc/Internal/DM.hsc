{-# LANGUAGE OverloadedStrings, CPP, TemplateHaskell, DeriveGeneric,
             GeneralizedNewtypeDeriving, FlexibleInstances,
             PatternSynonyms, ViewPatterns #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal.DM
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Bindings for the grid/mesh (DM) module.
-- 
-- NOTE:
-- 
-- Changelog:
--  + 09/06/2017  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

#include <petsc.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <bindings.dsl.h>
module Bindings.Numeric.PETSc.Internal.DM where

#strict_import
import Foreign.Ptr
import Foreign.Marshal
import GHC.Generics (Generic)
import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Mat


-- * DM-related types.
------------------------------------------------------------------------------
-- | DM boundary types.
#integral_t DMBoundaryType
#num DM_BOUNDARY_NONE
#num DM_BOUNDARY_GHOSTED
#num DM_BOUNDARY_PERIODIC

-- | DMDA stencil types.
#integral_t DMDAStencilType
#num DMDA_STENCIL_BOX
#num DMDA_STENCIL_STAR


-- * Additional types for defining and using DM's.
------------------------------------------------------------------------------
-- TODO:
newtype BdyT     = BdyT     { unBdyT     :: PInt }
  deriving (Eq, Show, Storable, Generic)

newtype StencilT = StencilT { unStencilT :: PInt }
  deriving (Eq, Show, Storable, Generic)

type DMBoundaryType  = BdyT
type DMDAStencilType = StencilT

------------------------------------------------------------------------------
-- | No extra boundary-nodes.
pattern DM_BOUNDARY_NONE :: BdyT
pattern DM_BOUNDARY_NONE <- ((== BdyT c'DM_BOUNDARY_NONE) -> True)
  where DM_BOUNDARY_NONE  = BdyT c'DM_BOUNDARY_NONE

-- | Use ghost-nodes, outside of the domain, while formulating the problem's
--   boundary-conditions (though not present within the final systems-of-
--   equations, nor solutions).
pattern DM_BOUNDARY_GHOSTED :: BdyT
pattern DM_BOUNDARY_GHOSTED <- ((== BdyT c'DM_BOUNDARY_GHOSTED) -> True)
  where DM_BOUNDARY_GHOSTED  = BdyT c'DM_BOUNDARY_GHOSTED

-- | Wrap each dimension.
pattern DM_BOUNDARY_PERIODIC :: BdyT
pattern DM_BOUNDARY_PERIODIC <- ((== BdyT c'DM_BOUNDARY_PERIODIC) -> True)
  where DM_BOUNDARY_PERIODIC  = BdyT c'DM_BOUNDARY_PERIODIC

------------------------------------------------------------------------------
pattern BdyNone :: BdyT
pattern BdyNone  = DM_BOUNDARY_NONE

pattern BdyGhost :: BdyT
pattern BdyGhost  = DM_BOUNDARY_GHOSTED

pattern BdyWrap :: BdyT
pattern BdyWrap  = DM_BOUNDARY_PERIODIC

------------------------------------------------------------------------------
-- | Use `d`-boxes, for the stencils.
pattern DMDA_STENCIL_BOX :: StencilT
pattern DMDA_STENCIL_BOX <- ((== StencilT c'DMDA_STENCIL_BOX) -> True)
  where DMDA_STENCIL_BOX  = StencilT c'DMDA_STENCIL_BOX

-- | Use `d`-stars, for the stencils.
pattern DMDA_STENCIL_STAR :: StencilT
pattern DMDA_STENCIL_STAR <- ((== StencilT c'DMDA_STENCIL_STAR) -> True)
  where DMDA_STENCIL_STAR  = StencilT c'DMDA_STENCIL_STAR


-- ** DM types.
------------------------------------------------------------------------------
-- | Pointer to global arrays of the type-strings, for DM's.
newtype DMType = DMType { unDMType :: PStr }
               deriving (Eq, Show, Storable, Generic)

#globalarray DMDA     , CChar
#globalarray DMPLEX   , CChar
#globalarray DMFOREST , CChar
#globalarray DMMOAB   , CChar
#globalarray DMP4EST  , CChar
#globalarray DMP8EST  , CChar


-- ** DM field-related data-types.
------------------------------------------------------------------------------
-- | Callback-function data-type for imposing boundary-conditions.
#callback_t BCFun , PPReal -> PPScalar -> PCtx -> IO ()


-- * Data-structures for local DM queries.
------------------------------------------------------------------------------
-- | Information about the local portion of the problem.
--   TODO: Lenses?
data DMDALocalInfo a =
  DMDALocalInfo { _dim, _dof, _sw  :: a
                , _mx , _my , _mz  :: a
                , _xs , _ys , _zs  :: a
                , _xm , _ym , _zm  :: a
                , _gxs, _gys, _gzs :: a
                , _gxm, _gym, _gzm :: a
                , _bx , _by , _bz  :: BdyT
                , _st              :: StencilT
                , _da              :: DM
                } deriving (Eq, Show, Generic)

instance Functor DMDALocalInfo where
  fmap f (DMDALocalInfo dim dof sw mx my mz xs ys zs xm ym zm gxs gys gzs gxm gym gzm bx by bz st da) = DMDALocalInfo (f dim) (f dof) (f sw) (f mx) (f my) (f mz) (f xs) (f ys) (f zs) (f xm) (f ym) (f zm) (f gxs) (f gys) (f gzs) (f gxm) (f gym) (f gzm) bx by bz st da

-- TODO: This is so ugly -- can I use Generics, or something else, to make it
--   less so?
instance Storable (DMDALocalInfo PInt) where
  sizeOf  _ = 22*sizeOf (undefined :: PInt) + sizeOf nullPtr
  alignment = const $ alignment nullPtr
  peek ptr  = do
    xS <- peekArray 22 $ castPtr ptr
    da <- peek (plusPtr ptr $ 22*sizeOf (undefined :: PInt))
    let (dim:dof:sw:mx:my:mz:xs:ys:zs:xm:ym:zm:xS') = xS
        (gxs:gys:gzs:gxm:gym:gzm:xS'') = xS'
        [bx, by, bz] = BdyT <$> take 3 xS''
        st = StencilT $ last xS''
    return $ DMDALocalInfo dim dof sw mx my mz xs ys zs xm ym zm
      gxs gys gzs gxm gym gzm bx by bz st da
  poke ptr (DMDALocalInfo dim dof sw mx my mz xs ys zs xm ym zm
            gxs gys gzs gxm gym gzm bx by bz st da) = do
    let s = sizeOf (undefined :: PInt)
    pokeArray (castPtr ptr) [dim, dof, sw, mx, my, mz, xs, ys, zs, xm, ym, zm,
                             gxs, gys, gzs, gxm, gym, gzm]
    pokeArray (plusPtr ptr $ 18*s) [bx, by, bz]
    poke (plusPtr ptr $ 21*s) st
    poke (plusPtr ptr $ 22*s) da


-- * DM state constructors/destructors.
------------------------------------------------------------------------------
#ccall DMCreate     , Comm -> Ptr DM -> PRes
#ccall DMDestroy    ,         Ptr DM -> PRes

------------------------------------------------------------------------------
-- | DMDA-specific constructors.
#ccall DMDACreate1d , Comm -> BdyT -> PInt -> PInt -> PInt -> PPInt -> Ptr DM -> PRes
#ccall DMDACreate2d , Comm -> BdyT -> BdyT -> StencilT -> PInt -> PInt -> PInt -> PInt -> PInt -> PInt -> Ptr PInt -> Ptr PInt -> Ptr DM -> PRes


-- ** Additional setup functions.
------------------------------------------------------------------------------
#ccall DMSetFromOptions , DM -> PRes
#ccall DMSetUp , DM -> PRes

------------------------------------------------------------------------------
#callback_t DMCreateFun , DM -> PRes
newtype DMCreateFun = DMCreateFun { unDMCreateFun :: C'DMCreateFun }
  deriving (Eq, Show, Storable, Generic)

#ccall DMRegister    , DMType -> DMCreateFun -> PRes
#ccall DMRegisterAll , PRes

------------------------------------------------------------------------------
#ccall DMAddBoundary , DM -> PBool -> PStr -> PStr -> PInt -> PInt -> PPInt -> <BCFun> -> PInt -> PPInt -> PCtx -> PRes

#ccall DMSetApplicationContext , DM -> PCtx -> PRes


-- ** Conversons, labeling, and types.
------------------------------------------------------------------------------
#opaque_t _n_DMLabel
newtype DMLabel = DMLabel { unDMLabel :: Ptr C'_n_DMLabel }
  deriving (Eq, Ord, Show, Storable, Generic)

#ccall DMCreateLabel , DM -> PStr                -> PRes
#ccall DMHasLabel    , DM -> PStr -> Ptr PBool   -> PRes
#ccall DMGetLabel    , DM -> PStr -> Ptr DMLabel -> PRes

------------------------------------------------------------------------------
#ccall DMConvert , DM -> DMType -> Ptr DM -> PRes

------------------------------------------------------------------------------
#ccall DMLocalizeCoordinates , DM -> PRes
#ccall DMLocalizeCoordinate  , DM -> Ptr PScalar -> Ptr PScalar -> PRes

------------------------------------------------------------------------------
-- | Setter & getter for DM type.
#ccall DMSetType , DM ->     DMType -> PRes
#ccall DMGetType , DM -> Ptr DMType -> PRes

------------------------------------------------------------------------------
#ccall DMSetDimension , DM ->     PInt -> PRes
#ccall DMGetDimension , DM -> Ptr PInt -> PRes

#ccall DMSetCoordinateDim , DM ->     PInt -> PRes
#ccall DMGetCoordinateDim , DM -> Ptr PInt -> PRes


-- * Additional DM functions.
------------------------------------------------------------------------------
#ccall DMCreateGlobalVector , DM -> Ptr Vec -> PRes
#ccall DMCreateMatrix       , DM -> Ptr Mat -> PRes

------------------------------------------------------------------------------
#ccall DMSetDefaultConstraints , DM ->     Section ->     Mat -> PRes
#ccall DMGetDefaultConstraints , DM -> Ptr Section -> Ptr Mat -> PRes


-- ** DM's `Vec` & `Mat` functions.
------------------------------------------------------------------------------
-- NOTE: In `dm.c`, but not exported?
-- #ccall DMSetVec , DM -> Vec -> PRes

#ccall DMSetVecType , DM ->     VecType -> PRes
#ccall DMGetVecType , DM -> Ptr VecType -> PRes

#ccall VecSetDM , Vec ->     DM -> PRes
#ccall VecGetDM , Vec -> Ptr DM -> PRes

------------------------------------------------------------------------------
#ccall DMSetMatType , DM ->     MatType -> PRes
#ccall DMGetMatType , DM -> Ptr MatType -> PRes

#ccall MatSetDM , Mat ->     DM -> PRes
#ccall MatGetDM , Mat -> Ptr DM -> PRes


-- ** DM viewers.
------------------------------------------------------------------------------
#ccall DMView , DM -> Viewer -> PRes
#cinline DMViewFromOptions , DM -> PetscObject -> PStr -> PRes


-- * Refinement & coarsening functions.
------------------------------------------------------------------------------
#ccall DMRefine  , DM -> Comm -> Ptr DM -> PRes
#ccall DMCoarsen , DM -> Comm -> Ptr DM -> PRes


-- ** Use these hooks when custom data is needed to refine/coarsen.
--    NOTE: If multiple hooks are added, they'll be called in-order (of add).
------------------------------------------------------------------------------
#callback_t RefineHook , PPReal -> PPReal -> PRes
type    RefineHookT = DM -> PInt -> PPInt -> Ptr PPInt -> PRes
newtype RefineHook  = RefineHook { unRefineHook :: C'RefineHook }
  deriving (Eq, Show, Storable, Generic)

#callback_t InterpHook , PPReal -> PPReal -> PRes
type    InterpHookT = DM -> PInt -> PPInt -> Ptr PPInt -> PRes
newtype InterpHook  = InterpHook { unInterpHook :: C'InterpHook }
  deriving (Eq, Show, Storable, Generic)

#ccall DMRefineHookAdd , DM -> RefineHook -> InterpHook -> PCtx -> PRes

------------------------------------------------------------------------------
#callback_t CoarsenHook , PPReal -> PPReal -> PRes
type    CoarsenHookT = DM -> PInt -> PPInt -> Ptr PPInt -> PRes
newtype CoarsenHook  = CoarsenHook { unCoarsenHook :: C'CoarsenHook }
  deriving (Eq, Show, Storable, Generic)

#callback_t RestrictHook , PPReal -> PPReal -> PRes
type    RestrictHookT = DM -> PInt -> PPInt -> Ptr PPInt -> PRes
newtype RestrictHook  = RestrictHook { unRestrictHook :: C'RestrictHook }
  deriving (Eq, Show, Storable, Generic)

#ccall DMCoarsenHookAdd , DM -> CoarsenHook -> InterpHook -> PCtx -> PRes


------------------------------------------------------------------------------
#ccall DMSetFineDM   , DM ->     DM -> PRes
#ccall DMGetFineDM   , DM -> Ptr DM -> PRes
#ccall DMSetCoarseDM , DM ->     DM -> PRes
#ccall DMGetCoarseDM , DM -> Ptr DM -> PRes

#ccall DMGetRefineLevel  , DM -> PPInt -> PRes
#ccall DMGetCoarsenLevel , DM -> PPInt -> PRes

------------------------------------------------------------------------------
#ccall DMRefineHierarchy  , DM -> PInt -> Ptr DM -> PRes
#ccall DMCoarsenHierarchy , DM -> PInt -> Ptr DM -> PRes

#ccall DMCreateInterpolation , DM -> DM -> Ptr Mat -> Ptr Vec -> PRes
#ccall DMCreateRestriction   , DM -> DM -> Ptr Mat            -> PRes


-- * DM coordinates.
------------------------------------------------------------------------------
#ccall DMSetCoordinateDM , DM ->     DM -> PRes
#ccall DMGetCoordinateDM , DM -> Ptr DM -> PRes

#ccall DMSetCoordinates , DM -> Vec -> PRes
#ccall DMGetCoordinates , DM -> Ptr Vec -> PRes

#ccall DMSetCoordinatesLocal , DM -> Vec -> PRes
#ccall DMGetCoordinatesLocal , DM -> Ptr Vec -> PRes


-- * DMDA-specific functions.
------------------------------------------------------------------------------
#ccall DMDAGetInfo , DM -> PPInt -> PPInt -> PPInt -> PPInt -> PPInt -> PPInt -> PPInt -> PPInt -> PPInt -> Ptr BdyT -> Ptr BdyT -> Ptr BdyT -> Ptr StencilT -> PRes
#ccall DMDAGetLocalInfo , DM -> Ptr (DMDALocalInfo PInt) -> PRes

#ccall DMDAVecGetArrayRead , DM -> Vec -> Ptr (Ptr PPReal) -> PRes
#ccall DMDAVecGetArray     , DM -> Vec -> Ptr (Ptr PPReal) -> PRes
#ccall DMDAVecRestoreArrayRead , DM -> Vec -> Ptr (Ptr PPReal) -> PRes
#ccall DMDAVecRestoreArray     , DM -> Vec -> Ptr (Ptr PPReal) -> PRes

#ccall DMDAGetCorners      , DM -> PPInt -> PPInt -> PPInt -> PPInt -> PPInt -> PPInt -> PRes
#ccall DMDAGetGhostCorners , DM -> PPInt -> PPInt -> PPInt -> PPInt -> PPInt -> PPInt -> PRes


-- * Index-Set (IS) functions.
------------------------------------------------------------------------------
#ccall DMGetStratumIS , DM -> PStr -> PInt -> Ptr IS -> PRes
#ccall DMGetStratumSize , DM -> PStr -> PInt -> PPInt -> PRes

#ccall DMLabelStratumHasPoint , DMLabel -> PInt -> PInt -> PPBool -> PRes

------------------------------------------------------------------------------
{-
#ccall DMSetDefaultSection , DM ->     Section -> PRes
#ccall DMGetDefaultSection , DM -> Ptr Section -> PRes

#ccall DMSetDefaultGlobalSection , DM ->     Section -> PRes
#ccall DMGetDefaultGlobalSection , DM -> Ptr Section -> PRes
-}

#ccall DMSetCoordinateSection , DM ->     Section -> PRes
#ccall DMGetCoordinateSection , DM -> Ptr Section -> PRes


-- * Low-level access to node data.
------------------------------------------------------------------------------
-- | Global access.
#ccall DMGetGlobalVector     , DM -> Ptr Vec -> PRes
#ccall DMRestoreGlobalVector , DM -> Ptr Vec -> PRes
#ccall DMClearGlobalVectors  , DM            -> PRes

-- | Local access.
#ccall DMGetLocalVector      , DM -> Ptr Vec -> PRes
#ccall DMRestoreLocalVector  , DM -> Ptr Vec -> PRes
#ccall DMClearLocalVectors   , DM            -> PRes


-- ** Local & work arrays.
------------------------------------------------------------------------------
#ccall DMGetWorkArray     , DM -> PInt -> PetscT -> Ptr PCtx -> PRes
#ccall DMRestoreWorkArray , DM -> PInt -> PetscT -> Ptr PCtx -> PRes


-- ** MPI-related stuff.
------------------------------------------------------------------------------
#ccall DMGlobalToLocalBegin , DM -> Vec -> InsertMode -> Vec -> PRes
#ccall DMGlobalToLocalEnd   , DM -> Vec -> InsertMode -> Vec -> PRes


-- * DM discretisation (DS) stuff.
--   TODO: Move to another module?
------------------------------------------------------------------------------
-- | Wrapper for the `PetscDS` data-type.
#opaque_t _p_PetscDS
newtype DS = DS { unDS :: Ptr C'_p_PetscDS }
  deriving (Eq, Ord, Show, Storable, Generic)

------------------------------------------------------------------------------
-- | Wrapper for the `PetscDSType` data-type.
newtype DSType = DSType { unDSType :: CString }
  deriving (Eq, Show, Storable, Generic)

#globalarray PETSCDSBASIC , CChar


-- ** DS constructors & destructors.
------------------------------------------------------------------------------
#ccall PetscDSCreate  , Comm -> Ptr DS -> PRes
#ccall PetscDSDestroy ,         Ptr DS -> PRes


-- ** DS additional setup functionality.
------------------------------------------------------------------------------
#ccall PetscDSSetFromOptions , DS -> PRes
#ccall PetscDSSetUp          , DS -> PRes

#ccall PetscDSView , DS -> Viewer -> PRes

#ccall PetscDSSetType , DS ->     DSType -> PRes
#ccall PetscDSGetType , DS -> Ptr DSType -> PRes


{-- }
-- TODO: these have changed in later versions of PETSc?

-- ** PetscDS <-> DM functionality.
------------------------------------------------------------------------------
#ccall DMSetDS , DM ->     DS -> PRes
#ccall DMGetDS , DM -> Ptr DS -> PRes
--}

#ccall DMSetField , DM -> PInt ->     PetscObject -> PRes
#ccall DMGetField , DM -> PInt -> Ptr PetscObject -> PRes

------------------------------------------------------------------------------
-- TODO:
#callback_t CoordFn , PInt -> PReal -> PPReal -> PInt -> PPScalar -> PCtx -> PRes
type PCoordFn = Ptr C'CoordFn

#ccall DMProjectFunction , DM -> PReal -> PCoordFn -> PPCtx -> InsertMode -> Vec -> PRes


-- ** DS setters/getters.
------------------------------------------------------------------------------
#ccall PetscDSGetNumFields , DS -> PPInt -> PRes
#ccall PetscDSGetSpatialDimension , DS -> PPInt -> PRes

#ccall PetscDSSetDiscretization   , DS -> PInt ->     PetscObject -> PRes
#ccall PetscDSAddDiscretization   , DS         ->     PetscObject -> PRes
#ccall PetscDSGetDiscretization   , DS -> PInt -> Ptr PetscObject -> PRes

{-
#ccall PetscDSSetBdDiscretization , DS -> PInt ->     PetscObject -> PRes
#ccall PetscDSAddBdDiscretization , DS         ->     PetscObject -> PRes
#ccall PetscDSGetBdDiscretization , DS -> PInt -> Ptr PetscObject -> PRes
-}

------------------------------------------------------------------------------
-- | Residual- and Jacobian- function callbacks.
#callback_t DSFun , PInt -> PInt -> PInt -> PPInt -> PPInt -> PPScalar -> PPScalar -> PPScalar -> PPInt -> PPInt -> PPScalar -> PPScalar -> PPScalar -> PReal -> PPReal -> PPScalar -> PRes

#callback_t DSJac , PInt -> PInt -> PInt -> PPInt -> PPInt -> PPScalar -> PPScalar -> PPScalar -> PPInt -> PPInt -> PPScalar -> PPScalar -> PPScalar -> PReal -> PReal -> PPReal -> PPScalar -> PRes

type PDSFun = Ptr C'DSFun
type PDSJac = Ptr C'DSJac

------------------------------------------------------------------------------
-- | Set/get the residual & Jacobian evaluation functions.
#ccall PetscDSSetResidual   , DS -> PInt -> <DSFun> -> <DSFun> -> PRes
#ccall PetscDSGetResidual   , DS -> PInt -> PDSFun -> PDSFun -> PRes
#ccall PetscDSSetJacobian   , DS -> PInt -> PInt -> <DSJac> -> <DSJac> -> <DSJac> -> <DSJac> -> PRes
#ccall PetscDSGetJacobian   , DS -> PInt -> PInt -> PDSJac -> PDSJac -> PDSJac -> PDSJac -> PRes
#ccall PetscDSSetBdResidual , DS -> PInt -> <DSFun> -> <DSFun> -> PRes
#ccall PetscDSGetBdResidual , DS -> PInt -> PDSFun -> PDSFun -> PRes
#ccall PetscDSSetBdJacobian , DS -> PInt -> PInt -> <DSJac> -> <DSJac> -> <DSJac> -> <DSJac> -> PRes
#ccall PetscDSGetBdJacobian , DS -> PInt -> PInt -> PDSJac -> PDSJac -> PDSJac -> PDSJac -> PRes


-- * Finite Element (FE) discretisations.
------------------------------------------------------------------------------
#opaque_t _p_PetscFE
newtype PFE = PFE { unPFE :: Ptr C'_p_PetscFE }
  deriving (Eq, Ord, Show, Storable, Generic)


-- ** FE contructors & destructors.
------------------------------------------------------------------------------
#ccall PetscFECreate  , Comm -> Ptr PFE -> PRes
#ccall PetscFEDestroy ,         Ptr PFE -> PRes

#ccall PetscFECreateDefault , DM -> PInt -> PInt -> PBool -> PStr -> PInt -> Ptr PFE -> PRes


-- ** Additional FE setup.
------------------------------------------------------------------------------
#ccall PetscFESetFromOptions , PFE -> PRes
#ccall PetscFESetUp          , PFE -> PRes


-- ** FE setters/getters.
------------------------------------------------------------------------------
#ccall PetscFESetQuadrature , PFE ->     QRule -> PRes
#ccall PetscFEGetQuadrature , PFE -> Ptr QRule -> PRes


-- * Quadrature rules.
------------------------------------------------------------------------------
-- | Quadrature-rule data-types.
#opaque_t _p_PetscQuadrature
newtype QRule = QRule { unQRule :: Ptr C'_p_PetscQuadrature }
              deriving (Eq, Ord, Show, Storable, Generic)


-- ** Constructors & destructors.
------------------------------------------------------------------------------
#ccall PetscQuadratureCreate  , Comm -> Ptr QRule -> PRes
#ccall PetscQuadratureDestroy ,         Ptr QRule -> PRes


-- ** Quadrature-rule queries.
------------------------------------------------------------------------------
#ccall PetscQuadratureGetOrder , QRule -> PPInt -> PRes
#ccall PetscQuadratureGetData  , QRule -> PPInt -> PPInt -> Ptr PPReal -> Ptr PPReal -> PRes
