{-# LANGUAGE OverloadedStrings, CPP, TemplateHaskell, DeriveGeneric,
             GeneralizedNewtypeDeriving, FlexibleInstances,
             PatternSynonyms, ViewPatterns #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal.DMForest
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Bindings for DMForest submodule of the DM module.
-- 
-- NOTE:
-- 
-- Changelog:
--  + 20/08/2017  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

#include <petscdmforest.h>
#include <bindings.dsl.h>
module Bindings.Numeric.PETSc.Internal.DMForest where

#strict_import
import Foreign.Ptr
import Foreign.Marshal
import GHC.Generics (Generic)
import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.DM


-- * Forest data-types.
------------------------------------------------------------------------------
-- | Name of the base mesh topology.
--   NOTE: E.g., "shell", or "cube", and can be interpreted by subtypes of
--     `DMFOREST`.
newtype ForestTopology = ForestTopology { unForestTopology :: CString }
  deriving (Eq, Show, Storable, Generic)

------------------------------------------------------------------------------
-- | Enumerated type for flagging whether to keep/refine/coarsen a mesh
--   entity.
#integral_t DMForestAdaptivityPurpose
#num DM_FOREST_KEEP
#num DM_FOREST_REFINE
#num DM_FOREST_COARSEN

newtype DMAdapt = DMAdapt { unDMAdapt :: C'DMForestAdaptivityPurpose }
  deriving (Eq, Show, Storable, Generic)

------------------------------------------------------------------------------
#globalarray DMFORESTADAPTALL , CChar
#globalarray DMFORESTADAPTANY , CChar

newtype DMAdaptStrategy = DMAdaptStrategy { unDMAdaptStrategy :: CString }
  deriving (Eq, Show, Storable, Generic)


-- * Forest setup.
------------------------------------------------------------------------------
#ccall DMForestSetBaseDM , DM ->     DM -> PRes
#ccall DMForestGetBaseDM , DM -> Ptr DM -> PRes

------------------------------------------------------------------------------
#ccall DMIsForest , DM -> Ptr PBool -> PRes

#ccall DMForestRegisterType , DMType -> PRes


-- * Distribution & partitioning.
------------------------------------------------------------------------------
#ccall DMForestSetPartitionOverlap , DM ->     PInt -> PRes
#ccall DMForestGetPartitionOverlap , DM -> Ptr PInt -> PRes

------------------------------------------------------------------------------
#ccall DMForestSetCellWeights , DM ->     PPReal -> CopyMode -> PRes
#ccall DMForestGetCellWeights , DM -> Ptr PPReal             -> PRes

#ccall DMForestSetCellWeightFactor   , DM ->     PReal -> PRes
#ccall DMForestGetCellWeightFactor   , DM -> Ptr PReal -> PRes

-- #ccall DMForestSetCellWeightCapacity , DM ->     PReal -> PRes
-- #ccall DMForestGetCellWeightCapacity , DM -> Ptr PReal -> PRes


------------------------------------------------------------------------------
#ccall DMForestTemplate  , DM -> Comm -> Ptr DM -> PRes


-- * Forest topology & connectivity.
------------------------------------------------------------------------------
#ccall DMForestSetAdjacencyDimension   , DM ->     PInt -> PRes
#ccall DMForestGetAdjacencyDimension   , DM -> Ptr PInt -> PRes

#ccall DMForestSetAdjacencyCodimension , DM ->     PInt -> PRes
#ccall DMForestGetAdjacencyCodimension , DM -> Ptr PInt -> PRes

------------------------------------------------------------------------------
#ccall DMForestSetTopology , DM ->     ForestTopology -> PRes
#ccall DMForestGetTopology , DM -> Ptr ForestTopology -> PRes


-- * Forest refinement & coarsening.
------------------------------------------------------------------------------
#ccall DMForestSetInitialRefinement , DM ->     PInt -> PRes
#ccall DMForestGetInitialRefinement , DM -> Ptr PInt -> PRes

#ccall DMForestSetMaximumRefinement , DM ->     PInt -> PRes
#ccall DMForestGetMaximumRefinement , DM -> Ptr PInt -> PRes

#ccall DMForestSetMinimumRefinement , DM ->     PInt -> PRes
#ccall DMForestGetMinimumRefinement , DM -> Ptr PInt -> PRes

------------------------------------------------------------------------------
#ccall DMForestSetAdaptivityForest   , DM ->     DM      -> PRes
#ccall DMForestGetAdaptivityForest   , DM -> Ptr DM      -> PRes

#ccall DMForestSetAdaptivityLabel    , DM ->     DMLabel -> PRes
#ccall DMForestGetAdaptivityLabel    , DM -> Ptr DMLabel -> PRes

-- #ccall DMForestSetAdaptivitySuccess  , DM ->     PBool   -> PRes
-- #ccall DMForestGetAdaptivitySuccess  , DM -> Ptr PBool   -> PRes

#ccall DMForestSetAdaptivityPurpose  , DM ->     DMAdapt -> PRes
#ccall DMForestGetAdaptivityPurpose  , DM -> Ptr DMAdapt -> PRes

#ccall DMForestSetAdaptivityStrategy , DM ->     DMAdaptStrategy -> PRes
#ccall DMForestGetAdaptivityStrategy , DM -> Ptr DMAdaptStrategy -> PRes
