{-# LANGUAGE OverloadedStrings, CPP, TemplateHaskell, DeriveGeneric,
             GeneralizedNewtypeDeriving, FlexibleInstances,
             PatternSynonyms, ViewPatterns #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal.Partition
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Bindings for the grid/mesh (DM) module.
-- 
-- NOTE:
-- 
-- Changelog:
--  + 09/06/2017  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

#include <petscdmplex.h>
#include <bindings.dsl.h>
module Bindings.Numeric.PETSc.Internal.Partition where

#strict_import
import Foreign.Storable
import Foreign.Ptr
import Foreign.Marshal
import GHC.Generics (Generic)
import Bindings.Numeric.PETSc.Internal.Types


-- * Partitioner data-types.
------------------------------------------------------------------------------
#opaque_t _n_PetscPartitioner
newtype Partitioner =
  Partitioner { unPartitioner :: Ptr C'_n_PetscPartitioner }
  deriving (Eq, Show, Storable, Generic)

------------------------------------------------------------------------------
-- | Representation for the type of partitioner to use.
newtype PartitionerType =
  PartitionerType { unPartitionerType :: CString }
  deriving (Eq, Show, Storable, Generic)

#globalarray PETSCPARTITIONERCHACO    , CChar
#globalarray PETSCPARTITIONERPARMETIS , CChar
#globalarray PETSCPARTITIONERSHELL    , CChar
#globalarray PETSCPARTITIONERSIMPLE   , CChar
#globalarray PETSCPARTITIONERGATHER   , CChar


-- * Constructors & destructors.
------------------------------------------------------------------------------
#ccall PetscPartitionerCreate  , Comm -> Ptr Partitioner -> PRes
#ccall PetscPartitionerDestroy ,         Ptr Partitioner -> PRes


-- * Additional partitioner setup.
------------------------------------------------------------------------------
#ccall PetscPartitionerSetFromOptions , Partitioner -> PRes
#ccall PetscPartitionerSetUp          , Partitioner -> PRes

------------------------------------------------------------------------------
#ccall PetscPartitionerSetType , Partitioner ->     PartitionerType -> PRes
#ccall PetscPartitionerGetType , Partitioner -> Ptr PartitionerType -> PRes
