{-# LANGUAGE OverloadedStrings, CPP, TemplateHaskell, DeriveGeneric,
             GeneralizedNewtypeDeriving, PatternSynonyms, ViewPatterns #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal.TS
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Bindings for the Time-Stepping (TS) module.
-- 
-- NOTE:
-- 
-- Changelog:
--  + 05/07/2017  -- initial file;
-- 
-- TODO:
--  + use the assignment operators that the OpenGL bindings use?
-- 
------------------------------------------------------------------------------

#include <petsc.h>
#include <petscts.h>
#include <bindings.dsl.h>

module Bindings.Numeric.PETSc.Internal.TS where

#strict_import
import Foreign.C.String
import GHC.Generics (Generic)
import Bindings.Numeric.PETSc.Internal.Types


-- * Some TS type-aliases.
------------------------------------------------------------------------------
-- | Some TS function-pointers.
#callback_t TSIFunction   , TS -> PReal -> Vec -> Vec -> Vec -> PCtx -> PRes
#callback_t TSRHSFunction , TS -> PReal -> Vec        -> Vec -> PCtx -> PRes

#callback_t TSIJacobian   , TS -> PReal -> Vec -> Vec -> PReal -> Mat -> Mat -> PCtx -> PRes
#callback_t TSRHSJacobian , TS -> PReal -> Vec -> Mat -> Mat -> PCtx -> PRes

------------------------------------------------------------------------------
-- | Function-pointer aliases.
type TSCreateFunc  = Ptr () -- TODO:

------------------------------------------------------------------------------
-- | Callback functions of the form:
--     F(t, u, u_t)  ==  0
type TSIFunction   = TS -> PReal -> Vec -> Vec -> Vec -> PCtx -> PRes
type TSIFunPtr     = C'TSIFunction

type TSIJacPtr     = C'TSIJacobian
type TSIJacobian   = TS -> PReal -> Vec -> Vec -> PReal -> Mat -> Mat -> PCtx -> PRes

------------------------------------------------------------------------------
-- | Callback functions of the form:
--     F(t, u)  ==  u_t
type TSRHSFunction = TS -> PReal -> Vec -> Vec -> PCtx -> PRes
type TSRHSFunPtr   = C'TSRHSFunction

type TSRHSJacPtr   = C'TSRHSJacobian
type TSRHSJacobian = TS -> PReal -> Vec -> Mat -> Mat -> PCtx -> PRes

{-- }
------------------------------------------------------------------------------
-- | Callbacks for TS monitors.
#callback_t TSMonitor  , TS -> PInt -> PReal -> Vec ->     PCtx -> PRes
#callback_t TSFreeCtx  , TS ->                         Ptr PCtx -> PRes

newtype TSMonitor = TSMonitor { unTSMonitor :: C'TSMonitor }
                  deriving (Eq, Show, Storable, Generic)

newtype TSFreeCtx = TSFreeCtx { unTSFreeCtx :: C'TSFreeCtx }
                  deriving (Eq, Show, Storable, Generic)
--}


-- ** Time-stepping types.
------------------------------------------------------------------------------
newtype TSType = TSType { unTSType :: CString }
               deriving (Eq, Show, Storable, Generic)

------------------------------------------------------------------------------
-- | Some built-in TS algorithm types.
#globalarray TSTHETA  , CChar
#globalarray TSCN     , CChar
#globalarray TSBEULER , CChar


-- *** Convenience patterns for some TS types.
------------------------------------------------------------------------------
-- | General Theta Time-Stepping.
pattern TSTHETA :: TSType
pattern TSTHETA <- ((== TSType c'TSTHETA) -> True) where
  TSTHETA = TSType c'TSTHETA

-- | Crank-Nicolson Time-Stepping.
pattern TSCN :: TSType
pattern TSCN <- ((== TSType c'TSCN) -> True) where
  TSCN = TSType c'TSCN

-- | Backwards-Euler Time-Stepping.
pattern TSBEULER :: TSType
pattern TSBEULER <- ((== TSType c'TSBEULER) -> True) where
  TSBEULER = TSType c'TSBEULER


-- * Final time-step modes.
------------------------------------------------------------------------------
-- | How to handle the final time-step?
#integral_t TSExactFinalTimeOption
#num TS_EXACTFINALTIME_UNSPECIFIED
#num TS_EXACTFINALTIME_STEPOVER
#num TS_EXACTFINALTIME_INTERPOLATE
#num TS_EXACTFINALTIME_MATCHSTEP

newtype TSExactFinalTimeOption = TSExactFinalTimeOption {
  unTSExactFinalTimeOption :: C'TSExactFinalTimeOption
  } deriving (Eq, Show, Storable, Generic)

------------------------------------------------------------------------------
-- | Convenience patterns for the final time-step specification.
pattern TSFinalTimeUnspecified :: TSExactFinalTimeOption
pattern TSFinalTimeUnspecified <-
  ((== TSExactFinalTimeOption c'TS_EXACTFINALTIME_UNSPECIFIED) -> True) where
    TSFinalTimeUnspecified = TSExactFinalTimeOption c'TS_EXACTFINALTIME_UNSPECIFIED

pattern TSFinalTimeStepOver :: TSExactFinalTimeOption
pattern TSFinalTimeStepOver <-
  ((== TSExactFinalTimeOption c'TS_EXACTFINALTIME_STEPOVER) -> True) where
    TSFinalTimeStepOver = TSExactFinalTimeOption c'TS_EXACTFINALTIME_STEPOVER

pattern TSFinalTimeInterpolate :: TSExactFinalTimeOption
pattern TSFinalTimeInterpolate <-
  ((== TSExactFinalTimeOption c'TS_EXACTFINALTIME_INTERPOLATE) -> True) where
    TSFinalTimeInterpolate = TSExactFinalTimeOption c'TS_EXACTFINALTIME_INTERPOLATE

pattern TSFinalTimeMatchStep :: TSExactFinalTimeOption
pattern TSFinalTimeMatchStep <-
  ((== TSExactFinalTimeOption c'TS_EXACTFINALTIME_MATCHSTEP) -> True) where
    TSFinalTimeMatchStep = TSExactFinalTimeOption c'TS_EXACTFINALTIME_MATCHSTEP


-- * Problem-types.
------------------------------------------------------------------------------
-- | Specify whether the problem is linear or nonlinear.
#integral_t TSProblemType
#num TS_LINEAR
#num TS_NONLINEAR

newtype TSProblemType = TSProblemType { unTSProblemType :: C'TSProblemType }
                      deriving (Eq, Show, Storable, Generic)

------------------------------------------------------------------------------
-- | For linear problems.
pattern TSLinear :: TSProblemType
pattern TSLinear <- ((== TSProblemType c'TS_LINEAR) -> True) where
  TSLinear = TSProblemType c'TS_LINEAR

-- | For nonlinear problems.
pattern TSNonlinear :: TSProblemType
pattern TSNonlinear <- ((== TSProblemType c'TS_NONLINEAR) -> True) where
  TSNonlinear = TSProblemType c'TS_NONLINEAR


-- * TS state constructors/destructors, and basic setup.
------------------------------------------------------------------------------
#ccall TSCreate         , Comm -> Ptr TS -> IO PErr
#ccall TSSetUp          ,             TS -> IO PErr
#ccall TSSetFromOptions ,             TS -> IO PErr
#ccall TSDestroy        ,         Ptr TS -> IO PErr


-- * Set TS types.
------------------------------------------------------------------------------
#ccall TSSetType , TS ->     TSType -> IO PErr
#ccall TSGetType , TS -> Ptr TSType -> IO PErr

#ccall TSSetProblemType , TS ->     TSProblemType -> PRes
#ccall TSGetProblemType , TS -> Ptr TSProblemType -> PRes


-- ** Register additional TS types.
------------------------------------------------------------------------------
#ccall TSRegister , PStr -> TSCreateFunc -> IO PErr


-- ** TS solver & settings.
------------------------------------------------------------------------------
#ccall TSSolve , TS -> Vec -> IO PErr

-- | Set the initial solution, read the final solution.
#ccall TSSetSolution , TS ->     Vec -> PRes
#ccall TSGetSolution , TS -> Ptr Vec -> PRes

-- | Get solution-related values.
#ccall TSGetSolveTime      , TS -> PPReal -> PRes
#ccall TSGetTimeStepNumber , TS -> PPInt  -> PRes

------------------------------------------------------------------------------
-- | Setup callback-functions, for the solver.
#ccall TSSetIFunction   , TS -> Vec ->        TSIFunPtr   -> PCtx -> PRes
#ccall TSSetRHSFunction , TS -> Vec ->        TSRHSFunPtr -> PCtx -> PRes
#ccall TSSetIJacobian   , TS -> Mat -> Mat -> TSIJacPtr   -> PCtx -> PRes
#ccall TSSetRHSJacobian , TS -> Mat -> Mat -> TSRHSJacPtr -> PCtx -> PRes

------------------------------------------------------------------------------
-- | Simulation time & time-steps.
#ccall TSSetDuration , TS -> PInt -> PReal -> IO PErr
#ccall TSSetTimeStep , TS         -> PReal -> IO PErr

#ccall TSSetTime , TS ->     PReal -> IO PErr
#ccall TSGetTime , TS -> Ptr PReal -> IO PErr

#ccall TSSetInitialTimeStep , TS -> PReal -> PReal -> PRes
#ccall TSSetExactFinalTime  , TS -> TSExactFinalTimeOption -> PRes


-- ** Additional settings & queries.
------------------------------------------------------------------------------
#ccall TSGetSNES , TS -> Ptr SNES -> PRes

#ccall TSSetDM   , TS ->     DM   -> PRes
#ccall TSGetDM   , TS -> Ptr DM   -> PRes


-- * TS monitoring.
------------------------------------------------------------------------------
{-- }
#ccall TSSetMonitor    , TS -> TSMonitor -> PCtx -> TSFreeCtx   -> PRes
#ccall TSMonitorCancel , TS -> PRes
--}

-- ^ default monitor:
--   TODO:
-- #ccall TSMonitorDefault , TS -> PInt -> PReal -> Vec -> PViewerAndFormat -> PRes
#ccall TSMonitorDefault , TS -> PInt -> PReal -> Vec -> PCtx -> PRes
