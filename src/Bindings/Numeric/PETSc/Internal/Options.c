#include <bindings.cmacros.h>
#include <petscoptions.h>


extern PetscErrorCode ChkErr(PetscErrorCode ierr);


#define SIZEOF_OPTION_ITEMS (sizeof(PetscOptionItems))

// BC_INLINE0(SIZEOF_OPTION_ITEMS, size_t)


/*
PetscInt sizeOfOptionItems(void)
{
  return 

PetscErrorCode PetscOptsBegin(MPI_Comm comm, char* prfx, char* desc, char* mmod, PetscOptionItems *base)
{
  return PetscOptionsBegin(comm, prfx, desc, mmod);
}

PetscErrorCode OptionsBegin(MPI_Comm comm, char* prefix, char* mess, char*sec)
{
  PetscOptionItems POOBase;
  PetscOptionItems *POO = &POOBase;

  PetscMemzero(POO,sizeof(PetscOptionItems));
  for (POO->count=(PetscOptionsPublish?-1:1); POO->count<2; POO->count++) {
    PetscErrorCode _5_ierr = PetscOptionsBegin_Private(POO,comm,prefix,mess,sec);
    CHKERRQ(_5_ierr);
    _5_ierr = PetscOptionsEnd_Private(POO);
    CHKERRQ(_5_ierr);
  }
}
*/
