{-# LANGUAGE OverloadedStrings, CPP, TemplateHaskell, DeriveGeneric,
             PatternSynonyms, ViewPatterns #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal.KSP
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Bindings for the Krylov SubsPace (KSP) module.
-- 
-- NOTE:
-- 
-- Changelog:
--  + 09/06/2017  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

#include <petsc.h>
#include <petscksp.h>
#include <petscdm.h>
#include <bindings.dsl.h>

module Bindings.Numeric.PETSc.Internal.KSP where

#strict_import
import Bindings.Numeric.PETSc.Internal.Types


-- ^ KSP state constructors/destructors:
#ccall KSPCreate               , Comm    -> Ptr KSP -> IO PErr
#ccall KSPDestroy              ,            Ptr KSP -> IO PErr

-- ^ KSP general settings:
#ccall KSPSetUp                , KSP            -> IO PErr
#ccall KSPSetFromOptions       , KSP            -> IO PErr
#ccall KSPSetType              , KSP -> KSPType -> IO PErr

-- ^ KSP mesh/grid settings:
#ccall KSPSetDM                , KSP -> DM      -> IO PErr
#ccall KSPGetDM                , KSP -> Ptr DM  -> IO PErr

-- ^ KSP solvers:
#ccall KSPSetOperators         , KSP -> Mat -> Mat -> IO PErr
#ccall KSPSolve                , KSP -> Vec -> Vec -> IO PErr

-- ^ KSP LHS/RHS settings:
#ccall KSPSetComputeRHS        , KSP -> <KSPRHSFun> -> Ptr () -> IO PErr
#ccall KSPSetComputeOperators  , KSP -> <KSPOpsFun> -> Ptr () -> IO PErr
