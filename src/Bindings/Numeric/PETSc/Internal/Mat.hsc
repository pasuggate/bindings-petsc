{-# LANGUAGE CPP, ForeignFunctionInterface, ViewPatterns, PatternSynonyms,
             DeriveGeneric, GeneralizedNewtypeDeriving
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal.Mat
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Low-level bindings to the `Mat` objects of PETSc.
-- 
-- Changelog:
--  + 11/06/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

#include <petscsnes.h>
#include <petscmat.h>
#include <petscdm.h>
#include <bindings.dsl.h>

module Bindings.Numeric.PETSc.Internal.Mat where

#strict_import
import Foreign.Ptr
import Foreign.Marshal
import GHC.Generics (Generic)
import Bindings.Numeric.PETSc.Internal.Types


-- * Matrix types & options.
------------------------------------------------------------------------------
-- | Internal type/data-layout of PETSc matrices.
#globalarray MATAIJ    , CChar
#globalarray MATMPIAIJ , CChar
#globalarray MATDENSE  , CChar

newtype MatType = MatType { unMatType :: CString }
                deriving (Eq, Show, Storable, Generic)

------------------------------------------------------------------------------
-- | Types for the various properties and options, of a matrix.
#integral_t MatOption
#num MAT_SYMMETRIC
#num MAT_SPD

newtype MatOption = MatOption { unMatOption :: C'MatOption }
                  deriving (Eq, Ord, Enum, Num, Show, Generic)

------------------------------------------------------------------------------
-- | Stencil for updating matrices.
data MatStencil = MatStencil { _k, _j, _i, _c :: PInt }
                deriving (Eq, Show, Generic)

instance Storable MatStencil where
  sizeOf  _ = 4*sizeOf (undefined :: PInt)
  alignment = const $ alignment (undefined :: Int)
  peek p = do [k, j, i, c] <- peekArray 4 $ castPtr p
              return $ MatStencil k j i c
  poke p (MatStencil k j i c) = pokeArray (castPtr p) [k, j, i, c]

------------------------------------------------------------------------------
#integral_t MatAssemblyType
#num MAT_FINAL_ASSEMBLY
#num MAT_FLUSH_ASSEMBLY

newtype MatAssemblyType = MatAssemblyType { unMatAssemblyType :: PInt }
                        deriving (Eq, Ord, Enum, Show, Generic, Storable)

-- | Patterns for the assembly types.
pattern MAT_FINAL_ASSEMBLY :: MatAssemblyType
pattern MAT_FINAL_ASSEMBLY <- ((== MatAssemblyType c'MAT_FINAL_ASSEMBLY) -> True)
  where MAT_FINAL_ASSEMBLY = MatAssemblyType c'MAT_FINAL_ASSEMBLY

pattern MAT_FLUSH_ASSEMBLY :: MatAssemblyType
pattern MAT_FLUSH_ASSEMBLY <- ((== MatAssemblyType c'MAT_FLUSH_ASSEMBLY) -> True)
  where MAT_FLUSH_ASSEMBLY = MatAssemblyType c'MAT_FLUSH_ASSEMBLY


-- * Matrix types- & option- convenience patterns.
------------------------------------------------------------------------------
-- | Sparse `AIJ` format, for the matrix data & indices.
pattern MATAIJ :: MatType
pattern MATAIJ <- ((== MatType c'MATAIJ) -> True) where
  MATAIJ = MatType c'MATAIJ

-- | Sparse, parallel `AIJ` format, for the matrix data & indices.
pattern MATMPIAIJ :: MatType
pattern MATMPIAIJ <- ((== MatType c'MATMPIAIJ) -> True) where
  MATMPIAIJ = MatType c'MATMPIAIJ

-- | Dense matrix format.
pattern MATDENSE :: MatType
pattern MATDENSE <- ((== MatType c'MATDENSE) -> True) where
  MATDENSE = MatType c'MATDENSE

------------------------------------------------------------------------------
-- | Symmetric matrix?
pattern MatSymmetric :: MatOption
pattern MatSymmetric <- ((== c'MAT_SYMMETRIC) -> True) where
  MatSymmetric = c'MAT_SYMMETRIC

-- | Symmetric and positive-definite matrix?
pattern MatSPD :: MatOption
pattern MatSPD <- ((== c'MAT_SPD) -> True) where
  MatSPD = c'MAT_SPD


-- * Basic `Mat` matrix operations.
------------------------------------------------------------------------------
#ccall MatCreate         , Comm -> Ptr Mat -> PRes
#ccall MatSetFromOptions ,             Mat -> PRes
#ccall MatSetUp          ,             Mat -> PRes
#ccall MatDestroy        ,         Ptr Mat -> PRes

#ccall MatView , Mat -> Viewer -> PRes

------------------------------------------------------------------------------
-- | Getters/setters.
#ccall MatSetSizes  , Mat  -> PInt -> PInt -> PInt -> PInt -> PRes
#ccall MatGetSize   , Mat  -> Ptr PInt     -> Ptr PInt     -> PRes
#ccall MatSetType   , Mat  -> MatType            -> PRes
#ccall MatSetOption , Mat  -> MatOption -> PBool -> PRes

------------------------------------------------------------------------------
-- | Begin/end assembly.
#ccall MatAssemblyBegin , Mat -> MatAssemblyType -> PRes
#ccall MatAssemblyEnd   , Mat -> MatAssemblyType -> PRes

------------------------------------------------------------------------------
-- | Matrix-free operations.
#ccall MatCreateSNESMF , SNES -> Ptr Mat -> PRes
#ccall MatCreateMFFD   , Comm -> PInt -> PInt -> PInt -> PInt -> Ptr Mat -> PRes
#ccall MatMFFDComputeJacobian , SNES -> Vec -> Mat -> Mat -> Ptr () -> PRes
#ccall SNESComputeJacobianDefault , SNES -> Vec -> Mat -> Mat -> Ptr () -> PRes
#ccall SNESComputeJacobianDefaultColor , SNES -> Vec -> Mat -> Mat -> Ptr () -> PRes


-- * Matric values and memory allocation.
------------------------------------------------------------------------------
#ccall MatCreateMPIAIJWithArrays , Comm -> PInt -> PInt -> PInt -> PInt -> PPInt -> PPInt -> PPScalar -> Ptr Mat -> PRes
#ccall MatMPIAIJSetPreallocationCSR , Mat -> PPInt -> PPInt -> PPScalar -> PRes


-- * Matrix Queries.
------------------------------------------------------------------------------
#ccall MatAssembled , Mat -> PPBool -> PRes


-- * Matrix-element setters & getters.
------------------------------------------------------------------------------
#ccall MatSetValuesStencil , Mat -> PInt -> Ptr MatStencil -> PInt -> Ptr MatStencil -> Ptr PScalar -> InsertMode -> PRes

#ccall MatGetValues , Mat -> PInt -> PPInt -> PInt -> PPInt -> PPScalar -> PRes

#ccall MatStoreValues    , Mat -> PRes
#ccall MatRetrieveValues , Mat -> PRes


-- * Matrix operations on coordinates (e.g., for mesh refinement &
--   coarsening).
------------------------------------------------------------------------------
#ccall MatInterpolate , Mat -> Vec -> Vec -> PRes
#ccall MatRestrict    , Mat -> Vec -> Vec -> PRes


-- * Matrix null space functions.
------------------------------------------------------------------------------
#ccall MatNullSpaceCreate  , Comm -> Ptr MatNullSpace -> PRes
#ccall MatNullSpaceDestroy ,         Ptr MatNullSpace -> PRes

#ccall MatNullSpaceRemove  , MatNullSpace -> Vec -> PRes
#ccall MatSetNullSpace     , Mat ->     MatNullSpace -> PRes
#ccall MatGetNullSpace     , Mat -> Ptr MatNullSpace -> PRes
