{-# LANGUAGE OverloadedStrings, CPP, TemplateHaskell, DeriveGeneric,
             GeneralizedNewtypeDeriving, FlexibleInstances,
             PatternSynonyms, ViewPatterns #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal.DMPlex
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Bindings for the grid/mesh (DM) module.
-- 
-- NOTE:
-- 
-- Changelog:
--  + 05/08/2017  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

#include <petscdmplex.h>
#include <bindings.dsl.h>
module Bindings.Numeric.PETSc.Internal.DMPlex where

#strict_import
import Foreign.Ptr
import Foreign.Marshal
import GHC.Generics (Generic)
import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.DM


-- * DMPlex constructors.
------------------------------------------------------------------------------
#ccall DMPlexCreate , Comm -> Ptr DM -> PRes
#ccall DMPlexCreateBoxMesh , Comm -> PInt -> PBool -> Ptr DM -> PRes
-- #ccall DMPlexCreateHexBoxMesh , Comm -> PInt -> PPInt -> BdyT -> BdyT -> BdyT -> Ptr DM -> PRes
#ccall DMPlexCreateFromFile , Comm -> PStr -> PBool -> Ptr DM -> PRes
#ccall DMPlexCreateFromDAG , DM -> PInt -> PPInt -> PPInt -> PPInt -> PPInt -> PPScalar -> PRes


-- * Additional DMPlex setup.
------------------------------------------------------------------------------
#ccall DMPlexCreateClosureIndex , DM -> Section -> PRes

#ccall DMPlexGetClosureIndices , DM -> Section -> Section -> PInt -> PPInt -> Ptr PPInt -> PPInt -> PRes
#ccall DMPlexRestoreClosureIndices , DM -> Section -> Section -> PInt -> PPInt -> Ptr PPInt -> PPInt -> PRes

#ccall DMPlexInterpolate , DM -> Ptr DM -> PRes


-- * DMPlex connectivity.
-- ** Cone functions.
------------------------------------------------------------------------------
#ccall DMPlexGetConeSize         , DM -> PInt ->     PPInt -> PRes
#ccall DMPlexGetCone             , DM -> PInt -> Ptr PPInt -> PRes
#ccall DMPlexGetCones            , DM ->         Ptr PPInt -> PRes
#ccall DMPlexGetConeOrientations , DM ->         Ptr PPInt -> PRes
#ccall DMPlexGetConeSection      , DM -> Ptr Section       -> PRes


-- ** DMPlex adjacency setup & queries,
------------------------------------------------------------------------------
#callback_t AdjFun , DM -> PInt -> PPInt -> Ptr PPInt -> PRes
type    AdjFunT = DM -> PInt -> PPInt -> Ptr PPInt -> PRes
newtype AdjFun  = AdjFun { unAdjFun :: C'AdjFun }
  deriving (Eq, Show, Storable, Generic)

#ccall DMPlexGetAdjacency , DM -> PInt -> PPInt -> Ptr PPInt -> PRes

{-- }
-- TODO: already come and gone from PETSc?
------------------------------------------------------------------------------
#ccall DMPlexSetAdjacencyUseCone    , DM ->  PBool -> PRes
#ccall DMPlexGetAdjacencyUseCone    , DM -> PPBool -> PRes
#ccall DMPlexSetAdjacencyUseClosure , DM ->  PBool -> PRes
#ccall DMPlexGetAdjacencyUseClosure , DM -> PPBool -> PRes
--}

------------------------------------------------------------------------------
{-- }
-- TODO: Not yet part of PETSc?
#ccall DMPlexSetAdjacencyUser , DM ->     AdjFun ->     PCtx -> PRes
#ccall DMPlexGetAdjacencyUser , DM -> Ptr AdjFun -> Ptr PCtx -> PRes
--}


-- ** Support functions.
------------------------------------------------------------------------------
#ccall DMPlexGetSupportSize    , DM -> PInt ->     PPInt -> PRes
#ccall DMPlexGetSupport        , DM -> PInt -> Ptr PPInt -> PRes
#ccall DMPlexGetSupportSection , DM -> Ptr Section       -> PRes


-- * MPI data-distribution & query functions.
------------------------------------------------------------------------------
#ccall DMPlexDistribute        , DM -> PInt -> Ptr SF -> Ptr DM -> PRes
#ccall DMPlexDistributeOverlap , DM -> PInt -> Ptr SF -> Ptr DM -> PRes

------------------------------------------------------------------------------
#ccall DMPlexDistributeField , DM -> SF -> Section -> Vec -> Section -> Vec -> PRes

-- #opaque_t MPI_Datatype
-- #ccall DMPlexDistributeData  , DM -> SF -> Section -> <MPI_Datatype> -> Ptr () -> Section -> Ptr (Ptr ()) -> PRes

------------------------------------------------------------------------------
#ccall DMPlexVecSetClosure , DM -> Section -> Vec -> PInt -> PPScalar -> InsertMode -> PRes
#ccall DMPlexVecGetClosure , DM -> Section -> Vec -> PInt -> PPInt -> Ptr PPScalar -> PRes
#ccall DMPlexVecRestoreClosure , DM -> Section -> Vec -> PInt -> PPInt -> Ptr PPScalar -> PRes


-- * DMPlex queries.
------------------------------------------------------------------------------
#ccall DMPlexEqual , DM -> DM -> PPBool -> PRes

#ccall DMPlexGetPointLocal , DM -> PInt -> PPInt -> PPInt -> PRes

#ccall DMPlexGetMeet     , DM -> PInt -> PPInt -> PPInt -> Ptr PPInt -> PRes
#ccall DMPlexRestoreMeet , DM -> PInt -> PPInt -> PPInt -> Ptr PPInt -> PRes
#ccall DMPlexGetJoin     , DM -> PInt -> PPInt -> PPInt -> Ptr PPInt -> PRes
#ccall DMPlexRestoreJoin , DM -> PInt -> PPInt -> PPInt -> Ptr PPInt -> PRes


-- * Refinement functions.
------------------------------------------------------------------------------
#callback_t RefineFun , PPReal -> PPReal -> PRes
type    RefineFunT = PPReal -> PPReal -> PRes
newtype RefineFun  = RefineFun { unRefineFun :: C'RefineFun }
  deriving (Eq, Show, Storable, Generic)

------------------------------------------------------------------------------
#ccall DMPlexSetRefinementFunction , DM ->     RefineFun -> PRes
#ccall DMPlexGetRefinementFunction , DM -> Ptr RefineFun -> PRes
#ccall DMPlexSetRefinementLimit    , DM ->     PReal     -> PRes
#ccall DMPlexGetRefinementLimit    , DM -> Ptr PReal     -> PRes
#ccall DMPlexSetRefinementUniform  , DM ->     PBool     -> PRes
#ccall DMPlexGetRefinementUniform  , DM -> Ptr PBool     -> PRes


-- * Mesh-checking routines.
------------------------------------------------------------------------------
#ccall DMPlexCheckSymmetry , DM                  -> PRes
#ccall DMPlexCheckSkeleton , DM -> PBool -> PInt -> PRes
#ccall DMPlexCheckFaces    , DM -> PBool -> PInt -> PRes


-- * Index-Set (IS) functions.
------------------------------------------------------------------------------
#ccall DMPlexCreateSection , DM -> PInt -> PInt -> PPInt -> PPInt -> PInt -> PPInt -> Ptr IS -> Ptr IS -> IS -> Ptr Section -> PRes


-- * DMPlex tree & layer functions.
------------------------------------------------------------------------------
#ccall DMPlexGetDepth , DM -> PPInt          -> PRes
#ccall DMPlexGetChart , DM -> PPInt -> PPInt -> PRes

------------------------------------------------------------------------------
#ccall DMPlexGetDepthStratum , DM -> PInt -> PPInt -> PPInt -> PRes
#ccall DMPlexGetHeightStratum , DM -> PInt -> PPInt -> PPInt -> PRes

------------------------------------------------------------------------------
#ccall DMPlexCreateDefaultReferenceTree , Comm -> PInt -> PBool -> Ptr DM -> PRes
#ccall DMPlexSetReferenceTree , DM ->     DM -> PRes
#ccall DMPlexGetReferenceTree , DM -> Ptr DM -> PRes

#ccall DMPlexSetTree , DM -> Section ->     PPInt ->     PPInt -> PRes
#ccall DMPlexGetTree , DM -> Section -> Ptr PPInt -> Ptr PPInt -> PRes

#ccall DMPlexGetTreeParent   , DM -> PInt -> Ptr PInt -> Ptr  PInt -> PRes
#ccall DMPlexGetTreeChildren , DM -> PInt -> Ptr PInt -> Ptr PPInt -> PRes


-- ** Mesh constraints & boundary-conditions.
------------------------------------------------------------------------------
-- | Gets/sets the points that are anchored by outside values.
--   NOTE: After setting the anchors, then use `DMGetConstraints`, followed
--     by filling in the entries of the constraint matrix.
#ccall DMPlexSetAnchors , DM ->     Section ->     IS -> PRes
#ccall DMPlexGetAnchors , DM -> Ptr Section -> Ptr IS -> PRes

------------------------------------------------------------------------------
-- | Boundary labeling.
#ccall DMPlexMarkBoundaryFaces , DM -> DMLabel -> PRes
#ccall DMPlexLabelComplete     , DM -> DMLabel -> PRes
