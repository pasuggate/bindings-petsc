{-# LANGUAGE GeneralizedNewtypeDeriving, CPP, TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings, PatternSynonyms, ViewPatterns #-}
{-# LANGUAGE DeriveGeneric, TypeSynonymInstances, FlexibleInstances #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal.Types
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Some PETSc types that are used by several modules.
-- 
-- NOTE:
-- 
-- Changelog:
--  + ??/??/2016  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

#include <petscsnes.h>
#include <petscdm.h>
#include <petscdmda.h>
#include <bindings.dsl.h>

module Bindings.Numeric.PETSc.Internal.Types
  ( PInt, PReal, PErr, PStr, PBool, PScalar
  , PPInt, PPBool, PPStr, PPReal, PPScalar
  , PObj, PCtx, PPCtx, PRes
  , Argv
  , MInt
  , C'PetscDataType
  , PetscErrorCode (..), C'PetscErrorCode
  , SizeT, C'size_t
  , pattern PMaxPathLen
  -- core types:
  , C'MPI_Comm, Comm (..)
  , pattern NoComm
  , p'PETSC_COMM_WORLD
  , DM (..)
  , pattern NoDM
  , TS (..)
  , pattern NoTS
  -- matrices and vectors:
  , Vec (..), VecType (..)
  , c'VECSEQ
  , c'VECMPI
  , c'VECSTANDARD
  , pattern NoVec
  , Mat (..)
  , pattern NoMat
  -- other core types:
  , SNES (..)
  , SNESType (..)
  , C'F (..), C'J (..), Func, Jaco
  , mk'F, mk'J
  , pattern PDecide
  , pattern PDetermine
  , pattern PDefault
  , pattern PIgnore
  -- more core types:
  , KSP (..), C'KSPRHSFun, C'KSPOpsFun, KSPType (..)
  , PetscObject (..), PetscT (..)
  , pattern NoObject
  , pattern NoObj
  -- copy modes:
  , CopyMode (..)
  , pattern CopyValues
  , pattern OwnPointer
  , pattern UsePointer
  -- file types:
  , PFileMode (..)
  , pattern FileRead
  , pattern FileWrite
  , pattern FileAppend
  , pattern FileUpdate
  , pattern FileAppendUpdate
  -- viewers:
  , Viewer (..), PViewer
  -- convenience patterns for core-types:
  , C'PetscBool, pattern PetscBool
  , pattern PetscInt
  , pattern PetscDouble
  , pattern PetscString
  , pattern PetscReal
  -- insert-mode:
  , pattern InsertValues
  , pattern AddValues
  , InsertMode (..)
  -- PETSc codes:
  , pattern PetscSuccess
  -- PETSc solvers:
  , pattern SNESNewtonLS
  , pattern SNESNGMRES
  , pattern SNESNCG
  -- additional types & patterns:
  , StarForest (..), PetscSF, PPSF, SF
  , pattern NoSF
  , noSF
  , Section (..)
  , pattern NoSection
  , IS (..)
  , pattern NoIS
  , MatNullSpace (..)
  , pattern NoMatNullSpace
  ) where

#strict_import
import Foreign.Ptr
import Foreign.C.String
import GHC.Generics
import System.IO.Unsafe
import Data.String
import qualified Data.ByteString.Char8 as BS


-- * Convenience aliases.
------------------------------------------------------------------------------
type PInt    = C'PetscInt
type PReal   = C'PetscReal
type PErr    = PetscErrorCode
type PStr    = CString
type PBool   = C'PetscBool
type PScalar = C'PetscScalar

-- ^ arrays of, and references to, PETSc values:
type PPBool = Ptr PBool
type PPStr  = Ptr PStr
type PPInt  = Ptr PInt
type PPReal = Ptr PReal
type PPScalar = Ptr PScalar

type PObj   = PetscObject
type PCtx   = Ptr ()
type PPCtx  = Ptr PCtx
type PRes   = IO PetscErrorCode

type Argv   = Ptr CString

type Func   = C'F
type Jaco   = C'J

-- ^ MPI data-types.
type MInt   = C'PetscMPIInt


-- ** Values that encode PETSc data-types.
------------------------------------------------------------------------------
#integral_t PetscDataType
#num PETSC_INT
#num PETSC_DOUBLE
#num PETSC_COMPLEX
#num PETSC_LONG
#num PETSC_SHORT
#num PETSC_FLOAT
#num PETSC_CHAR
#num PETSC_BIT_LOGICAL
#num PETSC_ENUM
#num PETSC_BOOL
-- #num PETSC_FLOAT128
#num PETSC_OBJECT
#num PETSC_FUNCTION
#num PETSC_STRING

newtype PetscT = PetscT { unPetscT :: C'PetscDataType }
               deriving (Eq, Show, Storable, Generic)

------------------------------------------------------------------------------
pattern PetscBool   :: PetscT
pattern PetscBool   <- ((== PetscT c'PETSC_BOOL) -> True)
  where PetscBool    = PetscT c'PETSC_BOOL

pattern PetscInt    :: PetscT
pattern PetscInt    <- ((== PetscT c'PETSC_INT) -> True)
  where PetscInt     = PetscT c'PETSC_INT

pattern PetscDouble :: PetscT
pattern PetscDouble <- ((== PetscT c'PETSC_DOUBLE) -> True)
  where PetscDouble  = PetscT c'PETSC_DOUBLE

pattern PetscString :: PetscT
pattern PetscString <- ((== PetscT c'PETSC_STRING) -> True)
  where PetscString  = PetscT c'PETSC_STRING

------------------------------------------------------------------------------
-- TODO: These types should be set from PETSc configuration.
pattern PetscReal   :: PetscT
pattern PetscReal   <- ((== PetscT c'PETSC_DOUBLE) -> True)
  where PetscReal    = PetscT c'PETSC_DOUBLE

pattern Petscscalar :: PetscT
pattern Petscscalar <- ((== PetscT c'PETSC_DOUBLE) -> True)
  where Petscscalar  = PetscT c'PETSC_DOUBLE


-- * Naughty instances.
------------------------------------------------------------------------------
instance IsString CString where
  fromString = unsafePerformIO . newCString


-- * Some standard C types.
------------------------------------------------------------------------------
#integral_t size_t
type SizeT = C'size_t

#num PETSC_MAX_PATH_LEN
pattern PMaxPathLen :: SizeT
pattern PMaxPathLen <- ((== c'PETSC_MAX_PATH_LEN) -> True)
  where PMaxPathLen = c'PETSC_MAX_PATH_LEN


-- * Petsc integral types.
------------------------------------------------------------------------------
-- TODO: Make `pattern's for these?
#integral_t PetscErrorCode
#num PETSC_ERR_MIN_VALUE        /* always one less than the smallest value */

#num PETSC_ERR_MEM              /* unable to allocate requested memory */
#num PETSC_ERR_SUP              /* no support for requested operation */
#num PETSC_ERR_SUP_SYS          /* no support for requested operation on this computer system */
#num PETSC_ERR_ORDER            /* operation done in wrong order */
#num PETSC_ERR_SIG              /* signal received */
#num PETSC_ERR_FP               /* floating point exception */
#num PETSC_ERR_COR              /* corrupted PETSc object */
#num PETSC_ERR_LIB              /* error in library called by PETSc */
#num PETSC_ERR_PLIB             /* PETSc library generated inconsistent data */
#num PETSC_ERR_MEMC             /* memory corruption */
#num PETSC_ERR_CONV_FAILED      /* iterative method (KSP or SNES) failed */
#num PETSC_ERR_USER             /* user has not provided needed function */
#num PETSC_ERR_SYS              /* error in system call */
#num PETSC_ERR_POINTER          /* pointer does not point to valid address */

#num PETSC_ERR_ARG_SIZ          /* nonconforming object sizes used in operation */
#num PETSC_ERR_ARG_IDN          /* two arguments not allowed to be the same */
#num PETSC_ERR_ARG_WRONG        /* wrong argument (but object probably ok) */
#num PETSC_ERR_ARG_CORRUPT      /* null or corrupted PETSc object as argument */
#num PETSC_ERR_ARG_OUTOFRANGE   /* input argument, out of range */
#num PETSC_ERR_ARG_BADPTR       /* invalid pointer argument */
#num PETSC_ERR_ARG_NOTSAMETYPE  /* two args must be same object type */
#num PETSC_ERR_ARG_NOTSAMECOMM  /* two args must be same communicators */
#num PETSC_ERR_ARG_WRONGSTATE   /* object in argument is in wrong state, e.g. unassembled mat */
#num PETSC_ERR_ARG_TYPENOTSET   /* the type of the object has not yet been set */
#num PETSC_ERR_ARG_INCOMP       /* two arguments are incompatible */
#num PETSC_ERR_ARG_NULL         /* argument is null that should not be */
#num PETSC_ERR_ARG_UNKNOWN_TYPE /* type name doesn't match any registered type */

#num PETSC_ERR_FILE_OPEN        /* unable to open file */
#num PETSC_ERR_FILE_READ        /* unable to read from file */
#num PETSC_ERR_FILE_WRITE       /* unable to write to file */
#num PETSC_ERR_FILE_UNEXPECTED  /* unexpected data in file */

#num PETSC_ERR_MAT_LU_ZRPVT     /* detected a zero pivot during LU factorization */
#num PETSC_ERR_MAT_CH_ZRPVT     /* detected a zero pivot during Cholesky factorization */

#num PETSC_ERR_INT_OVERFLOW   

#num PETSC_ERR_FLOP_COUNT     
#num PETSC_ERR_NOT_CONVERGED   /* solver did not converge */
#num PETSC_ERR_MISSING_FACTOR  /* MatGetFactor() failed */
#num PETSC_ERR_OPT_OVERWRITE   /* attempted to overwrite options which should not be changed */

#num PETSC_ERR_MAX_VALUE       /* always one greater than the largest error code */


-- ** PETSc integral types & constants.
------------------------------------------------------------------------------
-- | PETSc's general-purpose integral type.
#integral_t PetscInt

-- | When indices, number of dimensions, etc. can be inferred, for an integral
--   type, can use one of these predefined values.
#num PETSC_DECIDE
#num PETSC_DETERMINE
#num PETSC_DEFAULT


-- *** PETSc size patterns.
------------------------------------------------------------------------------
pattern PDecide :: PInt
pattern PDecide <- ((== c'PETSC_DECIDE) -> True) where
  PDecide = c'PETSC_DECIDE

pattern PDetermine :: PInt
pattern PDetermine <- ((== c'PETSC_DETERMINE) -> True) where
  PDetermine = c'PETSC_DETERMINE

-- | As of PETSc-3.7.6 , this is `-2`.
pattern PDefault :: PInt
pattern PDefault <- ((== c'PETSC_DEFAULT) -> True) where
  PDefault = c'PETSC_DEFAULT

pattern PIgnore :: Ptr a
pattern PIgnore <- ((== nullPtr) -> True) where
  PIgnore = nullPtr

{-- }
pattern DECIDE :: CInt
pattern DECIDE <- ((== c'PETSC_DECIDE) -> True) where
  DECIDE = c'PETSC_DECIDE

pattern DETERMINE :: CInt
pattern DETERMINE <- ((== c'PETSC_DETERMINE) -> True) where
  DETERMINE = c'PETSC_DETERMINE

pattern DEFAULT :: CInt
pattern DEFAULT <- ((== c'PETSC_DEFAULT) -> True) where
  DEFAULT = c'PETSC_DEFAULT
--}

------------------------------------------------------------------------------
#integral_t PetscBool

-- #integral_t PetscReal
#synonym_t  PetscReal , CDouble
#synonym_t  PetscScalar , <PetscReal>

------------------------------------------------------------------------------
#integral_t PetscCopyMode
#num PETSC_COPY_VALUES
#num PETSC_OWN_POINTER
#num PETSC_USE_POINTER

newtype CopyMode = CopyMode { unCopyMode :: C'PetscCopyMode }
  deriving (Eq, Ord, Enum, Bounded, Show, Storable, Generic)

------------------------------------------------------------------------------
pattern CopyValues :: CopyMode
pattern CopyValues <- ((== CopyMode c'PETSC_COPY_VALUES) -> True)
  where CopyValues  = CopyMode c'PETSC_COPY_VALUES

pattern OwnPointer :: CopyMode
pattern OwnPointer <- ((== CopyMode c'PETSC_OWN_POINTER) -> True)
  where OwnPointer  = CopyMode c'PETSC_OWN_POINTER

pattern UsePointer :: CopyMode
pattern UsePointer <- ((== CopyMode c'PETSC_USE_POINTER) -> True)
  where UsePointer  = CopyMode c'PETSC_USE_POINTER


-- * Petsc callback types.
------------------------------------------------------------------------------
#callback_t J , SNES -> Vec -> Mat -> Mat -> PCtx -> IO PErr
#callback_t F , SNES -> Vec        -> Vec -> PCtx -> IO PErr

#callback_t KSPOpsFun , KSP -> Mat -> Mat -> PCtx -> IO PErr
#callback_t KSPRHSFun , KSP               -> PCtx -> IO PErr


-- * PETSc file I/O types.
------------------------------------------------------------------------------
#integral_t PetscFileMode
#num FILE_MODE_READ
#num FILE_MODE_WRITE
#num FILE_MODE_APPEND
#num FILE_MODE_UPDATE
#num FILE_MODE_APPEND_UPDATE

newtype PFileMode = PFileMode { unPFileMode :: C'PetscFileMode }
                  deriving (Eq, Ord, Enum, Bounded, Show, Storable, Generic)

------------------------------------------------------------------------------
-- | Convenience aliases for the file-access modes.
pattern FileRead :: PFileMode
pattern FileRead <- ((== PFileMode c'FILE_MODE_READ) -> True) where
  FileRead = PFileMode c'FILE_MODE_READ

pattern FileWrite :: PFileMode
pattern FileWrite <- ((== PFileMode c'FILE_MODE_WRITE) -> True) where
  FileWrite = PFileMode c'FILE_MODE_WRITE

pattern FileAppend :: PFileMode
pattern FileAppend <- ((== PFileMode c'FILE_MODE_APPEND) -> True) where
  FileAppend = PFileMode c'FILE_MODE_APPEND

pattern FileUpdate :: PFileMode
pattern FileUpdate <- ((== PFileMode c'FILE_MODE_UPDATE) -> True) where
  FileUpdate = PFileMode c'FILE_MODE_UPDATE

pattern FileAppendUpdate :: PFileMode
pattern FileAppendUpdate <- ((== PFileMode c'FILE_MODE_APPEND_UPDATE) -> True) where
  FileAppendUpdate = PFileMode c'FILE_MODE_APPEND_UPDATE


-- * PETSc mode types.
------------------------------------------------------------------------------
-- | Vector mode types.
#globalarray VECSEQ      , CChar
#globalarray VECMPI      , CChar
#globalarray VECSTANDARD , CChar

-- | Update mode types.
#integral_t InsertMode
#num INSERT_VALUES
#num ADD_VALUES

------------------------------------------------------------------------------
-- | Nonlinear Solver mode types.
#globalarray SNESNEWTONLS , CChar
#globalarray SNESNCG      , CChar
#globalarray SNESNGMRES   , CChar


-- * High-level interface types.
------------------------------------------------------------------------------
-- | The base class that all other PETSc objects are derived from.
#opaque_t _p_PetscObject
newtype PetscObject = PetscObject { unPetscObject :: Ptr C'_p_PetscObject }
                    deriving (Eq, Ord, Show, Storable, Generic)

-- | For empty/optional objects.
pattern NoObject :: PetscObject
pattern NoObject <- ((== PetscObject nullPtr) -> True)
  where NoObject = PetscObject nullPtr

pattern NoObj    = NoObject


-- ** MPI data-types and patterns.
------------------------------------------------------------------------------
-- | MPI communicator data-type.
newtype Comm = Comm { unComm :: C'MPI_Comm }
             deriving (Eq, Show, Storable, Generic)

#integral_t MPI_Comm
#num MPI_COMM_NULL

#globalvar  PETSC_COMM_WORLD , <MPI_Comm>
#integral_t PetscMPIInt

------------------------------------------------------------------------------
-- | No communicator.
pattern NoComm :: Comm
pattern NoComm <- ((== Comm c'MPI_COMM_NULL) -> True)
  where NoComm = Comm c'MPI_COMM_NULL

{-- }
-- | Global communicator.
pattern PetscCommWorld :: Comm
pattern PetscCommWorld <- ((== Comm c'PETSC_COMM_WORLD) -> True)
  where PetscCommWorld = Comm c'PETSC_COMM_WORLD
--}


-- ** Viewer data-types & patterns.
------------------------------------------------------------------------------
-- | PETSc viewers.
#opaque_t _p_PetscViewer
newtype Viewer = Viewer { unViewer :: Ptr C'_p_PetscViewer }
                deriving (Eq, Show, Storable, Generic)

type PViewer = Ptr Viewer

------------------------------------------------------------------------------
-- | PETSc error codes.
newtype PetscErrorCode =
  PetscErrorCode { unPetscErrorCode :: C'PetscErrorCode }
  deriving (Eq, Ord, Enum, Num, Show, Storable, Generic)


-- ** Data-types for the nonlinear solver module.
------------------------------------------------------------------------------
-- | Nonlinear solver context.
#opaque_t _p_SNES
newtype SNES = SNES { unSNES :: Ptr C'_p_SNES }
             deriving (Eq, Storable, Generic)

newtype SNESType = SNESType { unSNESType :: CString }
                 deriving (Eq, Show, Generic)


-- ** Data-types for the Krylov subspace module.
------------------------------------------------------------------------------
-- | Abstract container for Krylov subspace settings.
newtype KSP  = KSP  { unKSP  :: Ptr () } deriving (Eq, Storable, Generic)

newtype KSPType = KSPType { unKSPType :: CString }
                deriving (Eq, Show, Generic)


-- ** Mesh/grid data-types.
------------------------------------------------------------------------------
newtype DM = DM { unDM :: Ptr () }
  deriving (Eq, Show, Storable, Generic)

-- | NULL/optional DM pattern.
pattern NoDM :: DM
pattern NoDM <- ((== DM nullPtr) -> True)
  where NoDM = DM nullPtr


-- ** Miscellaneous and shared/common data-types.
------------------------------------------------------------------------------
-- | Update modes, when writing values into PETSc container data-types.
newtype InsertMode = InsertMode C'InsertMode
                   deriving (Eq, Show, Storable, Generic)


-- ** Time-Stepping (TS) module types.
------------------------------------------------------------------------------
#opaque_t _p_TS

newtype TS = TS { unTS :: Ptr C'_p_TS } deriving (Eq, Storable, Generic)

------------------------------------------------------------------------------
-- | For empty/optional `TS` fields.
pattern NoTS :: TS
pattern NoTS <- ((== TS nullPtr) -> True)
  where NoTS = TS nullPtr


-- ** Container types.
------------------------------------------------------------------------------
-- | Wrappers around PETSc's vector and matrix data-types.
newtype Vec = Vec { unVec :: Ptr C'_p_Vec }
            deriving (Eq, Show, Storable, Generic)

newtype Mat = Mat { unMat :: Ptr C'_p_Mat }
            deriving (Eq, Show, Storable, Generic)

#opaque_t _p_Vec
#opaque_t _p_Mat

------------------------------------------------------------------------------
-- | Matrices can have null-space objects.
newtype MatNullSpace =
  MatNullSpace { unMatNullSpace :: Ptr C'_p_MatNullSpace }
  deriving (Eq, Show, Storable, Generic)

#opaque_t _p_MatNullSpace

------------------------------------------------------------------------------
newtype VecType = VecType { unVecType :: CString }
  deriving (Eq, Show, Storable, Generic)

------------------------------------------------------------------------------
-- | For empty/optional `Vec` fields.
pattern NoVec :: Vec
pattern NoVec <- ((== Vec nullPtr) -> True)
  where NoVec = Vec nullPtr

-- | For empty/optional `Mat` fields.
pattern NoMat :: Mat
pattern NoMat <- ((== Mat nullPtr) -> True)
  where NoMat = Mat nullPtr

------------------------------------------------------------------------------
-- | Value for empty/optional `MatNullSpace` fields.
pattern NoMatNullSpace :: MatNullSpace
pattern NoMatNullSpace <- ((== MatNullSpace nullPtr) -> True)
  where NoMatNullSpace  = MatNullSpace nullPtr


-- * IS (Index-Set) data-types.
------------------------------------------------------------------------------
#opaque_t _p_IS
newtype IS      = IS { unIS :: Ptr C'_p_IS }
                deriving (Eq, Ord, Show, Storable, Generic)

#opaque_t _p_PetscSection
newtype Section = Section { unSection :: Ptr C'_p_PetscSection }
                deriving (Eq, Ord, Show, Storable, Generic)

------------------------------------------------------------------------------
-- | Useful when an IS field is optional.
pattern NoIS :: IS
pattern NoIS <- ((== IS nullPtr) -> True) where
  NoIS = IS nullPtr

-- | Empty/optional `PetscSection's.
pattern NoSection :: Section
pattern NoSection <- ((== Section nullPtr) -> True) where
  NoSection = Section nullPtr


-- * Star Forests (for MPI distribution).
------------------------------------------------------------------------------
#opaque_t _p_PetscSF
newtype StarForest = StarForest { unStarForest :: Ptr C'_p_PetscSF }
                   deriving (Eq, Ord, Show, Storable, Generic)

type PPSF    = Ptr StarForest
type PetscSF = StarForest
type SF      = StarForest

pattern NoSF :: StarForest
pattern NoSF <- ((== StarForest nullPtr) -> True) where
  NoSF = StarForest nullPtr

noSF :: StarForest
noSF  = StarForest nullPtr


-- * Convenience patterns.
------------------------------------------------------------------------------
-- | Function/operation succeded.
pattern PetscSuccess :: PetscErrorCode
pattern PetscSuccess <- ((== PetscErrorCode 0) -> True) where
  PetscSuccess = PetscErrorCode 0


-- ** SNES solver `SNESType` patterns.
------------------------------------------------------------------------------
-- | Newton solver using Line-Search.
pattern SNESNewtonLS :: SNESType
pattern SNESNewtonLS <- ((== SNESType c'SNESNEWTONLS) -> True) where
  SNESNewtonLS = SNESType c'SNESNEWTONLS

-- | Nonlinear Conjugate Gradient solver.
pattern SNESNCG :: SNESType
pattern SNESNCG <- ((== SNESType c'SNESNCG) -> True) where
  SNESNCG = SNESType c'SNESNCG

-- | Nonlinear Generalised Minimum Residual solver.
pattern SNESNGMRES :: SNESType
pattern SNESNGMRES <- ((== SNESType c'SNESNGMRES) -> True) where
  SNESNGMRES = SNESType c'SNESNGMRES


-- ** Insert-mode patterns.
------------------------------------------------------------------------------
-- | Insert values, overwriting previous values.
pattern InsertValues :: InsertMode
pattern InsertValues <- ((== InsertMode c'INSERT_VALUES) -> True) where
  InsertValues = InsertMode c'INSERT_VALUES

-- | Add new values to the previous values.
pattern AddValues :: InsertMode
pattern AddValues <- ((== InsertMode c'ADD_VALUES) -> True) where
  AddValues = InsertMode c'ADD_VALUES
