{-# LANGUAGE CPP, ForeignFunctionInterface,
             OverloadedStrings, PatternSynonyms, ViewPatterns,
             GeneralizedNewtypeDeriving, DeriveGeneric
  #-}

#include <petsc.h>
#include <petscoptions.h>
#include <bindings.dsl.h>

module Bindings.Numeric.PETSc.Internal.Options where

#strict_import
import Foreign.Ptr
import Foreign.Storable
import System.IO.Unsafe
import GHC.Generics (Generic)
import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Core


-- * Options data-type stuff.
------------------------------------------------------------------------------
#globalvar PetscOptionsPublish , PBool

#opaque_t _n_PetscOptionItem
#opaque_t _p_PetscObject
#opaque_t _n_PetscOptions

#starttype PetscOptionItems
#field count   , PInt
#field next    , Ptr <_n_PetscOptionItem>
#field prefix  , CString
#field pprefix , CString
#field title   , CString
#field comm    , Comm
#field printhelp      , PBool
#field changedmethod  , PBool
#field alreadyprinted , PBool
#field object  , PetscObject
#field options , Ptr <_n_PetscOptions>
#stoptype

------------------------------------------------------------------------------
-- | Option-builder data-type.
-- #opaque_t _n_PetscOptionItems
newtype POptItems = POptItems { unPOptItems :: Ptr C'PetscOptionItems }
  deriving (Eq, Storable, Generic)

------------------------------------------------------------------------------
-- | Options database.
newtype POptions = POptions { unPOptions :: Ptr C'_n_PetscOptions }
  deriving (Eq, Ord, Show, Storable, Generic)


-- * Standard database queries.
------------------------------------------------------------------------------
#ccall PetscOptionsGetInt , POptions -> PStr -> PStr -> PPInt -> PPBool -> PRes


-- * Query and (un)register additional PETSc options.
--    TODO:
------------------------------------------------------------------------------


-- ** Begin a query-session.
------------------------------------------------------------------------------
#ccall PetscOptionsBegin_Private , POptItems -> Comm -> PStr -> PStr -> PStr -> IO PErr
#ccall PetscOptionsEnd_Private   , POptItems -> IO PErr


-- ** Query options by type.
------------------------------------------------------------------------------
#ccall PetscOptionsReal_Private  , POptItems -> PStr -> PStr -> PStr -> PReal -> PPReal -> PPBool -> IO PErr
#ccall PetscOptionsBool_Private  , POptItems -> PStr -> PStr -> PStr -> PBool -> PPBool -> PPBool -> IO PErr
#ccall PetscOptionsString_Private , POptItems -> PStr -> PStr -> PStr -> PStr -> PStr -> SizeT -> PPBool -> PRes
#ccall PetscOptionsInt_Private , POptItems -> PStr -> PStr -> PStr -> PInt -> PPInt -> PPBool -> PRes

-- *** Lists & arrays of options.
------------------------------------------------------------------------------
#ccall PetscOptionsEList_Private , POptItems -> PStr -> PStr -> PStr -> PPStr -> PInt -> PStr -> PPInt -> PPBool -> PRes

#ccall PetscOptionsIntArray_Private , POptItems -> PStr -> PStr -> PStr -> PPInt -> PPInt -> PPBool -> PRes
