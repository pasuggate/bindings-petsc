{-# LANGUAGE OverloadedStrings, CPP, TemplateHaskell, DeriveGeneric,
             PatternSynonyms, ViewPatterns #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal.IS
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Bindings to the Index-Set (IS) module.
-- 
-- NOTE:
-- 
-- Changelog:
--  + 05/08/2017  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

#include <petscis.h>
#include <bindings.dsl.h>
module Bindings.Numeric.PETSc.Internal.IS where

#strict_import
import Foreign.Ptr
import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Core


-- * IS functions.
------------------------------------------------------------------------------
#ccall ISCreate , Comm -> Ptr IS -> PRes
-- #ccall ISSetUp , IS -> PRes
-- #ccall ISSetFromOptions , IS -> PRes
#ccall ISDestroy , Ptr IS -> PRes

#ccall ISView , IS -> Viewer -> PRes


-- * Section functions.
--   NOTE: These aren't collective, with a few exceptions.
------------------------------------------------------------------------------
#ccall PetscSectionCreate , Comm -> Ptr Section -> PRes
#ccall PetscSectionSetUp , Section -> PRes
-- #ccall PetscSectionSetFromOptions , Section -> PRes
#ccall PetscSectionDestroy , Ptr Section -> PRes

------------------------------------------------------------------------------
-- | Shallow (if possible) copies & clones of sections.
--   NOTE: These are collective.
#ccall PetscSectionCopy  , Section ->     Section -> PRes
#ccall PetscSectionClone , Section -> Ptr Section -> PRes


-- ** Section field setter/getters.
------------------------------------------------------------------------------
-- | Sets/gets the name of a `Section's field.
#ccall PetscSectionSetFieldName , Section -> PInt ->  PStr -> PRes
#ccall PetscSectionGetFieldName , Section -> PInt -> PPStr -> PRes

-- | Sets/gets the number of field's for the given point.
#ccall PetscSectionSetNumFields , Section -> PInt ->  PInt -> PRes
#ccall PetscSectionGetNumFields , Section -> PInt -> PPInt -> PRes

-- | Sets/gets the number of components, for the given field.
#ccall PetscSectionSetFieldComponents , Section -> PInt ->  PInt -> PRes
#ccall PetscSectionGetFieldComponents , Section -> PInt -> PPInt -> PRes

-- | Sets/gets the number of dofs, for the given point & field.
#ccall PetscSectionSetFieldDof , Section -> PInt -> PInt ->  PInt -> PRes
#ccall PetscSectionGetFieldDof , Section -> PInt -> PInt -> PPInt -> PRes

-- | Sets/gets the dofs for the given index.
#ccall PetscSectionSetDof , Section -> PInt ->     PInt -> PRes
#ccall PetscSectionGetDof , Section -> PInt -> Ptr PInt -> PRes

-- | Sets/gets the domain, `[start, end)`, of the `Section`.
#ccall PetscSectionSetChart , Section ->  PInt ->  PInt -> PRes
#ccall PetscSectionGetChart , Section -> PPInt -> PPInt -> PRes

{-- }
-- | Sets/adds/gets the number of DoF's for the given point.
#ccall PetscSectionSetNumDof , Section -> PInt ->  PInt -> PRes
#ccall PetscSectionAddNumDof , Section -> PInt ->  PInt -> PRes
#ccall PetscSectionGetNumDof , Section -> PInt -> PPInt -> PRes
--}

------------------------------------------------------------------------------
-- | Sets/gets the offsets of the dofs/fields for the given point.
--   NOTE: Only valid post-setup? (Typically computed during setup?)
#ccall PetscSectionSetOffset , Section -> PInt ->  PInt -> PRes
#ccall PetscSectionGetOffset , Section -> PInt -> PPInt -> PRes
#ccall PetscSectionSetFieldOffset , Section -> PInt -> PInt ->  PInt -> PRes
#ccall PetscSectionGetFieldOffset , Section -> PInt -> PInt -> PPInt -> PRes

-- | Sets/gets the permutation for `[0, pEnd-pStart)`.
#ccall PetscSectionSetPermutation , Section ->     IS -> PRes
#ccall PetscSectionGetPermutation , Section -> Ptr IS -> PRes


-- ** Section viewer API.
------------------------------------------------------------------------------
#ccall PetscSectionView , Section -> Viewer -> PRes
