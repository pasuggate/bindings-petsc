{-# LANGUAGE CPP, ForeignFunctionInterface, TupleSections, DeriveGeneric,
             GeneralizedNewtypeDeriving, OverloadedStrings, PatternSynonyms,
             ViewPatterns
  #-}

#include <stdio.h>
#include <petsc.h>
#include <petsclog.h>
#include <bindings.dsl.h>
module Bindings.Numeric.PETSc.Internal.Core where

#strict_import
import GHC.Generics (Generic)
import System.IO.Unsafe
import Control.Monad.IO.Class
import Bindings.Numeric.PETSc.Internal.Types

import Foreign.Ptr
import Foreign.Marshal
-- import Foreign.C.Types
import Foreign.Storable


-- * PETSc core.
------------------------------------------------------------------------------
-- | Defined in `Core.c`, which wraps the macro "CHKERRQ(n)".
#ccall   ChkErr , PErr -> IO PErr

#cinline PetscFunctionBeginUser , IO ()
-- #cinline PetscFunctionReturn    , PetscErrorCode -> IO PetscErrorCode

------------------------------------------------------------------------------
-- | Register a citation, along with a flag that tracks whether the citation
--   has been displayed.
#cinline PetscCitationsRegister , PStr -> PPBool -> PRes


-- ** PETSc initialisation and finalisation.
------------------------------------------------------------------------------
#ccall PetscInitialize , PPInt -> Ptr Argv -> PStr -> PStr -> IO PErr
#ccall PetscFinalize   ,                                      IO PErr

------------------------------------------------------------------------------
-- | Register additional finalisers. The finaliser function is of type:
--     PetscErrorCode <finaliser> (void) { ... }
--   NOTE: Requires wrapping using `mk'PFinalFun f`.
#callback_t PFinalFun , PRes
#ccall PetscRegisterFinalize , <PFinalFun> -> PRes


-- ** PETSc functions common to all `PetscObject's.
------------------------------------------------------------------------------
#ccall PetscObjectDestroy , Ptr PetscObject -> PRes

#ccall PetscObjectSetName , PetscObject ->     PStr -> PRes
#ccall PetscObjectGetName , PetscObject -> Ptr PStr -> PRes

#ccall PetscObjectViewFromOptions , PetscObject -> PetscObject -> PStr -> PRes
#ccall PetscObjectComm , PetscObject -> IO Comm
#ccall PetscObjectGetComm , PetscObject -> Ptr Comm -> PRes

#ccall PetscObjectCompose , PetscObject -> PStr -> PetscObject -> PRes


-- ** PETSc logging, and profiling functions.
------------------------------------------------------------------------------
#integral_t PetscClassId
#num DM_CLASSID

newtype ClassId = ClassId { unClassId :: C'PetscClassId }
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Storable, Generic)

#integral_t PetscLogEvent

newtype LogEvent = LogEvent { unLogEvent :: C'PetscLogEvent }
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Storable, Generic)

------------------------------------------------------------------------------
#ccall PetscLogEventRegister , PStr -> ClassId -> Ptr LogEvent -> PRes

#cinline PetscLogEventBegin , LogEvent -> PObj -> PObj -> PObj -> PObj -> PRes
#cinline PetscLogEventEnd   , LogEvent -> PObj -> PObj -> PObj -> PObj -> PRes

------------------------------------------------------------------------------
{-- }
-- TODO: `PetscLogDouble`??
#ccall PetscLogFlops , CDouble -> IO PErr

petscLogFlops :: (MonadIO m, Integral i) => i -> m ()
petscLogFlops n = liftIO $ do
  c'PetscLogFlops (fromIntegral n) >>= chkErr
--}


-- ** Low-level functionality.
------------------------------------------------------------------------------
#cinline PetscMemzero , Ptr () -> <size_t> -> PRes
#cinline PetscFree    , Ptr ()             -> PRes


-- * Function aliases.
------------------------------------------------------------------------------
chkErr :: PErr -> IO ()
{-# INLINE chkErr #-}
chkErr ierr = c'ChkErr ierr >> return ()


-- * MPI <-> PETSc functionality.
------------------------------------------------------------------------------
-- TODO: Support MPI error-codes.
#ccall MPI_Comm_rank , Comm -> Ptr MInt -> PRes

mpiCommRank :: MonadIO m => Comm -> m Int
mpiCommRank comm = liftIO $ with 0 $ \pm -> do
  c'MPI_Comm_rank comm pm >>= chkErr >> fromIntegral `fmap` peek pm


-- * Miscellaneous functions.
------------------------------------------------------------------------------
#ccall PetscSleep , PReal -> PRes

petscSleep :: MonadIO m => Double -> m ()
petscSleep dur = liftIO $ c'PetscSleep (realToFrac dur) >>= chkErr


-- ** Conversions between PETSc & Haskell booleans.
------------------------------------------------------------------------------
pbool :: Bool -> C'PetscBool
pbool False = 0
pbool True  = 1
{-# INLINE pbool  #-}

pfalse, ptrue :: PBool
pfalse = pbool False
ptrue  = pbool True
{-# INLINE pfalse #-}
{-# INLINE ptrue  #-}

hbool :: C'PetscBool -> Bool
hbool 0 = False
hbool 1 = True
{-# INLINE hbool  #-}


-- ** Error-handling.
------------------------------------------------------------------------------
nosuch :: FilePath -> Int -> String -> String
nosuch fp ln fn = fp ++ ':':show ln ++ ':':fn ++ "\tno such function"
