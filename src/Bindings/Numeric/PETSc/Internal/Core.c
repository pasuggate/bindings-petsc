#include <bindings.cmacros.h>
#include <petscsys.h>
#include <petscerror.h>
#include <petscsnes.h>
#include <petscts.h>
#include <petscds.h>


/* ------------------------------------------------------------------------ */
/*  Pointers to PETSc's global arrays.                                      */
/* ------------------------------------------------------------------------ */
/* -- Vector (Vec) types --                                                 */
BC_GLOBALARRAY(VECSEQ,char)
BC_GLOBALARRAY(VECMPI,char)
BC_GLOBALARRAY(VECSTANDARD,char)

// -- Matrix (Mat) types --
BC_GLOBALARRAY(MATAIJ,char)
BC_GLOBALARRAY(MATMPIAIJ,char)
BC_GLOBALARRAY(MATDENSE,char)

// -- Nonlinear solver (SNES) types --
BC_GLOBALARRAY(SNESNEWTONLS,char)
BC_GLOBALARRAY(SNESNCG,char)
BC_GLOBALARRAY(SNESNGMRES,char)

// -- Time-Stepping (TS) solver types --
BC_GLOBALARRAY(TSBEULER,char)
BC_GLOBALARRAY(TSTHETA,char)
BC_GLOBALARRAY(TSCN,char)

// -- PETSc viewer (PetscViewer) types --
BC_GLOBALARRAY(PETSCVIEWERVTK,char)
BC_GLOBALARRAY(PETSCVIEWERASCII,char)

// -- PETSc discretisation (PetscDS) types --
BC_GLOBALARRAY(PETSCDSBASIC,char)


/* ------------------------------------------------------------------------ */
/*  PETSc's inline system-functions.                                        */
/* ------------------------------------------------------------------------ */
/* -- Standard inlines --                                                   */
BC_INLINE_VOID(PetscFunctionBeginUser)
// BC_INLINE1(PetscFunctionReturn, PetscErrorCode, PetscErrorCode)

BC_INLINE2(PetscMemzero, void*, size_t, PetscErrorCode)
BC_INLINE1(PetscFree   , void*        , PetscErrorCode)

// BC_INLINE1(PETSC_VIEWER_DRAW_WORLD, MPI_Comm, PetscViewer)


/* ------------------------------------------------------------------------ */
/*  Inlines for additional system tasks.                                    */
BC_INLINE2(PetscCitationsRegister, const char*, PetscBool*, PetscErrorCode)

BC_INLINE5(PetscLogEventBegin, PetscLogEvent, PetscObject, PetscObject, PetscObject, PetscObject, PetscErrorCode)
BC_INLINE5(PetscLogEventEnd  , PetscLogEvent, PetscObject, PetscObject, PetscObject, PetscObject, PetscErrorCode)


/* ------------------------------------------------------------------------ */
/*  Macro-wrappers for system tasks.                                        */
PetscErrorCode ChkErr(PetscErrorCode ierr)
{
  CHKERRQ(ierr);
  return ierr;
}


PetscViewer PetscViewerStdOutWorld(void)
{
  PetscViewer pv = PETSC_VIEWER_STDOUT_WORLD;
  return pv;
}
