{-# LANGUAGE CPP, ForeignFunctionInterface, DeriveGeneric,
             GeneralizedNewtypeDeriving
  #-}

#include <petscsnes.h>
#include <petscmat.h>
#include <petscdm.h>
#include <bindings.dsl.h>
module Bindings.Numeric.PETSc.Internal.Vec where

#strict_import
import GHC.Generics (Generic)
import Bindings.Numeric.PETSc.Internal.Types


-- * Additional data-types.
------------------------------------------------------------------------------
#integral_t NormType
#num NORM_1
#num NORM_2
#num NORM_FROBENIUS
#num NORM_INFINITY
#num NORM_1_AND_2

newtype NormType = NormType { unNormType :: C'NormType }
  deriving (Eq, Ord, Enum, Bounded, Read, Show, Generic, Storable)


-- ^ Vector constructor & destructor:
#ccall VecCreate         , Comm -> Ptr Vec             -> PRes
#ccall VecDestroy        ,         Ptr Vec             -> PRes

-- ^ Set vector sizes & types:
#ccall VecSetFromOptions , Vec                         -> PRes
#ccall VecSetUp          , Vec                         -> PRes
#ccall VecSetType        , Vec  -> VecType             -> PRes
#ccall VecSetSizes       , Vec  -> PInt     -> PInt    -> PRes
#ccall VecGetSize        , Vec  -> Ptr PInt            -> PRes
#ccall VecGetLocalSize   , Vec  -> PPInt               -> PRes


-- ^ Begin/end assembly:
#ccall VecAssemblyBegin  , Vec  -> PRes
#ccall VecAssemblyEnd    , Vec  -> PRes

-- ^ Low-level access:
#ccall VecGetArray       , Vec -> Ptr (Ptr PReal) -> PRes
#ccall VecRestoreArray   , Vec -> Ptr (Ptr PReal) -> PRes

-- ^ Vector value operations:
#ccall VecSet         , Vec                  -> PReal       -> PRes
#ccall VecSetValues   , Vec  -> PInt -> Ptr PInt -> Ptr PReal -> InsertMode -> PRes
#ccall VecGetValues   , Vec  -> PInt -> Ptr PInt -> Ptr PReal -> PRes
#ccall VecZeroEntries , Vec  -> PRes
#ccall VecCopy        , Vec                  -> Vec          -> PRes
#ccall VecDuplicate   , Vec                  -> Ptr Vec      -> PRes

#ccall VecChop , Vec ->              PReal -> PRes
#ccall VecNorm , Vec -> NormType -> PPReal -> PRes

-- ^ Vector operations:
#ccall VecAXPY , Vec -> PReal -> Vec -> PRes

-- ^ Vector viewers:
#ccall VecView , Vec  -> Viewer -> PRes
