{-# LANGUAGE CPP, ForeignFunctionInterface, TupleSections,
             DeriveGeneric, GeneralizedNewtypeDeriving,
             OverloadedStrings, PatternSynonyms, ViewPatterns
  #-}
  
------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Internal.View
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Bindings for PETSc's Viewer module.
-- 
-- NOTE:
-- 
-- Changelog:
--  + 11/07/2017  -- initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

#include <petsc.h>
#include <petscviewertypes.h>
#include <petscviewerhdf5.h>
#include <bindings.dsl.h>
module Bindings.Numeric.PETSc.Internal.View where

#strict_import
import GHC.Generics (Generic)
import System.IO.Unsafe
import Control.Monad.IO.Class
import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Core
import Bindings.Numeric.PETSc.Internal

import Foreign.Ptr
import Foreign.Marshal
import Foreign.C.String
import Foreign.Storable


-- * PETSc viewer types.
------------------------------------------------------------------------------
newtype ViewerT = ViewerT { unViewerT :: CString }
                 deriving (Eq, Show, Storable, Generic)

#globalarray PETSCVIEWERASCII , CChar
#globalarray PETSCVIEWERVTK   , CChar

pattern ViewerVTK :: ViewerT
pattern ViewerVTK <- ((== ViewerT c'PETSCVIEWERVTK) -> True) where
  ViewerVTK = ViewerT c'PETSCVIEWERVTK

pattern ViewerASCII :: ViewerT
pattern ViewerASCII <- ((== ViewerT c'PETSCVIEWERASCII) -> True) where
  ViewerASCII = ViewerT c'PETSCVIEWERASCII

------------------------------------------------------------------------------
-- | Pattern for empty/optional viewers.
pattern NoViewer :: Viewer
pattern NoViewer <- ((== Viewer nullPtr) -> True)
  where NoViewer = Viewer nullPtr


-- * PETSc viewer formats.
------------------------------------------------------------------------------
newtype ViewerF = ViewerF { unViewerF :: C'PetscViewerFormat }
                 deriving (Eq, Show, Storable, Generic)

-- TODO:
--   http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Viewer/PetscViewerFormat.html
#integral_t PetscViewerFormat
#num PETSC_VIEWER_DEFAULT
#num PETSC_VIEWER_ASCII_VTK
#num PETSC_VIEWER_NOFORMAT

pattern PETSC_VIEWER_DEFAULT :: ViewerF
pattern PETSC_VIEWER_DEFAULT <- ((== ViewerF c'PETSC_VIEWER_DEFAULT) -> True)
  where PETSC_VIEWER_DEFAULT = ViewerF c'PETSC_VIEWER_DEFAULT

pattern PETSC_VIEWER_ASCII_VTK :: ViewerF
pattern PETSC_VIEWER_ASCII_VTK <- ((== ViewerF c'PETSC_VIEWER_ASCII_VTK) -> True)
  where PETSC_VIEWER_ASCII_VTK = ViewerF c'PETSC_VIEWER_ASCII_VTK


-- * Additional viewer data-types.
------------------------------------------------------------------------------
-- | Represents the name of an X display.
newtype Display = Display { unDisplay :: CString }
                deriving (Eq, Show, Storable, Generic)

pattern NoDisplay :: Display
pattern NoDisplay <- ((== Display nullPtr) -> True)
  where NoDisplay  = Display nullPtr


-- * Convenience aliases.
------------------------------------------------------------------------------
type PViewerT = Ptr ViewerT
type PViewerF = Ptr ViewerF


-- * PETSc viewers.
------------------------------------------------------------------------------
#ccall PetscViewerCreate  , Comm -> Ptr Viewer -> PRes
#ccall PetscViewerDestroy ,         Ptr Viewer -> PRes

petscViewerCreate :: MonadIO m => Comm -> m Viewer
petscViewerCreate comm = liftIO $ with NoViewer $ \pv -> do
  c'PetscViewerCreate comm pv >>= chkErr >> peek pv

petscViewerDestroy :: MonadIO m => Viewer -> m ()
petscViewerDestroy view = liftIO $ with view $ \pv -> do
  c'PetscViewerDestroy pv >>= chkErr


-- ** Additional setup functionality.
------------------------------------------------------------------------------
#ccall PetscViewerSetFromOptions , Viewer -> PRes
#ccall PetscViewerSetUp          , Viewer -> PRes

petscViewerSetFromOptions :: MonadIO m => Viewer -> m ()
petscViewerSetFromOptions obj = liftIO $ do
  c'PetscViewerSetFromOptions obj >>= chkErr

petscViewerSetUp :: MonadIO m => Viewer -> m ()
petscViewerSetUp obj = liftIO $ c'PetscViewerSetUp obj >>= chkErr


-- * Predefined viewers.
------------------------------------------------------------------------------
pattern NoViewerT :: ViewerT
pattern NoViewerT <- ((== ViewerT nullPtr) -> True)
  where NoViewerT  = ViewerT nullPtr

pattern NoViewerF :: ViewerF
pattern NoViewerF <- ((== ViewerF c'PETSC_VIEWER_NOFORMAT) -> True)
  where NoViewerF  = ViewerF c'PETSC_VIEWER_NOFORMAT

pattern PetscViewerStdOutWorld :: Viewer
pattern PetscViewerStdOutWorld <- ((== unsafeStdOutWorld) -> True)
  where PetscViewerStdOutWorld  = unsafeStdOutWorld

------------------------------------------------------------------------------
#ccall   PetscViewerStdOutWorld  ,         IO Viewer
#ccall   PETSC_VIEWER_DRAW_      , Comm -> IO Viewer
-- #cinline PETSC_VIEWER_DRAW_WORLD ,         IO Viewer

petscViewerStdOutWorld :: MonadIO m => m Viewer
petscViewerStdOutWorld  = liftIO $ c'PetscViewerStdOutWorld

unsafeStdOutWorld :: Viewer
unsafeStdOutWorld  = unsafePerformIO petscViewerStdOutWorld


-- * PETSc viewer getters & setters.
------------------------------------------------------------------------------
#ccall PetscViewerSetType    , Viewer ->     ViewerT -> PRes
#ccall PetscViewerGetType    , Viewer -> Ptr ViewerT -> PRes
#ccall PetscViewerPushFormat , Viewer ->     ViewerF -> PRes
#ccall PetscViewerPopFormat  , Viewer                 -> PRes

------------------------------------------------------------------------------
petscViewerSetType :: MonadIO m => Viewer -> ViewerT -> m ()
petscViewerSetType view vtyp = liftIO $ do
  c'PetscViewerSetType view vtyp >>= chkErr

petscViewerGetType :: MonadIO m => Viewer -> m ViewerT
petscViewerGetType view = liftIO $ with NoViewerT $ \ptyp -> do
  c'PetscViewerGetType view ptyp >>= chkErr >> peek ptyp

------------------------------------------------------------------------------
petscViewerPushFormat :: MonadIO m => Viewer -> ViewerF -> m ()
petscViewerPushFormat view vfmt = liftIO $ do
  c'PetscViewerPushFormat view vfmt >>= chkErr

petscViewerPopFormat :: MonadIO m => Viewer -> m ()
petscViewerPopFormat view = liftIO $ do
  c'PetscViewerPopFormat view >>= chkErr


-- * PETSc viewers.
-- ** PETSc viewer file I/O.
------------------------------------------------------------------------------
#ccall PetscViewerVTKOpen , Comm -> CString -> PFileMode -> PViewer -> PRes
#ccall PetscViewerHDF5Open , Comm -> PStr -> PFileMode -> PViewer -> PRes
#ccall PetscViewerFileSetName , Viewer -> CString -> PRes

petscViewerVTKOpen :: MonadIO m => Comm -> FilePath -> PFileMode -> m Viewer
petscViewerVTKOpen comm fp fm = liftIO $ with (Viewer nullPtr) $ \pv -> do
  withCString fp $ \cstr -> c'PetscViewerVTKOpen comm cstr fm pv >>= chkErr
  peek pv

petscViewerHDF5Open :: MonadIO m => Comm -> FilePath -> PFileMode -> m Viewer
petscViewerHDF5Open comm fp fm = liftIO $ with NoViewer $ \pv -> do
  withCString fp $ \str -> c'PetscViewerHDF5Open comm str fm pv >>= chkErr
  peek pv

------------------------------------------------------------------------------
petscViewerFileSetName :: MonadIO m => Viewer -> String -> m ()
petscViewerFileSetName view name = liftIO $ withCString name $ \fp -> do
  c'PetscViewerFileSetName view fp >>= chkErr


-- ** Additional PETSc viewers.
------------------------------------------------------------------------------
#ccall PetscViewerDrawOpen , Comm -> Display -> CString -> PInt -> PInt -> PInt -> PInt -> Ptr Viewer -> PRes

-- TODO: Handle more window options, see:
--   http://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Viewer/PetscViewerDrawOpen.html
petscViewerDrawOpen :: MonadIO m =>
  Comm -> Display -> String -> Maybe (Int, Int) -> Maybe (Int, Int) -> m Viewer
petscViewerDrawOpen comm disp lab pos siz = liftIO $ do
  let (x, y) = case pos of
        Nothing     -> (PDecide, PDecide)
        Just (a, b) -> (fromIntegral a, fromIntegral b)
      (w, h) = case siz of
        Nothing     -> (PDecide, PDecide)
        Just (a, b) -> (fromIntegral a, fromIntegral b)
  withPString lab $ \str -> with NoViewer $ \ptr -> do
    c'PetscViewerDrawOpen comm disp str x y w h ptr >>= chkErr >> peek ptr


-- ** Configure viewers from command-line options.
------------------------------------------------------------------------------
#ccall PetscOptionsGetViewer , Comm -> PStr -> PStr -> PViewer -> PViewerF -> PPBool -> PRes
