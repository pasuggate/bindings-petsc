{-# LANGUAGE CPP, ForeignFunctionInterface #-}

#include <petscsnes.h>
#include <petscmat.h>
#include <petscdm.h>
#include <bindings.dsl.h>

module Bindings.Numeric.PETSc.Internal.SNES where

#strict_import
import Bindings.Numeric.PETSc.Internal.Types


-- ^ SNES context operations:
#ccall SNESCreate      , Comm -> Ptr SNES -> IO PErr
#ccall SNESDestroy     ,         Ptr SNES -> IO PErr

#ccall SNESSetType     , SNES -> SNESType                      -> IO PErr
#ccall SNESSolve       , SNES -> Vec -> Vec                    -> IO PErr
#ccall SNESSetFunction , SNES -> Vec ->               <F> -> Ptr () -> IO PErr
#ccall SNESSetJacobian , SNES -> Mat -> Mat -> <J> -> Ptr () -> IO PErr

-- ^^ Additional SNES settings:
#ccall SNESSetFromOptions     , SNES -> IO PErr
#ccall SNESGetSolution        , SNES -> Ptr Vec -> IO PErr
#ccall SNESSetIterationNumber , SNES -> PInt -> IO PErr
#ccall SNESGetTolerances      , SNES -> Ptr PReal -> Ptr PReal -> Ptr PReal -> Ptr PInt -> Ptr PInt -> IO PErr
#ccall SNESSetTolerances      , SNES ->     PReal ->     PReal ->     PReal ->     PInt ->     PInt -> IO PErr
