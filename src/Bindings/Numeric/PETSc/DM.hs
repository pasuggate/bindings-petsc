{-# LANGUAGE CPP, TypeOperators, PatternSynonyms, ViewPatterns, TupleSections,
             GeneralizedNewtypeDeriving, StandaloneDeriving, DeriveGeneric,
             DeriveDataTypeable, MultiParamTypeClasses, FlexibleInstances
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.DM
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Mid-level bindings to the grid/mesh (DM) modules of PETSc.
-- 
-- Changelog:
--  + 11/06/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.DM
       ( DM(..)
       , DMType(..)
       , DMLabel(..)
       , DMDALocalInfo(..)
       , DMCreateFun
         -- ^ boundary-condition types:
       , BdyT(..)
       , DMBoundaryType
       , pattern DM_BOUNDARY_NONE, pattern BdyNone
       , pattern DM_BOUNDARY_GHOSTED, pattern BdyGhost
       , pattern DM_BOUNDARY_PERIODIC, pattern BdyWrap
         -- ^ stencil types:
       , StencilT(..)
       , DMDAStencilType
       , pattern DMDA_STENCIL_BOX
       , pattern DMDA_STENCIL_STAR
         -- ^ sizing types:
       , pattern PDecide
       , pattern PDetermine
       , module Bindings.Numeric.PETSc.DM
       ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal
import Foreign.C.String
import GHC.Generics (Generic)
import Control.Monad.IO.Class
import Control.Monad.Primitive
import Data.Typeable (Typeable)
import Data.StateVar
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vector
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Storable.Mutable as Mut

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Vec
import Bindings.Numeric.PETSc.Internal.Mat
import Bindings.Numeric.PETSc.Internal.DM
import Bindings.Numeric.PETSc.Internal

import Control.Monad.Reader


-- * DM-type patterns.
------------------------------------------------------------------------------
-- | Pattern for empty/optional `DMType` fields.
pattern NoDMType :: DMType
pattern NoDMType <- ((== DMType nullPtr) -> True)
  where NoDMType = DMType nullPtr

pattern NoDMLabel :: DMLabel
pattern NoDMLabel <- ((== DMLabel nullPtr) -> True)
  where NoDMLabel = DMLabel nullPtr

------------------------------------------------------------------------------
-- | Distributed Array (DA) DM-type.
pattern DMDA :: DMType
pattern DMDA <- ((== DMType c'DMDA) -> True)
  where DMDA = DMType c'DMDA

pattern DMPLEX :: DMType
pattern DMPLEX <- ((== DMType c'DMPLEX) -> True)
  where DMPLEX = DMType c'DMPLEX

pattern DMFOREST :: DMType
pattern DMFOREST <- ((== DMType c'DMFOREST) -> True)
  where DMFOREST = DMType c'DMFOREST

pattern DMMOAB :: DMType
pattern DMMOAB <- ((== DMType c'DMMOAB) -> True)
  where DMMOAB = DMType c'DMMOAB

#ifdef __WITH_P4EST
pattern DMP4EST :: DMType
pattern DMP4EST <- ((== DMType c'DMP4EST) -> True)
  where DMP4EST = DMType c'DMP4EST
#endif

pattern DMP8EST :: DMType
pattern DMP8EST <- ((== DMType c'DMP8EST) -> True)
  where DMP8EST = DMType c'DMP8EST

------------------------------------------------------------------------------
type BCFun = Maybe (PPReal -> PPScalar -> PCtx -> IO ())

pattern NoBCFun :: BCFun
pattern NoBCFun  = Nothing

------------------------------------------------------------------------------
-- | Value for empty/optional `PetscFE` field.
pattern NoPFE :: PFE
pattern NoPFE <- ((== PFE nullPtr) -> True)
  where NoPFE = PFE nullPtr


-- * Common DM function instances.
------------------------------------------------------------------------------
-- | Common `PetscObject` functionality for `DM` objects.
instance IsPetscObject DM where
  create         = dmCreate
  setFromOptions = dmSetFromOptions
  setUp          = dmSetUp
  destroy        = dmDestroy
  {-# INLINE create #-}
  {-# INLINE setFromOptions #-}
  {-# INLINE setUp #-}
  {-# INLINE destroy #-}


-- * DM constructors and destructors.
------------------------------------------------------------------------------
dmCreate :: MonadIO m => Comm -> m DM
dmCreate comm = liftIO $ with NoDM $ \pdm -> do
  c'DMCreate comm pdm >>= chkErr >> peek pdm

-- | Destroy a DM context.
dmDestroy :: MonadIO m => DM -> m ()
dmDestroy dm = liftIO $ with dm $ \p -> c'DMDestroy p >>= chkErr

------------------------------------------------------------------------------
-- | Create a new DM DA context.
--   OBSOLETE:
dmdaCreate2d :: Comm -> BdyT -> BdyT -> StencilT -> Int -> Int -> Int -> IO DM
dmdaCreate2d c bx by stencil dof m n =
  with (DM nullPtr) $ \s -> do
  let p0 = nullPtr :: Ptr PInt
      m' = fromIntegral m
      n' = fromIntegral n
      d' = fromIntegral dof
  c'DMDACreate2d c bx by stencil m' n' PDecide PDecide d' 1 p0 p0 s >>= chkErr >> peek s

-- | Create a structured grid as a Distributed Array (DA).
dmDACreate1d :: BdyT -> Int -> Int -> IO DM
dmDACreate1d b m dof = with (DM nullPtr) $ \da -> do
  let p0 = nullPtr :: Ptr PInt
      m' = fromIntegral m
      d' = fromIntegral dof
  cm <- petscCommWorld
  c'DMDACreate1d cm b m' PDecide d' p0 da >>= chkErr
  peek da

-- | Create a structured grid as a Distributed Array (DA).
dmDACreate2d :: BdyT -> BdyT -> StencilT -> Int -> Int -> Int -> Int -> IO DM
dmDACreate2d bx by st m n dof w = with (DM nullPtr) $ \da -> do
  let p0 = nullPtr :: Ptr PInt
      m' = fromIntegral m
      n' = fromIntegral n
      d' = fromIntegral dof
      w' = fromIntegral w
  cm <- petscCommWorld
  c'DMDACreate2d cm bx by st m' n' PDecide PDecide d' w' p0 p0 da >>= chkErr
  peek da


-- ** Additional setup functions.
------------------------------------------------------------------------------
dmSetFromOptions :: MonadIO m => DM -> m ()
dmSetFromOptions sec = liftIO $ c'DMSetFromOptions sec >>= chkErr

dmSetUp :: MonadIO m => DM -> m ()
dmSetUp sec = liftIO $ c'DMSetUp sec >>= chkErr

------------------------------------------------------------------------------
-- | Register additional DM types.
dmRegister :: MonadIO m => DMType -> DMCreateFun -> m ()
dmRegister typ fun = liftIO $ c'DMRegister typ fun >>= chkErr

-- | Register all known DM types.
dmRegisterAll :: MonadIO m => m ()
dmRegisterAll  = liftIO $ c'DMRegisterAll >>= chkErr

------------------------------------------------------------------------------
-- | Add a boundary-condition to the given mesh.
--   TODO: Make polymorphic in the context argument?
dmAddBoundary :: MonadIO m =>
  DM -> Bool -> String -> String -> Int -> [Int] -> BCFun -> [Int] -> PCtx ->
  m ()
dmAddBoundary dm ess name lab field comps bfun ids ctx = liftIO $ do
  withPString name $ \ps -> withPString lab $ \pl ->
    withArrayLen (fromIntegral <$> comps) $ \n ca ->
    withArrayLen (fromIntegral <$> ids) $ \m ia -> do
      let (n', m') = (fromIntegral n, fromIntegral m)
          e = pbool ess
          i = fromIntegral field
      bf <- maybe (pure nullFunPtr) mk'BCFun bfun
      c'DMAddBoundary dm e ps pl i n' ca bf m' ia ctx >>= chkErr

------------------------------------------------------------------------------
dmSetApplicationContext :: MonadIO m => DM -> PCtx -> m ()
dmSetApplicationContext dm ctx = liftIO $ do
  c'DMSetApplicationContext dm ctx >>= chkErr


-- ** Conversons, labeling, and types.
------------------------------------------------------------------------------
dmConvert :: MonadIO m => DM -> DMType -> m DM
dmConvert dm typ = liftIO $ with NoDM $ \ptr -> do
  c'DMConvert dm typ ptr >>= chkErr >> peek ptr

------------------------------------------------------------------------------
dmSetDimension :: MonadIO m => DM -> Int -> m ()
dmSetDimension dm dim = liftIO $ do
  c'DMSetDimension dm (fromIntegral dim) >>= chkErr

dmGetDimension :: MonadIO m => DM -> m Int
dmGetDimension dm = liftIO $ with 0 $ \ptr -> do
  c'DMGetDimension dm ptr >>= chkErr >> ipeek ptr

------------------------------------------------------------------------------
dmSetCoordinateDim :: MonadIO m => DM -> Int -> m ()
dmSetCoordinateDim dm dim = liftIO $ do
  c'DMSetCoordinateDim dm (fromIntegral dim) >>= chkErr

dmGetCoordinateDim :: MonadIO m => DM -> m Int
dmGetCoordinateDim dm = liftIO $ with 0 $ \ptr -> do
  c'DMGetCoordinateDim dm ptr >>= chkErr >> ipeek ptr

------------------------------------------------------------------------------
-- | If a mesh is periodic, then construct local coordinates for each cell.
dmLocalizeCoordinates :: MonadIO m => DM -> m ()
dmLocalizeCoordinates dm = liftIO $ c'DMLocalizeCoordinates dm >>= chkErr

{-- }
-- | If a mesh is periodic, then project each coordinate-component into some
--   interval `[0, L_i)`.
dmLocalizeCoordinate ::
  (MonadIO m, Functor f, Storable (f PScalar), Real a) => DM -> f a -> m (f a)
dmLocalizeCoordinate dm coord = liftIO $ do
  let coord' = realToFrac <$> coord
  with coord' $ \src -> with nullPtr $ \dst -> do
    c'DMLocalizeCoordinate dm src dst >>= chkErr >> peek dst
--}

------------------------------------------------------------------------------
-- | Stores the vector at which to compute the residual, Jacobian, and VI
--   bounds, for nonlinear problems.
--   NOTE: Not exported, and "developer" level?
-- dmSetVec :: MonadIO m => DM -> Vec -> m ()
-- dmSetVec dm vec = liftIO $ c'DMSetVec dm vec >>= chkErr

dmSetVecType :: MonadIO m => DM -> VecType -> m ()
dmSetVecType dm typ = liftIO $ c'DMSetVecType dm typ >>= chkErr

dmGetVecType :: MonadIO m => DM -> m VecType
dmGetVecType dm = liftIO $ with NoVecType $ \ptr -> do
  c'DMGetVecType dm ptr >>= chkErr >> peek ptr


-- ** Refinement & coarsening functions.
------------------------------------------------------------------------------
dmRefine :: MonadIO m => DM -> Comm -> m DM
dmRefine dm comm = liftIO $ with NoDM $ \ptr -> do
  c'DMRefine dm comm ptr >>= chkErr >> peek ptr

dmCoarsen :: MonadIO m => DM -> Comm -> m DM
dmCoarsen dm comm = liftIO $ with NoDM $ \ptr -> do
  c'DMCoarsen dm comm ptr >>= chkErr >> peek ptr

------------------------------------------------------------------------------
dmSetCoarseDM :: MonadIO m => DM -> DM -> m ()
dmSetCoarseDM dm cdm = liftIO $ c'DMSetCoarseDM dm cdm >>= chkErr

dmGetCoarseDM :: MonadIO m => DM -> m DM
dmGetCoarseDM dm = liftIO $ with NoDM $ \ptr -> do
  c'DMGetCoarseDM dm ptr >>= chkErr >> peek ptr

------------------------------------------------------------------------------
dmSetFineDM :: MonadIO m => DM -> DM -> m ()
dmSetFineDM cdm dm = liftIO $ c'DMSetFineDM cdm dm >>= chkErr

dmGetFineDM :: MonadIO m => DM -> m DM
dmGetFineDM dm = liftIO $ with NoDM $ \ptr -> do
  c'DMGetFineDM dm ptr >>= chkErr >> peek ptr

------------------------------------------------------------------------------
dmCreateLabel :: MonadIO m => DM -> String -> m ()
dmCreateLabel dm lab = liftIO $ withPString lab $ \str -> do
  c'DMCreateLabel dm str >>= chkErr

dmHasLabel :: MonadIO m => DM -> String -> m Bool
dmHasLabel dm lab = liftIO $ withPString lab $ \str -> with 0 $ \ptr -> do
  c'DMHasLabel dm str ptr >>= chkErr >> bpeek ptr

dmGetLabel :: MonadIO m => DM -> String -> m DMLabel
dmGetLabel dm lab = liftIO $ withPString lab $ \str ->
  with NoDMLabel $ \ptr -> do
  c'DMGetLabel dm str ptr >>= chkErr >> peek ptr


-- * Additional DM functions.
------------------------------------------------------------------------------
dmCreateGlobalVector :: MonadIO m => DM -> m Vec
dmCreateGlobalVector dm = liftIO $ with (Vec nullPtr) $ \pv -> do
  c'DMCreateGlobalVector dm pv >>= chkErr >> peek pv

dmCreateMatrix :: MonadIO m => DM -> m Mat
dmCreateMatrix dm = liftIO $ with (Mat nullPtr) $ \pm -> do
  c'DMCreateMatrix dm pm >>= chkErr >> peek pm

------------------------------------------------------------------------------
dmSetDefaultConstraints :: MonadIO m => DM -> Section -> Mat -> m ()
dmSetDefaultConstraints dm sec mat = liftIO $ do
  c'DMSetDefaultConstraints dm sec mat >>= chkErr

-- NOTE: Borrowed references, so don't destroy the returned objects.
dmGetDefaultConstraints :: MonadIO m => DM -> m (Section, Mat)
dmGetDefaultConstraints dm = liftIO $ with NoSection $ \ps -> do
  with NoMat $ \pm -> do
    c'DMGetDefaultConstraints dm ps pm >>= chkErr
    (,) <$> peek ps <*> peek pm


-- ** DM viewers.
------------------------------------------------------------------------------
-- | View a DM (collectively).
dmView :: MonadIO m => DM -> Viewer -> m ()
dmView dm view = liftIO $ c'DMView dm view >>= chkErr

-- | Parse command-line options to determine if/how a `DM` object is to be
--   viewed.
dmViewFromOptions :: MonadIO m => DM -> PetscObject -> String -> m ()
dmViewFromOptions dm obj opt = liftIO $ withCString opt $ \str -> do
  c'DMViewFromOptions dm obj str >>= chkErr


-- ** DM modes & settings.
------------------------------------------------------------------------------
dmSetType :: MonadIO m => DM -> DMType -> m ()
dmSetType dm dt = liftIO $ c'DMSetType dm dt >>= chkErr

dmGetType :: MonadIO m => DM -> m DMType
dmGetType dm = liftIO $ with NoDMType $ \dt -> do
  c'DMGetType dm dt >>= chkErr
  peek dt

------------------------------------------------------------------------------
dmSetMatType :: MonadIO m => DM -> MatType -> m ()
dmSetMatType dm mt = liftIO $ c'DMSetMatType dm mt >>= chkErr

dmGetMatType :: MonadIO m => DM -> m MatType
dmGetMatType dm = liftIO $ with (MatType nullPtr) $ \pt -> do
  c'DMGetMatType dm pt >>= chkErr >> peek pt


-- ** Local & work arrays.
------------------------------------------------------------------------------
dmGetWorkArray :: MonadIO m => DM -> Int -> PetscT -> m (Ptr ())
dmGetWorkArray dm n t = liftIO $ with nullPtr $ \ptr -> do
  c'DMGetWorkArray dm (fromIntegral n) t ptr >>= chkErr >> peek ptr

dmRestoreWorkArray :: MonadIO m => DM -> Int -> PetscT -> Ptr () -> m ()
dmRestoreWorkArray dm n t ar = liftIO $ with ar $ \ptr -> do
  c'DMRestoreWorkArray dm (fromIntegral n) t ptr >>= chkErr


-- ** MPI-related stuff.
------------------------------------------------------------------------------
dmGetLocalVector :: MonadIO m => DM -> m Vec
dmGetLocalVector dm = liftIO $ with (Vec nullPtr) $ \pv -> do
  c'DMGetLocalVector dm pv >>= chkErr >> peek pv

dmRestoreLocalVector :: MonadIO m => DM -> Vec -> m ()
dmRestoreLocalVector dm lv = liftIO $ with lv $ \pv -> do
  c'DMRestoreLocalVector dm pv >>= chkErr

------------------------------------------------------------------------------
-- | Check-out the underlying vector -- to be restored before the DM is used.
dmGetGlobalVector :: MonadIO m => DM -> m Vec
dmGetGlobalVector dm = liftIO $ with (Vec nullPtr) $ \pv -> do
  c'DMGetGlobalVector dm pv >>= chkErr >> peek pv

dmRestoreGlobalVector :: MonadIO m => DM -> Vec -> m ()
dmRestoreGlobalVector dm lv = liftIO $ with lv $ \pv -> do
  c'DMRestoreGlobalVector dm pv >>= chkErr

------------------------------------------------------------------------------
-- | Update the local-vector with the adjacent-node ghost-data.
dmGlobalToLocalBegin :: MonadIO m => DM -> Vec -> InsertMode -> Vec -> m ()
dmGlobalToLocalBegin dm gv m lv = liftIO $ do
  c'DMGlobalToLocalBegin dm gv m lv >>= chkErr

dmGlobalToLocalEnd :: MonadIO m => DM -> Vec -> InsertMode -> Vec -> m ()
dmGlobalToLocalEnd dm gv m lv = liftIO $ do
  c'DMGlobalToLocalEnd dm gv m lv >>= chkErr


-- * DMDA-specific functions.
------------------------------------------------------------------------------
-- | Obtain read-only access to the underlying array, of the given global and
--   local vectors.
--   NOTE: Assumes 2D array.
--   TODO: Use the type-system to enforce correct array dimensionality?
dmDAVecGetArrayRead :: MonadIO m => DM -> Vec -> m (Ptr PPReal)
dmDAVecGetArrayRead dm vec = liftIO $ with nullPtr $ \ptr -> do
  c'DMDAVecGetArrayRead dm vec ptr >>= chkErr >> peek ptr

dmDAVecRestoreArrayRead :: MonadIO m => DM -> Vec -> Ptr PPReal -> m ()
dmDAVecRestoreArrayRead dm vec pp = liftIO $ with pp $ \ptr -> do
  c'DMDAVecRestoreArrayRead dm vec ptr >>= chkErr

-- | Obtain read/write access to the underlying array, of the given global and
--   local vectors.
dmDAVecGetArray :: MonadIO m => DM -> Vec -> m (Ptr PPReal)
dmDAVecGetArray dm vec = liftIO $ with nullPtr $ \ptr -> do
  c'DMDAVecGetArray dm vec ptr >>= chkErr >> peek ptr

dmDAVecRestoreArray :: MonadIO m => DM -> Vec -> Ptr PPReal -> m ()
dmDAVecRestoreArray dm vec pp = liftIO $ with pp $ \ptr -> do
  c'DMDAVecRestoreArray dm vec ptr >>= chkErr

------------------------------------------------------------------------------
-- | Read the global coordinates of the lower-left corner, and the local
--   widths?
dmDAGetCorners :: MonadIO m => DM -> m ((Int, Int, Int), (Int, Int, Int))
dmDAGetCorners dm = liftIO $ allocaArray 6 $ \p0 -> do
  let [p1, p2, p3, p4, p5] = plusPtr p0 . (*s) <$> [1..5]
      s = sizeOf (undefined :: PInt)
  c'DMDAGetCorners dm p0 p1 p2 p3 p4 p5 >>= chkErr
  [x, y, z, m, n, p] <- fmap fromIntegral <$> peekArray 6 p0
  return ((x, y, z), (m, n, p))

dmDAGetGhostCorners :: MonadIO m => DM -> m ((Int, Int, Int), (Int, Int, Int))
dmDAGetGhostCorners dm = liftIO $ allocaArray 6 $ \p0 -> do
  let [p1, p2, p3, p4, p5] = plusPtr p0 . (*s) <$> [1..5]
      s = sizeOf (undefined :: PInt)
  c'DMDAGetGhostCorners dm p0 p1 p2 p3 p4 p5 >>= chkErr
  [x, y, z, m, n, p] <- fmap fromIntegral <$> peekArray 6 p0
  return ((x, y, z), (m, n, p))


-- ** DMDA info functions.
------------------------------------------------------------------------------
-- data DMDAInfo = DMDAInfo { _dims :: PInt

-- TODO: Use `StateVar`?
dmDAGetInfoDims :: MonadIO m => DM -> m Int
dmDAGetInfoDims dm = liftIO $ with 0 $ \ptr -> do
  c'DMDAGetInfo dm ptr
    PIgnore PIgnore PIgnore     -- M, N, P
    PIgnore PIgnore PIgnore     -- m, n, p
    PIgnore PIgnore             -- dofs, stencil-width
    PIgnore PIgnore PIgnore     -- bx, by, bz
    PIgnore                     -- stencil-type
    >>= chkErr
  ipeek ptr

dmDAGetInfoNums :: MonadIO m => DM -> m (Int, Int, Int)
dmDAGetInfoNums dm = liftIO $ do
  with 0 $ \pm -> with 0 $ \pn -> with 0 $ \pp -> do
    c'DMDAGetInfo dm  PIgnore
      pm pn PIgnore               -- M, N, P
      PIgnore PIgnore PIgnore     -- m, n, p
      PIgnore PIgnore             -- dofs, stencil-width
      PIgnore PIgnore PIgnore     -- bx, by, bz
      PIgnore                     -- stencil-type
      >>= chkErr
    (,,) <$> ipeek pm <*> ipeek pn <*> ipeek pp

dmDAGetLocalInfo :: MonadIO m => DM -> m (DMDALocalInfo PInt)
dmDAGetLocalInfo dm = liftIO $ alloca $ \p -> do
  c'DMDAGetLocalInfo dm p >>= chkErr >> peek p


{-- }
-- | Monadic `DM` environment.
newtype DMM a = DMM { runDMM :: ReaderT DM IO a }
              deriving (Functor, Applicative, Monad, MonadIO, Generic)

deriving instance MonadReader DM DMM

data DMVar a = DMVar (DM -> IO a) (DM -> a -> IO ())

makeDMVar :: DMM a -> (a -> DMM ()) -> DMVar a
makeDMVar g s = DMVar (runTD g) (\dm x -> runTD (s x) dm)

runTD = runReaderT . runDMM

instance HasGetter (DMVar a) a where
  {-# INLINE get #-}
  get (DMVar g _) = g

instance HasSetter (DMVar a) a where
  {-# INLINE ($=) #-}
  DMVar _ s $= x = s x

instance HasUpdate (DMVar a) a a where
  r $~ f = get >>= \x -> r $~ f
--}


-- * DM IS functions.
------------------------------------------------------------------------------
dmGetStratumIS :: MonadIO m => DM -> String -> Int -> m IS
dmGetStratumIS dm lab strat = liftIO $ do
  let n = fromIntegral strat
  withCString lab $ \str -> with NoIS $ \pis -> do
    c'DMGetStratumIS dm str n pis >>= chkErr
    peek pis

{-
------------------------------------------------------------------------------
dmSetDefaultSection :: MonadIO m => DM -> Section -> m ()
dmSetDefaultSection dm sec = liftIO $ c'DMSetDefaultSection dm sec >>= chkErr

dmGetDefaultSection :: MonadIO m => DM -> m Section
dmGetDefaultSection dm = liftIO $ with NoSection $ \ptr -> do
  c'DMGetDefaultSection dm ptr >>= chkErr >> peek ptr

------------------------------------------------------------------------------
dmSetDefaultGlobalSection :: MonadIO m => DM -> Section -> m ()
dmSetDefaultGlobalSection dm sec = liftIO $ do
  c'DMSetDefaultGlobalSection dm sec >>= chkErr

dmGetDefaultGlobalSection :: MonadIO m => DM -> m Section
dmGetDefaultGlobalSection dm = liftIO $ with NoSection $ \ptr -> do
  c'DMGetDefaultGlobalSection dm ptr >>= chkErr >> peek ptr
-}

------------------------------------------------------------------------------
dmSetCoordinateSection :: MonadIO m => DM -> Section -> m ()
dmSetCoordinateSection dm sec = liftIO $ do
  c'DMSetCoordinateSection dm sec >>= chkErr

dmGetCoordinateSection :: MonadIO m => DM -> m Section
dmGetCoordinateSection dm = liftIO $ with NoSection $ \ptr -> do
  c'DMGetCoordinateSection dm ptr >>= chkErr >> peek ptr


-- * DM discretisation (DS) data-types & functions.
------------------------------------------------------------------------------
-- | Value for empty/optional fields.
pattern NoDS :: DS
pattern NoDS <- ((== DS nullPtr) -> True)
  where NoDS = DS nullPtr

------------------------------------------------------------------------------
-- | Value for empty/optional discretisation type.
pattern NoDSType :: DSType
pattern NoDSType <- ((== DSType nullPtr) -> True)
  where NoDSType = DSType nullPtr

-- | Basic discretisation.
pattern DSBasic :: DSType
pattern DSBasic <- ((== DSType c'PETSCDSBASIC) -> True)
  where DSBasic = DSType c'PETSCDSBASIC

------------------------------------------------------------------------------
-- | Function-pointer types for residuals & Jacobians.
type DSFun = Maybe (PInt -> PInt -> PInt -> PPInt -> PPInt -> PPScalar -> PPScalar -> PPScalar -> PPInt -> PPInt -> PPScalar -> PPScalar -> PPScalar -> PReal -> PPReal -> PPScalar -> PRes)
type DSJac = Maybe (PInt -> PInt -> PInt -> PPInt -> PPInt -> PPScalar -> PPScalar -> PPScalar -> PPInt -> PPInt -> PPScalar -> PPScalar -> PPScalar -> PReal -> PReal -> PPReal -> PPScalar -> PRes)

------------------------------------------------------------------------------
-- | Value for empty/optional residual function.
pattern NoDSFun :: DSFun
pattern NoDSFun  = Nothing

-- | Value for empty/optional Jacobian function.
pattern NoDSJac :: DSJac
pattern NoDSJac  = Nothing


-- ** Standard PETSc functionality.
------------------------------------------------------------------------------
-- | Common `PetscObject` functionality for `PetscDS` objects.
instance IsPetscObject DS where
  create         = petscDSCreate
  setFromOptions = petscDSSetFromOptions
  setUp          = petscDSSetUp
  destroy        = petscDSDestroy
  {-# INLINE create #-}
  {-# INLINE setFromOptions #-}
  {-# INLINE setUp #-}
  {-# INLINE destroy #-}

#include "petsc_macro.h"
macro_PETSC_OBJECT_INST(petscFE,PFE)


-- ** DS constructors & destructors.
------------------------------------------------------------------------------
petscDSCreate :: MonadIO m => Comm -> m DS
petscDSCreate comm = liftIO $ with NoDS $ \pds -> do
  c'PetscDSCreate comm pds >>= chkErr >> peek pds

petscDSDestroy :: MonadIO m => DS -> m ()
petscDSDestroy ds = liftIO $ with ds $ \pds -> do
  c'PetscDSDestroy pds >>= chkErr


-- ** DS setup functions.
------------------------------------------------------------------------------
petscDSSetFromOptions :: MonadIO m => DS -> m ()
petscDSSetFromOptions ds = liftIO $ c'PetscDSSetFromOptions ds >>= chkErr

petscDSSetUp :: MonadIO m => DS -> m ()
petscDSSetUp ds = liftIO $ c'PetscDSSetUp ds >>= chkErr

petscDSView :: MonadIO m => DS -> Viewer -> m ()
petscDSView ds view = liftIO $ c'PetscDSView ds view >>= chkErr


-- ** Additional setters/getters.
------------------------------------------------------------------------------
petscDSSetType :: MonadIO m => DS -> DSType -> m ()
petscDSSetType ds dt = liftIO $ c'PetscDSSetType ds dt >>= chkErr

petscDSGetType :: MonadIO m => DS -> m DSType
petscDSGetType ds = liftIO $ with NoDSType $ \pdt -> do
  c'PetscDSGetType ds pdt >>= chkErr >> peek pdt

------------------------------------------------------------------------------
petscDSGetNumFields :: MonadIO m => DS -> m Int
petscDSGetNumFields ds = liftIO $ with 0 $ \pn -> do
  c'PetscDSGetNumFields ds pn >>= chkErr >> ipeek pn

petscDSGetSpatialDimension :: MonadIO m => DS -> m Int
petscDSGetSpatialDimension ds = liftIO $ with 0 $ \pdim -> do
  c'PetscDSGetSpatialDimension ds pdim >>= chkErr >> ipeek pdim


{-- }
-- TODO: these have changed in later versions of PETSc?

-- *** DM <-> DS functionality.
------------------------------------------------------------------------------
dmSetDS :: MonadIO m => DM -> DS -> m ()
dmSetDS dm ds = liftIO $ c'DMSetDS dm ds >>= chkErr

dmGetDS :: MonadIO m => DM -> m DS
dmGetDS dm = liftIO $ with NoDS $ \pds -> do
  c'DMGetDS dm pds >>= chkErr >> peek pds
--}

------------------------------------------------------------------------------
dmSetField :: MonadIO m => DM -> Int -> PetscObject -> m ()
dmSetField dm i obj = liftIO $ do
  c'DMSetField dm (fromIntegral i) obj >>= chkErr

dmGetField :: MonadIO m => DM -> Int -> m PetscObject
dmGetField dm i = liftIO $ with NoObject $ \po -> do
  c'DMGetField dm (fromIntegral i) po >>= chkErr >> peek po


-- *** Callback setters/getters.
------------------------------------------------------------------------------
petscDSSetResidual :: MonadIO m => DS -> Int -> DSFun -> DSFun -> m ()
petscDSSetResidual ds i f0 f1 = liftIO $ do
  f0' <- maybe (pure nullFunPtr) mk'DSFun f0
  f1' <- maybe (pure nullFunPtr) mk'DSFun f1
  c'PetscDSSetResidual ds (fromIntegral i) f0' f1' >>= chkErr

petscDSSetJacobian ::
  MonadIO m => DS -> Int -> Int -> DSJac -> DSJac -> DSJac -> DSJac -> m ()
petscDSSetJacobian ds i j g0 g1 g2 g3 = liftIO $ do
  g0' <- maybe (pure nullFunPtr) mk'DSJac g0
  g1' <- maybe (pure nullFunPtr) mk'DSJac g1
  g2' <- maybe (pure nullFunPtr) mk'DSJac g2
  g3' <- maybe (pure nullFunPtr) mk'DSJac g3
  let (i', j') = (fromIntegral i, fromIntegral j)
  c'PetscDSSetJacobian ds i' j' g0' g1' g2' g3' >>= chkErr

petscDSSetDiscretization :: MonadIO m => DS -> Int -> PetscObject -> m ()
petscDSSetDiscretization ds i obj = liftIO $ do
  c'PetscDSSetDiscretization ds (fromIntegral i) obj >>= chkErr

{-
petscDSSetBdDiscretization :: MonadIO m => DS -> Int -> PetscObject -> m ()
petscDSSetBdDiscretization ds i obj = liftIO $ do
  c'PetscDSSetBdDiscretization ds (fromIntegral i) obj >>= chkErr
-}


-- * Finite Element (FE) discretisations.
------------------------------------------------------------------------------
petscFECreate :: MonadIO m => Comm -> m PFE
petscFECreate comm = liftIO $ with NoPFE $ \ptr -> do
  c'PetscFECreate comm ptr >>= chkErr >> peek ptr

petscFEDestroy :: MonadIO m => PFE -> m ()
petscFEDestroy obj = liftIO $ with obj $ \ptr -> do
  c'PetscFEDestroy ptr >>= chkErr

------------------------------------------------------------------------------
petscFECreateDefault ::
  MonadIO m => DM -> Int -> Int -> Bool -> String -> Int -> m PFE
petscFECreateDefault dm dim ncomp simplex pfx qorder = liftIO $ do
  let [d, n, q] = fromIntegral <$> [dim, ncomp, qorder]
      s = pbool simplex
  withPString pfx $ \pstr -> with NoPFE $ \pfe -> do
    c'PetscFECreateDefault dm d n s pstr q pfe >>= chkErr >> peek pfe

------------------------------------------------------------------------------
petscFESetFromOptions :: MonadIO m => PFE -> m ()
petscFESetFromOptions obj = liftIO $ c'PetscFESetFromOptions obj >>= chkErr

petscFESetUp :: MonadIO m => PFE -> m ()
petscFESetUp obj = liftIO $ c'PetscFESetUp obj >>= chkErr

------------------------------------------------------------------------------
petscFESetQuadrature :: MonadIO m => PFE -> QRule -> m ()
petscFESetQuadrature pfe obj = liftIO $ do
  c'PetscFESetQuadrature pfe obj >>= chkErr

petscFEGetQuadrature :: MonadIO m => PFE -> m QRule
petscFEGetQuadrature pfe = liftIO $ with NoQRule $ \ptr -> do
  c'PetscFEGetQuadrature pfe ptr >>= chkErr >> peek ptr


-- * Quadrature rules.
------------------------------------------------------------------------------
-- | Quadrature-rule data-types & values.
pattern NoQRule :: QRule
pattern NoQRule <- ((== QRule nullPtr) -> True)
  where NoQRule  = QRule nullPtr


-- ** Quadrature-rule instances.
------------------------------------------------------------------------------
macro_PETSC_OBJECT_INST(petscQuadrature,QRule)


-- ** Constructors & destructors.
------------------------------------------------------------------------------
petscQuadratureCreate :: MonadIO m => Comm -> m QRule
petscQuadratureCreate comm = liftIO $ with NoQRule $ \ptr -> do
  c'PetscQuadratureCreate comm ptr >>= chkErr >> peek ptr

petscQuadratureDestroy :: MonadIO m => QRule -> m ()
petscQuadratureDestroy obj = liftIO $ with obj $ \ptr -> do
  c'PetscQuadratureDestroy ptr >>= chkErr


-- ** Additional quadrature-rule functions.
------------------------------------------------------------------------------
-- | Fake `setFromOptions` method, for the above object instance.
petscQuadratureSetFromOptions :: Applicative m => QRule -> m ()
petscQuadratureSetFromOptions  = const $ pure ()

-- | Fake `setUp` method, for the above object instance.
petscQuadratureSetUp :: Applicative m => QRule -> m ()
petscQuadratureSetUp  = const $ pure ()


-- ** Quadrature-rule queries.
------------------------------------------------------------------------------
petscQuadratureGetOrder :: MonadIO m => QRule -> m Int
petscQuadratureGetOrder qrule = liftIO $ with 0 $ \ptr -> do
  c'PetscQuadratureGetOrder qrule ptr >>= chkErr >> ipeek ptr

------------------------------------------------------------------------------
data QRuleData =
  QRuleData { qrdim :: Int
            , qrnum :: Int
            , qrpts :: Vector Double
            , qrwts :: Vector Double
            } deriving (Eq, Show, Generic)

petscQuadratureGetData :: MonadIO m => QRule -> m QRuleData
petscQuadratureGetData qrule = liftIO $ with 0 $ \pd -> with 0 $ \pn -> do
  with nullPtr $ \pp -> with nullPtr $ \pw -> do
    c'PetscQuadratureGetData qrule pd pn pp pw >>= chkErr
    dim <- ipeek pd
    num <- ipeek pn
    pp' <- peek pp
    pw' <- peek pw
    QRuleData dim num
      <$> vdbl `fmap` fromArrayM num pp'
      <*> vdbl `fmap` fromArrayM num pw'


-- * Vector environments.
------------------------------------------------------------------------------
withGlobalVector :: MonadIO m => DM -> (Vec -> m a) -> m a
withGlobalVector dm action = do
  vec <- dmGetGlobalVector dm
  res <- action vec
  dmRestoreLocalVector dm vec
  return res
