{-# LANGUAGE CPP, TypeOperators, PatternSynonyms, ViewPatterns, TupleSections
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Vec
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Mid-level bindings to the `Vec` objects of PETSc.
-- 
-- Changelog:
--  + 11/06/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.Vec
  ( Vec (..)
  , VecType (..)
  , pattern VecMPI
  , pattern VecSEQ
  , pattern VecStandard
  , pattern NoVecType
  , pattern AddValues
  , pattern InsertValues
  , pattern PDetermine
  , pattern PDecide
    -- Constructors:
  , vecCreate
  , vecDestroy
  , vecEmpty
  , vecNULL
  , vecZeros
  , vecDuplicate
  , fromVector
  , toVector
    -- Assembly helpers:
  , vecAssemblyBegin
  , vecAssemblyEnd
  , vecSetFromOptions
  , vecSetUp
    -- Setters, getters, and modifiers:
  , vecSetType
  , vecSetSizes
  , vecGetSize
  , vecGetLocalSize
  , vecSet
  , vecChop
  , withVecArray
  , vecSetValues
  , vecAddValues
  , vecGetValues
  , vecGetArray
  , vecRestoreArray
  , fillVec
  , vecNorm
  , vecView
  ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal
import Control.Monad.Primitive
import Control.Monad.IO.Class
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vector
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Storable.Mutable as Mut

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Vec
import Bindings.Numeric.PETSc.Internal


-- * Vector-type patterns.
------------------------------------------------------------------------------
-- | Vector `VecType` patterns.
pattern VecMPI :: VecType
pattern VecMPI <- ((== VecType c'VECMPI) -> True) where
  VecMPI = VecType c'VECMPI

pattern VecSEQ :: VecType
pattern VecSEQ <- ((== VecType c'VECSEQ) -> True) where
  VecSEQ = VecType c'VECSEQ

pattern VecStandard :: VecType
pattern VecStandard <- ((== VecType c'VECSTANDARD) -> True) where
  VecStandard = VecType c'VECSTANDARD

------------------------------------------------------------------------------
pattern NoVecType :: VecType
pattern NoVecType <- ((== VecType nullPtr) -> True)
  where NoVecType  = VecType nullPtr


-- * Standard `Vec` functions.
------------------------------------------------------------------------------
-- | Common `PetscObject` functionality for `Vec` objects.
#include "petsc_macro.h"
macro_PETSC_OBJECT_INST(vec,Vec)

{-- }
instance IsPetscObject Vec where
  create         = vecCreate
  setFromOptions = vecSetFromOptions
  setUp          = vecSetUp
  destroy        = vecDestroy
  {-# INLINE create #-}
  {-# INLINE setFromOptions #-}
  {-# INLINE setUp #-}
  {-# INLINE destroy #-}
--}


-- * Vector operations.
------------------------------------------------------------------------------
-- | Create a new, empty vector.
vecCreate :: MonadIO m => Comm -> m Vec
vecCreate comm = liftIO $ with NoVec $ \pvec -> do
  c'VecCreate comm pvec >>= chkErr >> peek pvec

-- | Call PETSc's destructor.
--   NOTE: Unsafe.
--   TODO: Use FFI's `ForeignPtr` to handle this transparently?
vecDestroy :: MonadIO m => Vec -> m ()
vecDestroy vec = liftIO $ with vec $ \pvec -> c'VecDestroy pvec >>= chkErr


-- ** Additional vector constructors.
------------------------------------------------------------------------------
vecEmpty :: MonadIO m => m Vec
vecEmpty  = return vecNULL

-- OBSOLETE: More useful to use `NoVec`?
vecNULL :: Vec
vecNULL  = Vec nullPtr

vecZeros :: Integral i => Comm -> i -> IO Vec
vecZeros comm n = do
  let m = fromIntegral n
  vec <- vecCreate comm
  vecSetType  vec VecStandard
  vecSetSizes vec m m
  vecSet vec 0 >> return vec

vecDuplicate :: MonadIO m => Vec -> m Vec
vecDuplicate v = liftIO $ with vecNULL $ \p -> do
  c'VecDuplicate v p >>= chkErr >> peek p

-- ** Vector assembly helpers
------------------------------------------------------------------------------
-- | Start parallel, vector assembly.
vecAssemblyBegin :: MonadIO m => Vec -> m ()
vecAssemblyBegin v = liftIO $ do
  c'VecAssemblyBegin v >>= chkErr

-- | Finish parallel, vector assembly.
vecAssemblyEnd :: MonadIO m => Vec -> m ()
vecAssemblyEnd v = liftIO $ do
  c'VecAssemblyEnd v >>= chkErr


-- ** Additional setup functions.
------------------------------------------------------------------------------
-- | Set using the options database.
vecSetFromOptions :: MonadIO m => Vec -> m ()
vecSetFromOptions vec = liftIO $ c'VecSetFromOptions vec >>= chkErr

-- | Finalise the vector.
vecSetUp :: MonadIO m => Vec -> m ()
vecSetUp vec = liftIO $ c'VecSetUp vec >>= chkErr


-- ** Set/get vector attributes.
------------------------------------------------------------------------------
vecSetType :: MonadIO m => Vec -> VecType -> m ()
vecSetType v t = liftIO $ c'VecSetType v t >>= chkErr

vecSetSizes :: MonadIO m => Vec -> PInt -> PInt -> m ()
vecSetSizes v m n = liftIO $ c'VecSetSizes v m n >>= chkErr


-- *** Additional queries.
------------------------------------------------------------------------------
vecGetSize :: MonadIO m => Vec -> m PInt
vecGetSize v = liftIO (with 0 $ \p -> c'VecGetSize v p >>= chkErr >> peek p)

vecGetLocalSize :: MonadIO m => Vec -> m Int
vecGetLocalSize v = liftIO $ do
  with 0 $ \p -> c'VecGetLocalSize v p >>= chkErr >> ipeek p


-- ** Vector value modification functions.
------------------------------------------------------------------------------
-- | Set all values of the vector to the given value.
vecSet :: (MonadIO m, Real a) => Vec -> a -> m ()
{-# SPECIALISE vecSet :: Vec -> Double -> IO () #-}
{-# SPECIALISE vecSet :: Vec -> PReal  -> IO () #-}
{-# INLINABLE[1]  vecSet #-}
vecSet vec x = liftIO $ c'VecSet vec (realToFrac x) >>= chkErr

-- | Chop (zero) all values below the given (absolute) threshold.
vecChop :: (MonadIO m, Real a) => Vec -> a -> m ()
{-# SPECIALISE vecChop :: Vec -> Double -> IO () #-}
{-# SPECIALISE vecChop :: Vec -> PReal  -> IO () #-}
{-# INLINABLE[1]  vecChop #-}
vecChop vec tol = liftIO $ c'VecChop vec (realToFrac tol) >>= chkErr


------------------------------------------------------------------------------
-- | Directly access the memory used by the vector.
withVecArray :: Vec -> (MVector (PrimState IO) Double -> IO ()) -> IO ()
withVecArray v k = with nullPtr $ \p -> do
  c'VecGetArray v p >>= chkErr
  n <- fromIntegral <$> vecGetSize v
  q <- peek p >>= newForeignPtr_ . castPtr
  k $ Mut.unsafeFromForeignPtr0 q n
  c'VecRestoreArray v p >>= chkErr

-- | Set (and overwrite) the values of the vector, and at the given locations.
vecSetValues :: Vec -> Vector PInt -> Vector PReal -> IO ()
vecSetValues vec js xs =
  Vector.unsafeWith js $ \p -> do
    Vector.unsafeWith xs $ \q -> do
      c'VecSetValues vec (len js) p q InsertValues >>= chkErr

-- | Add the given values, and at the specificed locations, to the input
--   vector.
vecAddValues :: Vec -> Vector PInt -> Vector PReal -> IO ()
vecAddValues vec js xs =
  Vector.unsafeWith js $ \p -> do
    Vector.unsafeWith xs $ \q -> do
      c'VecSetValues vec (len js) p q AddValues >>= chkErr

------------------------------------------------------------------------------
-- | Check-in the values of the `Vec` object.
vecGetArray :: Vec -> IO (Vector PReal)
vecGetArray  = error "vecGetArray: unimplemented."

vecRestoreArray :: Vec -> Vector PReal -> IO Vec
vecRestoreArray  = error "vecRestoreArray: unimplemented."


-- ** Operations using/producing `vector` arrays.
------------------------------------------------------------------------------
-- | Allocate a new `Vec`, and fill it with values from the given vector.
fromVector :: Comm -> Vector Double -> IO Vec
fromVector comm xs = do
  vec <- vecCreate comm
  vecSetType  vec VecStandard
  vecSetSizes vec (len xs) (len xs)
  fillVec xs  vec

fillVec :: Vector Double -> Vec -> IO Vec
fillVec xs vec = do
  let n   = Vector.length xs
      xs' = Vector.map realToFrac xs
  vecSetValues vec (Vector.enumFromN 0 n) xs'
  return vec

-- | Read data out from a `Vec`, returning a vector.
toVector :: Vec -> IO (Vector Double)
toVector vec = do
  xi <- Vector.enumFromN 0 . fromIntegral <$> vecGetSize vec
  vecGetValues vec xi

------------------------------------------------------------------------------
-- | Extract the values of the `Vec` object.
vecGetValues :: (MonadIO m, Storable a, Fractional a) =>
  Vec -> Vector PInt -> m (Vector a)
{-# SPECIALISE vecGetValues :: Vec -> Vector PInt -> IO (Vector Double) #-}
{-# SPECIALISE vecGetValues :: Vec -> Vector PInt -> IO (Vector PReal ) #-}
vecGetValues vec js = liftIO $ do
  Vector.unsafeWith js $ \p -> do
    ar <- Mut.new (Vector.length js)
    Mut.unsafeWith ar $ \q -> do
      c'VecGetValues vec (len js) p q >>= chkErr
    Vector.map realToFrac <$> Vector.unsafeFreeze ar


-- ** Vector queries.
------------------------------------------------------------------------------
vecNorm :: (MonadIO m, Fractional a) => Vec -> NormType -> m a
{-# SPECIALISE   vecNorm :: Vec -> NormType -> IO Double #-}
{-# SPECIALISE   vecNorm :: Vec -> NormType -> IO PReal  #-}
{-# INLINABLE[1] vecNorm #-}
vecNorm vec typ = liftIO $ with 0 $ \ptr -> do
  c'VecNorm vec typ ptr >>= chkErr >> realToFrac <$> peek ptr


-- ** Vector output.
------------------------------------------------------------------------------
vecView :: MonadIO m => Vec -> Viewer -> m ()
{-# SPECIALISE   vecView :: Vec -> Viewer -> IO () #-}
{-# INLINABLE[1] vecView #-}
vecView v p = liftIO $ c'VecView v p >>= chkErr >> return ()
