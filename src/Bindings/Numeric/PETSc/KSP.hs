{-# LANGUAGE TypeOperators, PatternSynonyms, ViewPatterns, TupleSections
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.KSP
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Mid-level bindings to the Krylov subspace module of PETSc.
-- 
-- Changelog:
--  + 11/06/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.KSP
       ( KSP(..), KSPType(..)
       , pattern AddValues, pattern InsertValues
       , pattern PDecide, pattern PDetermine
       , module Bindings.Numeric.PETSc.KSP
       ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.Marshal
import Control.Monad.IO.Class

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.KSP
import Bindings.Numeric.PETSc.Internal


-- * KSP constructors & destructors.
------------------------------------------------------------------------------
-- | Create a new KSP context.
kspCreate :: Comm -> IO KSP
kspCreate c = with (KSP nullPtr) $ \s -> do
  c'KSPCreate c s >>= chkErr >> peek s

-- | Destroy a KSP context.
kspDestroy :: MonadIO m => KSP -> m ()
kspDestroy ksp = liftIO (with ksp $ \p -> c'KSPDestroy p >>= chkErr)

-- withKSP :: MonadIO m => KSP -> KSPM a -> m a
-- withKSP ksp actn =


-- ** KSP setup.
------------------------------------------------------------------------------
kspSetFromOptions :: MonadIO m => KSP -> m ()
kspSetFromOptions ksp = liftIO $ c'KSPSetFromOptions ksp >>= chkErr

kspSetOperators :: MonadIO m => KSP -> Mat -> Mat -> m ()
kspSetOperators ksp aa pp = liftIO $ c'KSPSetOperators ksp aa pp >>= chkErr


-- * KSP solution functions.
------------------------------------------------------------------------------
kspSolve :: MonadIO m => KSP -> Vec -> Vec -> m ()
kspSolve ksp bb xx = liftIO $ c'KSPSolve ksp bb xx >>= chkErr
