{-# LANGUAGE TypeOperators, PatternSynonyms, ViewPatterns, TupleSections
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.Mat
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Mid-level bindings to the `Mat` objects of PETSc.
-- 
-- Changelog:
--  + 11/06/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.Mat
       ( Mat (..)
       , MatType (..)
       , MatOption (..)
       , MatStencil (..)
       , MatNullSpace (..)
       , Func
       , Jaco
       , pattern AddValues
       , pattern InsertValues
       , pattern PDecide
       , pattern PDetermine
       , pattern MATAIJ
       , pattern MATDENSE
       , pattern MAT_FINAL_ASSEMBLY
       , pattern MAT_FLUSH_ASSEMBLY
       , module Bindings.Numeric.PETSc.Mat
       ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.ForeignPtr
import Foreign.Marshal
import System.IO.Unsafe
import Control.Monad.IO.Class
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.Core (pbool, hbool)
import Bindings.Numeric.PETSc.Internal.Mat
import Bindings.Numeric.PETSc.Internal


-- * Standard `Mat` functions.
------------------------------------------------------------------------------
-- | Common `PetscObject` functionality for `Mat` objects.
instance IsPetscObject Mat where
  create         = matCreate
  setFromOptions = matSetFromOptions
  setUp          = matSetUp
  destroy        = matDestroy
  {-# INLINE create #-}
  {-# INLINE setFromOptions #-}
  {-# INLINE setUp #-}
  {-# INLINE destroy #-}


-- * Basic `Mat` operations.
------------------------------------------------------------------------------
matCreate :: MonadIO m => Comm -> m Mat
matCreate comm = liftIO $ with NoMat $ \pmat -> do
  c'MatCreate comm pmat >>= chkErr >> peek pmat

matSetFromOptions :: MonadIO m => Mat -> m ()
matSetFromOptions mat = liftIO $ c'MatSetFromOptions mat >>= chkErr

matSetUp :: MonadIO m => Mat -> m ()
matSetUp mat = liftIO $ c'MatSetUp mat >>= chkErr

matDestroy :: MonadIO m => Mat -> m ()
matDestroy mat = liftIO $ with mat $ \pmat -> c'MatDestroy pmat >>= chkErr

------------------------------------------------------------------------------
matView :: MonadIO m => Mat -> Viewer -> m ()
matView mat view = liftIO $ c'MatView mat view >>= chkErr


-- ** Getters/setters for matrix parameters.
------------------------------------------------------------------------------
matSetSizes :: MonadIO m => Mat -> PInt -> PInt -> PInt -> PInt -> m ()
matSetSizes mat lm ln gm gn =
 liftIO $ c'MatSetSizes mat lm ln gm gn >>= chkErr

matSetOption :: MonadIO m => Mat -> MatOption -> Bool -> m ()
matSetOption mat opt x = liftIO $ c'MatSetOption mat opt (pbool x) >>= chkErr


-- ** Additional matrix constructors.
------------------------------------------------------------------------------
matEmpty :: Monad m => m Mat
matEmpty  = return matNULL

-- OBSOLETE: Replaced by the more-useful `NoMat`?
matNULL :: Mat
matNULL  = Mat nullPtr


-- ** Matrix finalisation operations.
------------------------------------------------------------------------------
matAssemblyBegin :: MonadIO m => Mat -> MatAssemblyType -> m ()
matAssemblyBegin mat typ = liftIO $ c'MatAssemblyBegin mat typ >>= chkErr

matAssemblyEnd :: MonadIO m => Mat -> MatAssemblyType -> m ()
matAssemblyEnd mat typ = liftIO $ c'MatAssemblyEnd mat typ >>= chkErr


-- * Matrix queries.
------------------------------------------------------------------------------
matAssembled :: MonadIO m => Mat -> m Bool
matAssembled mat = liftIO $ with (pbool False) $ \pb -> do
  c'MatAssembled mat pb >>= chkErr >> hbool `fmap` peek pb


-- * Matrix-element getters & setters.
------------------------------------------------------------------------------
matSetValuesStencil :: Mat -> Vector MatStencil -> Vector MatStencil -> Vector Double -> InsertMode -> IO ()
matSetValuesStencil mat rows cols vals mode = do
  Vec.unsafeWith rows $ \rp -> do
    Vec.unsafeWith cols $ \cp -> do
      Vec.unsafeWith vals $ \xp -> do
        let m = fromIntegral $ len rows
            n = fromIntegral $ len cols
        c'MatSetValuesStencil mat m rp n cp (castPtr xp) mode >>= chkErr

matGetValues :: MonadIO m => Mat -> Vector Int -> Vector Int -> m (Vector Double)
matGetValues mat rows cols = liftIO $ do
  Vec.unsafeWith (Vec.map fromIntegral rows) $ \rp -> do
    Vec.unsafeWith (Vec.map fromIntegral cols) $ \cp -> do
      let m = fromIntegral $ len rows
          n = fromIntegral $ len cols
          s = fromIntegral (m*n)
      allocaArray s $ \vp -> do
        c'MatGetValues mat m rp n cp vp >>= chkErr
        Vec.unsafeCast . Vec.fromList <$> peekArray s vp

-- ** Pre-allocation of memory.
------------------------------------------------------------------------------
matMPIAIJSetPreallocationCSR ::
     Mat           -- ^ MPIAIJ matrix
  -> Vector PInt   -- ^ array of row-pointers (length is @n+1@)
  -> Vector PInt   -- ^ array of column indices (length is NNZ)
  -> Vector Double -- ^ (optional) array of matrix values
  -> IO Mat        -- ^ just the input matrix
matMPIAIJSetPreallocationCSR mat rows cols vals = do
  let xs = Vec.unsafeCast vals :: Vector PScalar
  Vec.unsafeWith rows $ \rp -> do
    Vec.unsafeWith cols $ \cp -> do
      Vec.unsafeWith xs $ \xp -> do
        c'MatMPIAIJSetPreallocationCSR mat rp cp xp >>= chkErr
  return mat
{-# INLINABLE matMPIAIJSetPreallocationCSR #-}

matCreateMPIAIJWithArrays ::
     Comm          -- ^ MPI communicator
  -> PInt          -- ^ number of global rows (or @PDecide@)
  -> PInt          -- ^ number of global columns (or @PDecide@)
  -> Vector PInt   -- ^ array of row-pointers (length is @n+1@)
  -> Vector PInt   -- ^ array of column indices (length is NNZ)
  -> Vector Double -- ^ (optional) array of matrix values
  -> IO Mat        -- ^ just the input matrix
matCreateMPIAIJWithArrays comm m n rows cols vals = do
  let lr = fromIntegral $ len rows
      lc = fromIntegral $ len cols
      xs = Vec.unsafeCast vals :: Vector PScalar
  Vec.unsafeWith rows $ \rp -> do
    Vec.unsafeWith cols $ \cp -> do
      Vec.unsafeWith xs $ \xp -> do
        with NoMat $ \mp -> do
          c'MatCreateMPIAIJWithArrays comm lr lc m n rp cp xp mp >>= chkErr
          peek mp
{-# INLINABLE matCreateMPIAIJWithArrays #-}

-- ** Push & pop matrix (element) values.
------------------------------------------------------------------------------
matStoreValues :: MonadIO m => Mat -> m ()
matStoreValues mat = liftIO $ c'MatStoreValues mat >>= chkErr

matRetrieveValues :: MonadIO m => Mat -> m ()
matRetrieveValues mat = liftIO $ c'MatRetrieveValues mat >>= chkErr


-- * Matrix-free operations.
------------------------------------------------------------------------------
matCreateMFFD :: Comm -> PInt -> PInt -> IO Mat
matCreateMFFD c m n = with (Mat nullPtr) $ \p -> do
  c'MatCreateMFFD c PDecide PDecide m n p >>= chkErr >> peek p

matCreateSNESMF :: SNES -> IO Mat
matCreateSNESMF snes = with (Mat nullPtr) $ \p -> do
  c'MatCreateSNESMF snes p >>= chkErr >> peek p


-- ** Jacobian function helpers.
------------------------------------------------------------------------------
snesComputeJacobianDefault :: SNES -> Vec -> Mat -> Mat -> Ptr () -> IO PErr
snesComputeJacobianDefault  = c'SNESComputeJacobianDefault

snesComputeJacobianDefaultColor :: SNES -> Vec -> Mat -> Mat -> Ptr () -> IO PErr
snesComputeJacobianDefaultColor  = c'SNESComputeJacobianDefaultColor

matMFFDComputeJacobian :: SNES -> Vec -> Mat -> Mat -> Ptr () -> IO PErr
matMFFDComputeJacobian  = c'MatMFFDComputeJacobian

liftJacoIO :: (SNES -> Vec -> Mat -> Mat -> Ptr () -> IO PErr) -> IO Jaco
-- liftJacoIO  = mk'J
liftJacoIO  = mk'J

mffdJaco :: Jaco
mffdJaco  = unsafePerformIO $ mk'J c'MatMFFDComputeJacobian
-- mffdJaco  = unsafePerformIO $ mk'J c'SNESComputeJacobianDefault


-- * Matrix operations on coordinates (e.g., for mesh refinement &
--   coarsening).
------------------------------------------------------------------------------
-- | Depending on the shape of the matrix:
--     y = A*x  OR  y = A'*x
matInterpolate :: MonadIO m => Mat -> Vec -> Vec -> m ()
matInterpolate mat x y = liftIO $ c'MatInterpolate mat x y >>= chkErr

-- | Depending on the shape of the matrix:
--     y = A*x  OR  y = A'*x
matRestrict    :: MonadIO m => Mat -> Vec -> Vec -> m ()
matRestrict    mat x y = liftIO $ c'MatRestrict    mat x y >>= chkErr
