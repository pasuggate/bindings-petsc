{-# LANGUAGE TypeOperators, PatternSynonyms, ViewPatterns, TupleSections
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Bindings.Numeric.PETSc.TS
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Mid-level bindings to Time-Stepping (TS) modules of PETSc.
-- 
-- Changelog:
--  + 05/07/2017  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Bindings.Numeric.PETSc.TS
       ( TS(..), TSType(..)
       , TSExactFinalTimeOption, pattern TSFinalTimeStepOver
       , pattern TSFinalTimeInterpolate, pattern TSFinalTimeMatchStep
       , TSIFunction(..), TSRHSFunction(..)
       , TSIJacobian(..), TSRHSJacobian(..)
       , pattern TSTHETA, pattern TSCN, pattern TSBEULER
       , TSProblemType(..), pattern TSLinear, pattern TSNonlinear
       -- ^ callback constructors:
       , mk'TSRHSFunction, mk'TSRHSJacobian
       , module Bindings.Numeric.PETSc.TS
       ) where

import Foreign.Storable
import Foreign.Ptr
import Foreign.Marshal
import Control.Monad.IO.Class

import Bindings.Numeric.PETSc.Internal.Types
import Bindings.Numeric.PETSc.Internal.TS
import Bindings.Numeric.PETSc.Internal


-- * Constructors & destructors.
------------------------------------------------------------------------------
tsCreate :: MonadIO m => Comm -> m TS
tsCreate comm = liftIO $ do
  with (TS nullPtr) $ \ptr -> c'TSCreate comm ptr >>= chkErr >> peek ptr

tsDestroy :: MonadIO m => TS -> m ()
tsDestroy ts = liftIO $ with ts $ \pt -> do
  c'TSDestroy pt >>= chkErr


-- * Basic settings.
------------------------------------------------------------------------------
tsSetUp :: MonadIO m => TS -> m ()
tsSetUp ts = liftIO $ c'TSSetUp ts >>= chkErr

tsSetFromOptions :: MonadIO m => TS -> m ()
tsSetFromOptions ts = liftIO $ c'TSSetFromOptions ts >>= chkErr


-- ** Additional getters & setters.
------------------------------------------------------------------------------
tsSetType :: MonadIO m => TS -> TSType -> m ()
tsSetType ts t = liftIO $ c'TSSetType ts t >>= chkErr

tsGetType :: MonadIO m => TS -> m TSType
tsGetType ts = liftIO $ do
  with (TSType nullPtr) $ \ptr -> c'TSGetType ts ptr >>= chkErr >> peek ptr

------------------------------------------------------------------------------
-- | Is the problem linear (`TSLinear`), or nonlinear (`TSNonlinear`)?
--   NOTE:
--     
--     U_t  -  A.U     =  0  (linear)
--     U_t  -  A(t).U  =  0  (linear)
--       F(t, U, U_t)  =  0  (nonlinear)
--     
tsSetProblemType :: MonadIO m => TS -> TSProblemType -> m ()
tsSetProblemType ts pt = liftIO $ c'TSSetProblemType ts pt >>= chkErr

------------------------------------------------------------------------------
-- | Set the maximum number of time-steps, and maximum simulation-time.
tsSetDuration :: MonadIO m => TS -> Int -> Double -> m ()
tsSetDuration ts n t = liftIO $ do
  c'TSSetDuration ts (fromIntegral n) (realToFrac t) >>= chkErr

tsSetTimeStep :: MonadIO m => TS -> Double -> m ()
tsSetTimeStep ts dt = liftIO $ do
  c'TSSetTimeStep ts (realToFrac dt) >>= chkErr

------------------------------------------------------------------------------
-- | Query current simulation-time.
tsGetTime :: MonadIO m => TS -> m Double
tsGetTime ts = liftIO $ do
  with 0 $ \ptr -> c'TSGetTime ts ptr >>= chkErr >> realToFrac <$> peek ptr

tsSetTime :: MonadIO m => TS -> Double -> m ()
tsSetTime ts t = liftIO $ do
  c'TSSetTime ts (realToFrac t) >>= chkErr

tsSetInitialTimeStep :: MonadIO m => TS -> Double -> Double -> m ()
tsSetInitialTimeStep ts t dt = liftIO $ do
  c'TSSetInitialTimeStep ts (realToFrac t) (realToFrac dt) >>= chkErr

tsSetExactFinalTime :: MonadIO m => TS -> TSExactFinalTimeOption -> m ()
tsSetExactFinalTime ts topt = liftIO $ do
  c'TSSetExactFinalTime ts topt >>= chkErr


-- ** Set evaluation functions.
------------------------------------------------------------------------------
tsSetIFunction :: MonadIO m => TS -> Vec -> TSIFunction -> PCtx -> m ()
tsSetIFunction ts vec fun ctx = liftIO $ do
  f' <- mk'TSIFunction fun
  c'TSSetIFunction ts vec f' ctx >>= chkErr

tsSetRHSFunction :: MonadIO m => TS -> Vec -> TSRHSFunction -> PCtx -> m ()
tsSetRHSFunction ts vec fun ctx = liftIO $ do
  f' <- mk'TSRHSFunction fun
  c'TSSetRHSFunction ts vec f' ctx >>= chkErr

------------------------------------------------------------------------------
-- | Set callback for the Jacobian evaluation function.
tsSetIJacobian :: MonadIO m => TS -> Mat -> Mat -> TSIJacobian -> PCtx -> m ()
tsSetIJacobian ts matJ matP jac ctx = liftIO $ do
  j' <- mk'TSIJacobian jac
  c'TSSetIJacobian ts matJ matP j' ctx >>= chkErr

tsSetRHSJacobian :: MonadIO m => TS -> Mat -> Mat -> TSRHSJacobian -> PCtx -> m ()
tsSetRHSJacobian ts matJ matP jac ctx = liftIO $ do
  j' <- mk'TSRHSJacobian jac
  c'TSSetRHSJacobian ts matJ matP j' ctx >>= chkErr

------------------------------------------------------------------------------
-- | Set the initial values.
tsSetSolution :: MonadIO m => TS -> Vec -> m ()
tsSetSolution ts vec = liftIO $ do
  c'TSSetSolution ts vec >>= chkErr

-- | Query the final solution.
tsGetSolution :: MonadIO m => TS -> m Vec
tsGetSolution ts = liftIO $ with (Vec nullPtr) $ \ptr -> do
  c'TSGetSolution ts ptr >>= chkErr >> peek ptr


-- ** Register additional TS types.
------------------------------------------------------------------------------
tsRegister :: MonadIO m => TS -> Ptr () -> m ()
tsRegister ts f = undefined


-- * TS evaluation.
------------------------------------------------------------------------------
-- NOTE: Mutates the given `Vec`.
tsSolve :: MonadIO m => TS -> Vec -> m Vec
tsSolve ts vec = liftIO $ do
  c'TSSolve ts vec >>= chkErr
  return vec

tsGetSolveTime :: MonadIO m => TS -> m Double
tsGetSolveTime ts = liftIO $ with 0 $ \pd -> do
  c'TSGetSolveTime ts pd >>= chkErr >> realToFrac `fmap` peek pd

tsGetTimeStepNumber :: MonadIO m => TS -> m Int
tsGetTimeStepNumber ts = liftIO $ with 0 $ \pz -> do
  c'TSGetTimeStepNumber ts pz >>= chkErr >> fromIntegral `fmap` peek pz


-- * Additional TS settings & queries.
------------------------------------------------------------------------------
tsGetSNES :: MonadIO m => TS -> m SNES
tsGetSNES ts = liftIO $ with (SNES nullPtr) $ \ptr -> do
  c'TSGetSNES ts ptr >>= chkErr >> peek ptr

tsSetDM :: MonadIO m => TS -> DM -> m ()
tsSetDM ts dm = liftIO $ c'TSSetDM ts dm >>= chkErr

tsGetDM :: MonadIO m => TS -> m DM
tsGetDM ts = liftIO $ with (DM nullPtr) $ \pd -> do
  c'TSGetDM ts pd >>= chkErr >> peek pd


{-- }
-- * TS monitoring.
------------------------------------------------------------------------------
tsSetMonitor :: MonadIO m => TS -> TSMonitor -> m ()
tsSetMonitor ts mon = liftIO $ do
  c'TSSetMonitor ts mon nullPtr (TSFreeCtx nullFunPtr) >>= chkErr

tsSetMonitorWith :: MonadIO m => TS -> TSMonitor -> PCtx -> TSFreeCtx -> m ()
tsSetMonitorWith ts mon ctx fre = liftIO $ do
  c'TSSetMonitor ts mon ctx fre >>= chkErr

tsMonitorCancel :: MonadIO m => TS -> m ()
tsMonitorCancel ts = liftIO $ c'TSMonitorCancel ts >>= chkErr
--}
